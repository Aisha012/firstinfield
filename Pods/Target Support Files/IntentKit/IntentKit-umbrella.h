#ifdef __OBJC__
#import <UIKit/UIKit.h>
#else
#ifndef FOUNDATION_EXPORT
#if defined(__cplusplus)
#define FOUNDATION_EXPORT extern "C"
#else
#define FOUNDATION_EXPORT extern
#endif
#endif
#endif

#import "NSString+Helpers.h"
#import "INKActivity.h"
#import "INKActivityCell.h"
#import "INKActivityPresenter.h"
#import "INKActivityViewController.h"
#import "INKAppIconView.h"
#import "INKApplicationList.h"
#import "INKDefaultsManager.h"
#import "INKDefaultsCell.h"
#import "INKDefaultsViewController.h"
#import "INKDefaultToggleView.h"
#import "INKHandler.h"
#import "INKLocalizedString.h"
#import "INKOpenInActivity.h"
#import "INKPresentable.h"
#import "IntentKit.h"
#import "INKBrowserHandler.h"
#import "INKWebView.h"
#import "INKWebViewController.h"
#import "INKMapsHandler.h"

FOUNDATION_EXPORT double IntentKitVersionNumber;
FOUNDATION_EXPORT const unsigned char IntentKitVersionString[];

