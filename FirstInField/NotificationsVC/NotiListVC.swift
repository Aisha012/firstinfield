//
//  NotiListVC.swift
//  FirstInField
//
//  Created by Namespace  on 30/07/18.
//  Copyright © 2018 Namespace . All rights reserved.
//

import UIKit

class NotiListVC: UITableViewController {

    var notificationResponse: NotificationData?
     var notificationList: [Notification_Data]?
    var pageIndex = 1
    
    override func viewDidLoad() {
        super.viewDidLoad()
         self.navigationItem.title = "Notifications"
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
        NotificationCenter.default.addObserver(self, selector: #selector(self.getNotificationListing), name: Notification.Name("refreshMeeting"), object: nil)
        
        getNotificationListing()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return notificationList?.count ?? 0
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if (indexPath.row == self.notificationList!.count - 2 ) && (self.notificationResponse!.data!.pagination!.currentPage! < self.notificationResponse!.data!.pagination!.lastPage!){
            self.pageIndex = self.notificationResponse!.data!.pagination!.currentPage! + 1
            self.getNotificationListing()
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "notificationCell", for: indexPath) as! NotiCell
        
        cell.lblTitle.text = notificationList![indexPath.row].title
        cell.lblDescription.text = notificationList![indexPath.row].message
        let localDate = UTCToLocal(date: notificationList![indexPath.row].created!, fromFormat: "yyyy-MM-dd HH:mm:ss", toFormat: "yyyy-MM-dd HH:mm:ss")
        let date = getDateFromString(dateString: localDate, fromFormat: "yyyy-MM-dd HH:mm:ss")
        cell.timeLbl.text = configureToShowDateFormatter(for: date)
        
        // Configure the cell...

        cell.selectionStyle = .none
        return cell
    }
    

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigationr

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    @objc func getNotificationListing(){

        let resPonsedata = getUserDefaults(key: KeyResponseModel) as! Data
        let responseModel = try! jsonDecoder.decode(LoginResponse.self, from: resPonsedata)
        
        let userId = "?user_id=\(responseModel.user?.id ?? 0)"
        let token_part = "&api_token=" + API_Token + "&user_token=" + responseModel.user!.user_token!
        let deviceId = "&device_id=\(UserDefaults.standard.value(forKey: KEY_DEVICE_ID) ?? "")"

        self.getDataFromNet(url: Urls.Notification_listing.rawValue + userId + "&page=\(pageIndex)" + deviceId + token_part , selector: #selector(self.handleNetResponse(data:)))

    }
    
    @objc func handleNetResponse(data:Data?) {
        if let dat = data{
            do{
                notificationResponse = try jsonDecoder.decode(NotificationData.self, from: dat)
                if notificationResponse?.res_msg != nil && notificationResponse?.res_code! != 200{
                    self.showSnackBarWithMessage(message: responseModel!.res_msg!, color: #colorLiteral(red: 0.9254902005, green: 0.2352941185, blue: 0.101907857, alpha: 1), interval: .long)
                    return
                }
                if notificationResponse!.data != nil && notificationResponse!.data!.notificationList!.count > 0 {
                    
                    if self.notificationList == nil{
                        self.notificationList = notificationResponse?.data?.notificationList
                    }else if notificationList != nil{
                        for dict in (notificationResponse?.data?.notificationList)!{
                            notificationList!.append(dict)
                        }
                    }
                    
                    self.tableView.reloadData()
//                    self.sectionCounts = [Int](repeating: 0, count: checklistRespons!.checklist_data!.count)
//                    self.sectionCounts![0] = 1
//                    self.reloadInputViews()
                }else{
//                    self.tbleView.isHidden = true
//                    self.lblNotAvailable.isHidden = false
                }
                
            }catch let err{
                print(err)
            }
        }
    }

}

class NotiCell:UITableViewCell{
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    
    @IBOutlet weak var timeLbl: UILabel!
}
