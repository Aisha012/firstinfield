//
//  ForgotVC.swift
//  FirstInField
//
//  Created by Namespace  on 15/06/18.
//  Copyright © 2018 Namespace . All rights reserved.
//

import UIKit

class ForgotVC: UIViewController {
    @IBOutlet weak var txtUsername: UITextField!
    @IBOutlet weak var vwUserName: UIView!
    @IBOutlet weak var btnSubmit: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    override func viewDidLayoutSubviews() {
        self.vwUserName.layer.cornerRadius = self.vwUserName.frame.size.height/2
        self.btnSubmit.layer.cornerRadius = self.btnSubmit.frame.size.height/2
    }
    
    
    @IBAction func brnOnTapForgotPassword(_ sender: UIButton) {
//        self.navigationController?.popViewController(animated: false)
    }
    
    @IBAction func btnOnTapSubmit(_ sender: UIButton) {
        self.view.endEditing(true)
        if validateFields(){
            let request = forgotRequest(email: txtUsername.text!)
            self.postDataFromNet(url: Urls.forgot.rawValue, requestData: request, selector: #selector(ViewController.handleNetResponse(data:)))
        }
    }
    
    
    
    func validateFields()->Bool{
        if !self.txtUsername.isTextFieldEmpty(){
            self.showSnackBarWithMessage(message:"Email is empty.", color:#colorLiteral(red: 0.7450980544, green: 0.1568627506, blue: 0.07450980693, alpha: 1),interval:intervals.long)
            
            return false
        }
        if !self.txtUsername.isValidEmail(){
            self.showSnackBarWithMessage(message:"Enter a valid Email.", color:#colorLiteral(red: 0.7450980544, green: 0.1568627506, blue: 0.07450980693, alpha: 1),interval:intervals.long)
            return false
        }
        return true
    }
    
    
    @objc func handleNetResponse(data:Data?) {
        if let dat = data{
            print(String(decoding: data!, as: UTF8.self))
            do{
               let forgotResponse = try jsonDecoder.decode(ForgotResponse.self, from: dat)
                if forgotResponse.res_msg != nil && forgotResponse.res_code! != 200{
                    self.showSnackBarWithMessage(message: forgotResponse.res_msg!, color: #colorLiteral(red: 0.9254902005, green: 0.2352941185, blue: 0.1019607857, alpha: 1), interval: .long)
                    return
                }
                self.showSnackBarWithMessage(message: forgotResponse.res_msg!, color: #colorLiteral(red: 0.2745098174, green: 0.4862745106, blue: 0.1411764771, alpha: 1), interval: .long)
                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 4) {
                    self.navigationController?.popViewController(animated: true)
                }

            }catch let err {
                print(err)
            }
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
