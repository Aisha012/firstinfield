//
//  AppleMapVC.swift
//  FirstInField
//
//  Created by Namespace  on 26/06/18.
//  Copyright © 2018 Namespace . All rights reserved.
//
import MapKit
import UIKit
import IntentsUI
import IntentKit

typealias tuple = (String, String)
class AppleMapVC: UIViewController, MKMapViewDelegate,CLLocationManagerDelegate {
    @IBOutlet weak var mapView: MKMapView!
    var toLocation:CLLocationCoordinate2D!
    var fromLocation:CLLocationCoordinate2D!

    let locationManager = CLLocationManager()
    var cordinate:tuple! = nil
    var fromCordinate:tuple! = ("","")
   // lazy var customNavigationAnimationController = CustomNavigationAnimationController()
    override func viewDidLoad() {
        super.viewDidLoad()
        //self.navigationItem.title = "Apple Map"
        if cordinate != nil{
            toLocation = CLLocationCoordinate2D(latitude: Double(cordinate.0)!, longitude:  Double(cordinate.1)!)
            print(cordinate.0)
            print(cordinate.1)

        }else{
            toLocation = CLLocationCoordinate2D(latitude: 37.639097, longitude: -120.996878)
        }
        isAuthorizedtoGetUserLocation()
        if CLLocationManager.locationServicesEnabled() {
            if CLLocationManager.authorizationStatus() != .authorizedWhenInUse {
                guard let urlGeneral = URL(string: UIApplicationOpenSettingsURLString) else {
                    return
                }
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(urlGeneral)
                } else {
                    // Fallback on earlier versions
                }
            }
            
           
        }
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Map", style: .plain, target: self, action: #selector(changeMap))
        navigationItem.rightBarButtonItem?.tintColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        navigationItem.leftBarButtonItem?.tintColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)

    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        print("Did location updates is called")
        let locValue : CLLocationCoordinate2D = locationManager.location!.coordinate;
        _ = MKCoordinateSpanMake(1, 1)
        let long = locValue.longitude;
        let lat = locValue.latitude;
        fromCordinate.0 = "\(lat)"
        fromCordinate.1 = "\(long)"
        print(long);
        print(lat);
        let loadlocation = CLLocationCoordinate2D(latitude: lat, longitude: long)
        self.fromLocation = loadlocation
        self.drawRoute(sourceLocation: loadlocation, destinationLocation: toLocation)
        //mapView.centerCoordinate = loadlocation;
        locationManager.stopUpdatingLocation();
        
        //store the user location here to firebase or somewhere
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Did location updates is called but failed getting location \(error)")
    }

    
    //if we have no permission to access user location, then ask user for permission.
    func isAuthorizedtoGetUserLocation()->Bool {
        
        
        switch CLLocationManager.authorizationStatus() {
        case .notDetermined:
            // Request when-in-use authorization initially
            locationManager.requestWhenInUseAuthorization()
            break
            
        case .restricted, .denied:
            if !CLLocationManager.locationServicesEnabled() {
                let alert = UIAlertController(title: "Location Alert", message: "Please Enable Phone Location Services. go to Privacy>>Location Services >> set Switch On", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (ac) in
                    if let url = URL(string: "App-Prefs:root=Privacy&path=LOCATION/com.firstinfield") {
                        //UIApplication.shared.open(url, options: [:], completionHandler: nil)
                        if #available(iOS 10.0, *) {
                            UIApplication.shared.open(url, options: [:], completionHandler: { (opened) in
                            })
                        } else {
                            // Fallback on earlier versions
                        }
                    }
                }))
                
                self.present(alert, animated: true)
                
                
            }else{
                guard let urlGeneral = URL(string: UIApplicationOpenSettingsURLString) else {
                    return false
                }
                //isLocationFalse = true
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(urlGeneral)
                } else {
                    // Fallback on earlier versions
                }
            }
            print("denied")
            
            // Disable location features
            // disableMyLocationBasedFeatures()
            break
            
        case .authorizedWhenInUse:
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()

            // Enable basic location features
            //enableMyWhenInUseFeatures()
            break
            
        case .authorizedAlways:
            // registerForPushNotifications()
            // Enable any of your app's location features
            // enableMyAlwaysFeatures()
            break
        }
        
        
        
        
        //        if CLLocationManager.authorizationStatus() == .notDetermined{
        //            print("not determined")
        //        }else if CLLocationManager.authorizationStatus() == .denied{
        //        } else if CLLocationManager.authorizationStatus() == .restricted{
        //            print("restricted")
        //        }else if CLLocationManager.authorizationStatus() != .authorizedWhenInUse     {
        //            locationManager.requestWhenInUseAuthorization()
        //            return false
        //        }
        
        return true
    }
    
    
    
    
    //this method will be called each time when a user change his location access preference.
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .authorizedWhenInUse {
            print("User allowed us to access location")
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
            //do whatever init activities here.
        }
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func changeMap() {
        if cordinate != nil{
            let handler : INKMapsHandler = INKMapsHandler()
            handler.alwaysShowActivityView  =  true
            handler.useSystemDefault = true
            let toLoc  = cordinate.0 + "," + cordinate.1
            let fromLoc  = fromCordinate.0 + "," + fromCordinate.1
            let presenter = handler.directions(from: fromLoc, to: toLoc)
            presenter?.presentModalActivitySheet(from: self.navigationController!.tabBarController!.parent!, completion: {
                
            })
        }else{
            self.showSnackBarWithMessage(message: "No co-ordinates found for destination", color: UIColor.red, txtColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), interval: .long, minusHeight: 60)
        }
        
    }
    
    
    
    func drawRoute(sourceLocation:CLLocationCoordinate2D,destinationLocation:CLLocationCoordinate2D ){
        let sourcePin = customPin(pinTitle: "", pinSubTitle: "", location: sourceLocation)
        let destinationPin = customPin(pinTitle: "", pinSubTitle: "", location: destinationLocation)
    //    self.mapView.showAnnotations([sourcePin,destinationPin], animated: true)
        self.mapView.fitAll(in: [sourcePin,destinationPin], andShow: true)
//        let reg = MKCoordinateSpan.init(latitudeDelta: sourceLocation.latitude, longitudeDelta: sourceLocation.longitude)
//        let regi = MKCoordinateRegion.init(center: destinationLocation, span: reg)
//        self.mapView.setRegion(regi, animated: true)
         var sourcePlaceMark:MKPlacemark!
        if #available(iOS 10.0, *) {
             sourcePlaceMark = MKPlacemark(coordinate: sourceLocation)
        } else {
            // Fallback on earlier versions
        }
        var destinationPlaceMark:MKPlacemark!
        if #available(iOS 10.0, *) {
             destinationPlaceMark = MKPlacemark(coordinate: destinationLocation)
        } else {
            // Fallback on earlier versions
        }
        
        let directionRequest = MKDirectionsRequest()
        directionRequest.source = MKMapItem(placemark: sourcePlaceMark)
        directionRequest.destination = MKMapItem(placemark: destinationPlaceMark)
        directionRequest.transportType = .any
        
        let directions = MKDirections(request: directionRequest)
        directions.calculate {[weak self] (response, error) in
            guard let directionResonse = response else {
                if let error = error {
                    //print("we have error getting directions==\(error.localizedDescription)")
           //         self?.showSnackBarWithMessage(message: error.localizedDescription, color: UIColor.red, txtColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), interval: .long, minusHeight: 60)
                }
                return
            }
            
            //get route and assign to our route variable
            let route = directionResonse.routes[0]
            
            //add rout to our mapview
            self?.mapView.add(route.polyline, level: .aboveRoads)
            
            //setting rect of our mapview to fit the two locations
            let rect = route.polyline.boundingMapRect
            self?.mapView.setRegion(MKCoordinateRegionForMapRect(rect), animated: true)
    }
        self.mapView.delegate = self

    }
        
        
        //MARK:- MapKit delegates
        func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
            let renderer = MKPolylineRenderer(overlay: overlay)
            renderer.strokeColor = UIColor.blue
            renderer.lineWidth = 4.0
            return renderer
        }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
//    func navigationController(_ navigationController: UINavigationController, animationControllerFor operation: UINavigationControllerOperation, from fromVC: UIViewController, to toVC: UIViewController) -> UIViewControllerAnimatedTransitioning? {
//        if (fromVC.isKind(of: AppleMapVC.classForCoder()) && toVC.isKind(of: GoogleMapVC.classForCoder())) || (fromVC.isKind(of: GoogleMapVC.classForCoder()) && toVC.isKind(of: AppleMapVC.classForCoder())){
//            customNavigationAnimationController.reverse = operation == .pop
//            return customNavigationAnimationController
//        }
//        return nil
//
//    }

}

class customPin: NSObject, MKAnnotation {
    var coordinate: CLLocationCoordinate2D
    var title: String?
    var subtitle: String?
    
    init(pinTitle:String, pinSubTitle:String, location:CLLocationCoordinate2D) {
        self.title = pinTitle
        self.subtitle = pinSubTitle
        self.coordinate = location
    }
}


extension MKMapView {
    /// when we call this function, we have already added the annotations to the map, and just want all of them to be displayed.
    func fitAll() {
        var zoomRect            = MKMapRectNull;
        for annotation in annotations {
            let annotationPoint = MKMapPointForCoordinate(annotation.coordinate)
            let pointRect       = MKMapRectMake(annotationPoint.x, annotationPoint.y, 0.15, 0.15);
            zoomRect            = MKMapRectUnion(zoomRect, pointRect);
        }
        setVisibleMapRect(zoomRect, edgePadding: UIEdgeInsetsMake(100, 100, 100, 100), animated: true)
    }
    
    /// we call this function and give it the annotations we want added to the map. we display the annotations if necessary
    func fitAll(in annotations: [MKAnnotation], andShow show: Bool) {
        var zoomRect:MKMapRect  = MKMapRectNull
        
        for annotation in annotations {
            let aPoint          = MKMapPointForCoordinate(annotation.coordinate)
            let rect            = MKMapRectMake(aPoint.x, aPoint.y, 0.01, 0.01)
            
            if MKMapRectIsNull(zoomRect) {
                zoomRect = rect
            } else {
                zoomRect = MKMapRectUnion(zoomRect, rect)
            }
        }
        if(show) {
            addAnnotations(annotations)
        }
        setVisibleMapRect(zoomRect, edgePadding: UIEdgeInsets(top: 100, left: 100, bottom: 100, right: 100), animated: true)
    }
    
}

