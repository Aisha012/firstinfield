//
//  NotiRequestVC.swift
//  FirstInField
//
//  Created by Namespace  on 28/06/18.
//  Copyright © 2018 Namespace . All rights reserved.
//

import UIKit
import CoreLocation
import UserNotifications

protocol NotiRequestProtocol:class {
    func appDidEnterForeGROUND()
}

class NotiRequestVC: UIViewController, CLLocationManagerDelegate, NotiRequestProtocol {
    @IBOutlet weak var btnOkay: UIButton!
    
    var isNotification:Bool = false
    var isLocationFalse:Bool = false
    
    func appDidEnterForeGROUND() {
        if(isLocationFalse){
            self.isAuthorizedtoGetUserLocation()
            isLocationFalse = false
        }else{
            if #available(iOS 10.0, *) {
                UNUserNotificationCenter.current().getNotificationSettings { (settings) in
                    guard settings.authorizationStatus == .authorized else { return }
                    DispatchQueue.main.async {
                        UIApplication.shared.registerForRemoteNotifications()
                        self.setUserDefaults(key: KEY_LOCATION_NOTIFICATION, value: true)
                        self.dismiss(animated: true, completion: nil)
                        //self.navigationController?.popViewController(animated: true)
                    }
                }
            } else {
                if UIApplication.shared.isRegisteredForRemoteNotifications == false {
                    UIApplication.shared.registerForRemoteNotifications()
                }
            }
        }
        
       
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    lazy var locationManager = CLLocationManager()
    var btnPressed:Bool = false
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = true

        locationManager.delegate = self
        (UIApplication.shared.delegate as! AppDelegate).protoDel = self
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        print("appeared..")
    }
    
    @IBAction func btnOnClickOkay(){
        btnPressed = true
        if isAuthorizedtoGetUserLocation(){
            
        }
    }

    override func viewDidLayoutSubviews() {
        self.btnOkay.layer.cornerRadius = self.btnOkay.frame.size.height/2
        self.btnOkay.clipsToBounds = true
    }
    
    
//    override func viewWillAppear(_ animated: Bool) {
//        self.navigationController?.navigationBar.isHidden = true
//    }
//
//    override func viewWillDisappear(_ animated: Bool) {
//        self.navigationController?.navigationBar.isHidden = false
//    }
    
    func isAuthorizedtoGetUserLocation()->Bool {
        
        
        switch CLLocationManager.authorizationStatus() {
        case .notDetermined:
            // Request when-in-use authorization initially
            locationManager.requestWhenInUseAuthorization()
            break
            
        case .restricted, .denied:
            if !CLLocationManager.locationServicesEnabled() {
                let alert = UIAlertController(title: "Location Alert", message: "Please Enable Phone Location Services. go to Privacy>>Location Services >> set Switch On", preferredStyle: .alert)
                isLocationFalse = true
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (ac) in
                    if let url = URL(string: "App-Prefs:root=Privacy&path=LOCATION/com.firstinfield") {
                        //UIApplication.shared.open(url, options: [:], completionHandler: nil)
                        if #available(iOS 10.0, *) {
                            UIApplication.shared.open(url, options: [:], completionHandler: { (opened) in
                                if CLLocationManager.locationServicesEnabled() && CLLocationManager.authorizationStatus() != .authorizedAlways {
                                    guard let urlGeneral = URL(string: UIApplicationOpenSettingsURLString) else {
                                        return
                                    }
                                    UIApplication.shared.open(urlGeneral)
                                }
                            })
                        } else {
                            // Fallback on earlier versions
                        }
                    }
                }))

                self.present(alert, animated: true)

               
            }else{
                guard let urlGeneral = URL(string: UIApplicationOpenSettingsURLString) else {
                    return false
                }
                //isLocationFalse = true
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(urlGeneral)
                } else {
                    // Fallback on earlier versions
                }
            }
            print("denied")

            // Disable location features
           // disableMyLocationBasedFeatures()
            break
            
        case .authorizedWhenInUse:
            registerForPushNotifications()
            // Enable basic location features
            //enableMyWhenInUseFeatures()
            break
            
        case .authorizedAlways:
           // registerForPushNotifications()
            // Enable any of your app's location features
           // enableMyAlwaysFeatures()
            break
        }
        
        
        
        
//        if CLLocationManager.authorizationStatus() == .notDetermined{
//            print("not determined")
//        }else if CLLocationManager.authorizationStatus() == .denied{
//        } else if CLLocationManager.authorizationStatus() == .restricted{
//            print("restricted")
//        }else if CLLocationManager.authorizationStatus() != .authorizedWhenInUse     {
//            locationManager.requestWhenInUseAuthorization()
//            return false
//        }
        
        return true
    }
    
    var isComingFromDidChange:Bool = false
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus){
        print("changed")
        if status == .authorizedWhenInUse || status == .authorizedAlways{
           // setUserDefaults(key: KEY_LOCATION_NOTIFICATION, value: true)
            if btnPressed
            {
                isComingFromDidChange = true
                registerForPushNotifications()
                
            }
        }else{
        }
    }
    
    
    
    func registerForPushNotifications() {
        if #available(iOS 10.0, *) {
            UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound, .badge]) {[weak self] (granted, error) in
                guard granted else {
                    if (self?.isComingFromDidChange == false){
                        guard let urlGeneral = URL(string: UIApplicationOpenSettingsURLString) else {
                            return
                        }
                        UIApplication.shared.open(urlGeneral, options: [:], completionHandler: {(opu) in
                        })
                    }
                   self?.isComingFromDidChange = false
                    return
                }
                self?.getNotificationSettings()
            }
        } else {
            let setting = UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            UIApplication.shared.registerUserNotificationSettings(setting)
            UIApplication.shared.registerForRemoteNotifications()
            getNotificationSettings()
        }
    }
    
    func getNotificationSettings() {
        if #available(iOS 10.0, *) {
            UNUserNotificationCenter.current().getNotificationSettings { (settings) in
                guard settings.authorizationStatus == .authorized else { return }
                DispatchQueue.main.async {
                    UIApplication.shared.registerForRemoteNotifications()
                    self.setUserDefaults(key: KEY_LOCATION_NOTIFICATION, value: true)
                   // self.navigationController?.popViewController(animated: true)
                    self.dismiss(animated: true, completion: nil)

                }
            }
        } else {
            if UIApplication.shared.isRegisteredForRemoteNotifications == false {
                UIApplication.shared.registerForRemoteNotifications()
            }
        }
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
