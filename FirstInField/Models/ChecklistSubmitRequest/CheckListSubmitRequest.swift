//
//  CheckListSubmitRequest.swift
//  FirstInField
//
//  Created by Namespace  on 07/07/18.
//  Copyright © 2018 Namespace . All rights reserved.
//

import Foundation

struct CheckListSubmitRequest:Codable {
    var response:String?
    var remark:String?
    var response_by:String?
    var api_token:String?
    var user_token:String?
}
