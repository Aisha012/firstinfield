/* 
Copyright (c) 2018 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar

*/

import Foundation
struct Meeting_details : Codable {
    let id : Int?
    let meetingname : String?
    let description : String?
    let date : String?
    let start_time : String?
    let address : String?
    let latitude : String?
    let longitude : String?
    let status : String?
    let created_by : String?
    let end_time : String?
    
    enum CodingKeys: String, CodingKey {
        
        case id = "id"
        case meetingname = "meetingname"
        case description = "description"
        case date = "date"
        case start_time = "start_time"
        case address = "address"
        case latitude = "latitude"
        case longitude = "longitude"
        case status = "status"
        case created_by = "created_by"
        case end_time = "end_time"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(Int.self, forKey: .id)
        meetingname = try values.decodeIfPresent(String.self, forKey: .meetingname)
        description = try values.decodeIfPresent(String.self, forKey: .description)
        date = try values.decodeIfPresent(String.self, forKey: .date)
        start_time = try values.decodeIfPresent(String.self, forKey: .start_time)
        address = try values.decodeIfPresent(String.self, forKey: .address)
        latitude = try values.decodeIfPresent(String.self, forKey: .latitude)
        longitude = try values.decodeIfPresent(String.self, forKey: .longitude)
        status = try values.decodeIfPresent(String.self, forKey: .status)
        created_by = try values.decodeIfPresent(String.self, forKey: .created_by)
        end_time = try values.decodeIfPresent(String.self, forKey: .end_time)
    }

}
