/* 
Copyright (c) 2018 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar

*/

import Foundation
struct Meeting_detail_data : Codable {
	let created_by : String?
	let no_of_checklists : Int?
	let no_of_questions : Int?
	let no_of_representative : Int?
	let no_of_documents : Int?
	let meeting_details : Meeting_details?
	let representatives : [Representatives]?
	var documents : [Documents]?

	enum CodingKeys: String, CodingKey {

		case created_by = "created_by"
		case no_of_checklists = "no_of_checklists"
		case no_of_questions = "no_of_questions"
		case no_of_representative = "no_of_representative"
		case no_of_documents = "no_of_documents"
		case meeting_details = "meeting_details"
		case representatives = "representatives"
		case documents = "documents"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		created_by = try values.decodeIfPresent(String.self, forKey: .created_by)
		no_of_checklists = try values.decodeIfPresent(Int.self, forKey: .no_of_checklists)
		no_of_questions = try values.decodeIfPresent(Int.self, forKey: .no_of_questions)
		no_of_representative = try values.decodeIfPresent(Int.self, forKey: .no_of_representative)
		no_of_documents = try values.decodeIfPresent(Int.self, forKey: .no_of_documents)
		meeting_details = try values.decodeIfPresent(Meeting_details.self, forKey: .meeting_details)
		representatives = try values.decodeIfPresent([Representatives].self, forKey: .representatives)
		documents = try values.decodeIfPresent([Documents].self, forKey: .documents)
	}

}
