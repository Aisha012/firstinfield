//
//  StartMeetingRequest.swift
//  FirstInField
//
//  Created by Namespace  on 24/07/18.
//  Copyright © 2018 Namespace . All rights reserved.
//

import UIKit

struct StartMeetingRequest: Codable {
    var api_token: String
    var user_token: String
    var start_time: String
    var latitude: String
    var longitude: String

}



struct StopMeetingRequest: Codable {
    var api_token: String
    var user_token: String
    var end_time: String
    var meeting_type: String
    var remark_note: String

}

struct ResetPasswordRequest: Codable {
    var api_token: String
    var user_token: String
    var new_password: String
    var old_password: String
}

struct ResetAccountRequest: Codable {
    var api_token: String
    var user_token: String
    var email: String
}

struct LogOutAccountRequest: Codable {
    var api_token: String
    var user_token: String
    var device_id: String
}
