//
//  StartMeetingResponse.swift
//  FirstInField
//
//  Created by Namespace  on 24/07/18.
//  Copyright © 2018 Namespace . All rights reserved.
//


import Foundation
struct StartMeetingResponse : Codable {
    let res_code : Int?
    let res_msg : String?
    let data : Bool?
    
    enum CodingKeys: String, CodingKey {
        
        case res_code = "res_code"
        case res_msg = "res_msg"
        case data = "data"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        res_code = try values.decodeIfPresent(Int.self, forKey: .res_code)
        res_msg = try values.decodeIfPresent(String.self, forKey: .res_msg)
        data = try values.decodeIfPresent(Bool.self, forKey: .data)
    }
    
}


struct ResetPasswordResponse : Codable {
    let res_code : Int?
    let res_msg : String?
    let data : String?
    
    enum CodingKeys: String, CodingKey {
        
        case res_code = "res_code"
        case res_msg = "res_msg"
        case data = "data"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        res_code = try values.decodeIfPresent(Int.self, forKey: .res_code)
        res_msg = try values.decodeIfPresent(String.self, forKey: .res_msg)
        data = try values.decodeIfPresent(String.self, forKey: .data)
    }
    
}

struct ResetAccountResponse : Codable {
    let res_code : Int?
    let res_msg : String?
    let data : String?
    
    enum CodingKeys: String, CodingKey {
        
        case res_code = "res_code"
        case res_msg = "res_msg"
        case data = "data"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        res_code = try values.decodeIfPresent(Int.self, forKey: .res_code)
        res_msg = try values.decodeIfPresent(String.self, forKey: .res_msg)
        data = try values.decodeIfPresent(String.self, forKey: .data)
    }
    
}

struct LogOutUserResponse : Codable {
    let res_code : Int?
    let res_msg : String?
    let data : String?
    
    enum CodingKeys: String, CodingKey {
        
        case res_code = "res_code"
        case res_msg = "res_msg"
        case data = "data"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        res_code = try values.decodeIfPresent(Int.self, forKey: .res_code)
        res_msg = try values.decodeIfPresent(String.self, forKey: .res_msg)
        data = try values.decodeIfPresent(String.self, forKey: .data)
    }
    
}
