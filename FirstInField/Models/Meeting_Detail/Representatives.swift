/* 
Copyright (c) 2018 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar

*/

import Foundation
struct Representatives : Codable {
	let id : Int?
	let inviter_id : String?
	let firstname : String?
	let lastname : String?
	let email : String?
	let contactno : String?
	let additional_notes : String?
	let term_and_condition : String?
	let role : String?
	let status : String?
	let user_token : String?
	let created_at : String?
	let updated_at : String?
	let pivot : Pivot?
    let image:String?
	enum CodingKeys: String, CodingKey {

		case id = "id"
		case inviter_id = "inviter_id"
		case firstname = "firstname"
		case lastname = "lastname"
		case email = "email"
		case contactno = "contactno"
		case additional_notes = "additional_notes"
		case term_and_condition = "term_and_condition"
		case role = "role"
		case status = "status"
		case user_token = "user_token"
		case created_at = "created_at"
		case updated_at = "updated_at"
		case pivot = "pivot"
        case image = "image"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		id = try values.decodeIfPresent(Int.self, forKey: .id)
		inviter_id = try values.decodeIfPresent(String.self, forKey: .inviter_id)
		firstname = try values.decodeIfPresent(String.self, forKey: .firstname)
		lastname = try values.decodeIfPresent(String.self, forKey: .lastname)
		email = try values.decodeIfPresent(String.self, forKey: .email)
		contactno = try values.decodeIfPresent(String.self, forKey: .contactno)
		additional_notes = try values.decodeIfPresent(String.self, forKey: .additional_notes)
		term_and_condition = try values.decodeIfPresent(String.self, forKey: .term_and_condition)
		role = try values.decodeIfPresent(String.self, forKey: .role)
		status = try values.decodeIfPresent(String.self, forKey: .status)
		user_token = try values.decodeIfPresent(String.self, forKey: .user_token)
		created_at = try values.decodeIfPresent(String.self, forKey: .created_at)
		updated_at = try values.decodeIfPresent(String.self, forKey: .updated_at)
		pivot = try values.decodeIfPresent(Pivot.self, forKey: .pivot)
        image = try values.decodeIfPresent(String.self, forKey: .image)

	}

}
