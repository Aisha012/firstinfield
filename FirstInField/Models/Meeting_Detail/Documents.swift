/* 
Copyright (c) 2018 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar

*/

import Foundation
struct Documents : Codable {
	let id : Int?
	let meeting_id : String?
	let name : String?
	let uniquename : String?
	let path : String?
	let size : String?
	let is_deleted : String?
	let created_at : String?
	let updated_at : String?
	let type : String?

	enum CodingKeys: String, CodingKey {

		case id = "id"
		case meeting_id = "meeting_id"
		case name = "name"
		case uniquename = "uniquename"
		case path = "path"
		case size = "size"
		case is_deleted = "is_deleted"
		case created_at = "created_at"
		case updated_at = "updated_at"
		case type = "type"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		id = try values.decodeIfPresent(Int.self, forKey: .id)
		meeting_id = try values.decodeIfPresent(String.self, forKey: .meeting_id)
		name = try values.decodeIfPresent(String.self, forKey: .name)
		uniquename = try values.decodeIfPresent(String.self, forKey: .uniquename)
		path = try values.decodeIfPresent(String.self, forKey: .path)
		size = try values.decodeIfPresent(String.self, forKey: .size)
		is_deleted = try values.decodeIfPresent(String.self, forKey: .is_deleted)
		created_at = try values.decodeIfPresent(String.self, forKey: .created_at)
		updated_at = try values.decodeIfPresent(String.self, forKey: .updated_at)
		type = try values.decodeIfPresent(String.self, forKey: .type)
	}

}
