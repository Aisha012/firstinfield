/* 
Copyright (c) 2018 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar

*/
import MessageKit
import Foundation
typealias ChatCodable = Codable & MessageType
struct UserChatData : ChatCodable {
    var sender: Sender
    var messageId: String
    var sentDate: Date
    var kind: MessageKind
    
    var status: String?
    var senderId : Int?
    var senderName : String?
    var tempMessageId : Int?
    var message_send : String?
    var lastsendedon : String?
    
	enum CodingKeys: String, CodingKey {
        
        case tempMessageId = "chat_id"
        case senderId = "user_id"
        case senderName = "user_name"
        case status = "status"
        case message_send = "message_send"
        case lastsendedon = "lastsendedon"
        
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
        
        senderId = try values.decodeIfPresent(Int.self, forKey: .senderId)
        senderName = try values.decodeIfPresent(String.self, forKey: .senderName)
        sender = Sender(id: "\(senderId!)", displayName: senderName!)
        
        tempMessageId = try values.decodeIfPresent(Int.self, forKey: .tempMessageId)
        messageId = "\(tempMessageId ?? 0)"
        
        lastsendedon = try values.decodeIfPresent(String.self, forKey: .lastsendedon)
        let dateString = UTCToLocal(date: lastsendedon!, fromFormat: "yyyy-MM-dd HH-mm-ss", toFormat: "yyyy-MM-dd HH-mm-ss")
        sentDate = getDateFromString(dateString: dateString, fromFormat: "yyyy-MM-dd HH-mm-ss")
        
        message_send = try values.decodeIfPresent(String.self, forKey: .message_send)
        kind = .text(message_send!)
        
        status = (try values.decodeIfPresent(String.self, forKey: .status))!
        
	}

}
