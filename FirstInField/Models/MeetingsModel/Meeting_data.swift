/* 
Copyright (c) 2018 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar

*/

import Foundation
struct Meeting_data : Codable {
	var user_meetings : [User_meetings]?
	var pagination : Pagination?
    var meeting_count : Int?
    var adhoc_count : Int?

	enum CodingKeys: String, CodingKey {

		case user_meetings = "user_meetings"
		case pagination = "pagination"
        case meeting_count = "meeting_count"
        case adhoc_count = "adhoc_count"
        
	}

    init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		user_meetings = try values.decodeIfPresent([User_meetings].self, forKey: .user_meetings)
		pagination = try values.decodeIfPresent(Pagination.self, forKey: .pagination)
        meeting_count = try values.decodeIfPresent(Int.self, forKey: .meeting_count)
        adhoc_count = try values.decodeIfPresent(Int.self, forKey: .adhoc_count)
	}
    

}
