/* 
Copyright (c) 2018 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar

*/

import Foundation
struct Meetings : Codable {
	let id : Int?
	let meetingname : String?
	let date : String?
	let scheduled_start_time : String?
	let scheduled_end_time : String?
	let actual_start_time : String?
	let actual_end_time : String?
	let scheduled_address : String?
	let actual_address : String?
	let scheduled_latitude : String?
	let scheduled_longitude : String?
	let actual_latitude : String?
	let actual_longitude : String?
	let status : String?
	let created_by : String?
    
	enum CodingKeys: String, CodingKey {

		case id = "id"
		case meetingname = "meetingname"
		case date = "date"
		case scheduled_start_time = "scheduled_start_time"
		case scheduled_end_time = "scheduled_end_time"
		case actual_start_time = "start_time"
		case actual_end_time = "end_time"
		case scheduled_address = "address"
		case actual_address = "actual_address"
		case scheduled_latitude = "scheduled_latitude"
		case scheduled_longitude = "scheduled_longitude"
		case actual_latitude = "latitude"
		case actual_longitude = "longitude"
		case status = "status"
		case created_by = "created_by"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		id = try values.decodeIfPresent(Int.self, forKey: .id)
		meetingname = try values.decodeIfPresent(String.self, forKey: .meetingname)
		date = try values.decodeIfPresent(String.self, forKey: .date)
		scheduled_start_time = try values.decodeIfPresent(String.self, forKey: .scheduled_start_time)
		scheduled_end_time = try values.decodeIfPresent(String.self, forKey: .scheduled_end_time)
		actual_start_time = try values.decodeIfPresent(String.self, forKey: .actual_start_time)
		actual_end_time = try values.decodeIfPresent(String.self, forKey: .actual_end_time)
		scheduled_address = try values.decodeIfPresent(String.self, forKey: .scheduled_address)
		actual_address = try values.decodeIfPresent(String.self, forKey: .actual_address)
		scheduled_latitude = try values.decodeIfPresent(String.self, forKey: .scheduled_latitude)
		scheduled_longitude = try values.decodeIfPresent(String.self, forKey: .scheduled_longitude)
		actual_latitude = try values.decodeIfPresent(String.self, forKey: .actual_latitude)
		actual_longitude = try values.decodeIfPresent(String.self, forKey: .actual_longitude)
		status = try values.decodeIfPresent(String.self, forKey: .status)
		created_by = try values.decodeIfPresent(String.self, forKey: .created_by)
	}

}
