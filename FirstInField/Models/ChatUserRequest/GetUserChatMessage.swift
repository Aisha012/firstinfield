//
//  GetUserChatMessage.swift
//  FirstInField
//
//  Created by Namespace  on 13/09/18.
//  Copyright © 2018 Namespace . All rights reserved.
//

import Foundation
struct GetUserChatMessage : Codable {
    let user_id : Int?
    let message : String?
    let user_token : String?
    let api_token : String?
    
}
