/* 
Copyright (c) 2018 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar

*/

import Foundation
struct User : Codable {
	let id : Int?
	let first_name : String?
	let last_name : String?
	let email : String?
	let role : String?
    let company_id:Int?
	let name_of_company : String?
	let phone_no : String?
	let status : String?
    let user_token:String?
    let setting_status:Int?
    let adhoc_count:Int?
    let meeting_count:Int?
    
	enum CodingKeys: String, CodingKey {

		case id = "id"
		case first_name = "first_name"
		case last_name = "last_name"
		case email = "email"
		case role = "role"
        case company_id = "company_id"
		case name_of_company = "name_of_company"
		case phone_no = "phone_no"
		case status = "status"
        case user_token = "user_token"
        case setting_status = "setting_status"
        case adhoc_count = "adhoc_count"
        case meeting_count = "meeting_count"

	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		id = try values.decodeIfPresent(Int.self, forKey: .id)
		first_name = try values.decodeIfPresent(String.self, forKey: .first_name)
		last_name = try values.decodeIfPresent(String.self, forKey: .last_name)
		email = try values.decodeIfPresent(String.self, forKey: .email)
		role = try values.decodeIfPresent(String.self, forKey: .role)
        company_id = try values.decodeIfPresent(Int.self, forKey: .company_id)
		name_of_company = try values.decodeIfPresent(String.self, forKey: .name_of_company)
		phone_no = try values.decodeIfPresent(String.self, forKey: .phone_no)
		status = try values.decodeIfPresent(String.self, forKey: .status)
        user_token = try values.decodeIfPresent(String.self, forKey: .user_token)
        setting_status = try values.decodeIfPresent(Int.self, forKey: .setting_status)
        adhoc_count = try values.decodeIfPresent(Int.self, forKey: .adhoc_count)
        meeting_count = try values.decodeIfPresent(Int.self, forKey: .meeting_count)
       
	}

}
