//
//  Notification.swift
//  FirstInField
//
//  Created by Namespace  on 04/10/18.
//  Copyright © 2018 Namespace . All rights reserved.
//

import Foundation
struct NotificationData : Codable {
    let res_code : Int?
    let res_msg : String?
    let data : NotificationPagination?
    
    enum CodingKeys: String, CodingKey {
        
        case res_code = "res_code"
        case res_msg = "res_msg"
        case data = "data"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        res_code = try values.decodeIfPresent(Int.self, forKey: .res_code)
        res_msg = try values.decodeIfPresent(String.self, forKey: .res_msg)
        data = try values.decodeIfPresent(NotificationPagination.self, forKey: .data)
    }
    
}

struct NotificationPagination : Codable {
    let pagination : Pagination?
    let notificationList : [Notification_Data]?
    
    enum CodingKeys: String, CodingKey {
        
        case pagination = "pagination"
        case notificationList = "notification"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        pagination = try values.decodeIfPresent(Pagination.self, forKey: .pagination)
        notificationList = try values.decodeIfPresent([Notification_Data].self, forKey: .notificationList)
    }
    
}

struct Notification_Data : Codable {
    let id : Int?
    let title : String?
    let message : String?
    let status : String?
    let created : String?
    let notification_data : String?

    
    enum CodingKeys: String, CodingKey {
        
        case id = "id"
        case title = "title"
        case message = "message"
        case status = "status"
        case created = "created"
        case notification_data = "notification_data"
        
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(Int.self, forKey: .id)
        title = try values.decodeIfPresent(String.self, forKey: .title)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        status = try values.decodeIfPresent(String.self, forKey: .status)
        created = try values.decodeIfPresent(String.self, forKey: .created)
        notification_data = try values.decodeIfPresent(String.self, forKey: .notification_data)
    }
    
}
