/* 
Copyright (c) 2018 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar

*/

import Foundation
struct Last_message : Codable {
	let id : Int?
	let sender_id : String?
	let reciever_id : String?
	let meeting_id : String?
	let message : String?
	let is_read : String?
	let pid : String?
	let sendedon : String?
	let lastsendedon : String?
    let status : String?
    
	enum CodingKeys: String, CodingKey {

		case id = "id"
		case sender_id = "sender_id"
		case reciever_id = "reciever_id"
		case meeting_id = "meeting_id"
		case message = "message"
		case is_read = "is_read"
		case pid = "pid"
		case sendedon = "sendedon"
		case lastsendedon = "lastsendedon"
        case status = "status"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		id = try values.decodeIfPresent(Int.self, forKey: .id)
		sender_id = try values.decodeIfPresent(String.self, forKey: .sender_id)
		reciever_id = try values.decodeIfPresent(String.self, forKey: .reciever_id)
		meeting_id = try values.decodeIfPresent(String.self, forKey: .meeting_id)
		message = try values.decodeIfPresent(String.self, forKey: .message)
		is_read = try values.decodeIfPresent(String.self, forKey: .is_read)
		pid = try values.decodeIfPresent(String.self, forKey: .pid)
		sendedon = try values.decodeIfPresent(String.self, forKey: .sendedon)
		lastsendedon = try values.decodeIfPresent(String.self, forKey: .lastsendedon)
        status = try values.decodeIfPresent(String.self, forKey: .status)
	}

}


struct UsersInChat : Codable {
    let user_id : Int?
    let name : String?
    
    enum CodingKeys: String, CodingKey {
        
        case user_id = "user_id"
        case name = "name"
    
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        user_id = try values.decodeIfPresent(Int.self, forKey: .user_id)
        name = try values.decodeIfPresent(String.self, forKey: .name)
        
    }
    
}
