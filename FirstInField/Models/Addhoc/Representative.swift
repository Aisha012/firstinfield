/* 
Copyright (c) 2018 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar

*/

import Foundation
struct Representative : Codable {
	let id : Int?
	let inviter_id : Int?
	let firstname : String?
	let lastname : String?
	let email : String?
	let password : String?
	let contactno : String?
	let additional_notes : String?
	let address : String?
	let term_and_condition : Int?
	let image : String?
	let role : Int?
	let status : Int?
	let user_token : String?
	let remember_token : String?
	let stripe_id : String?
	let card_brand : String?
	let card_last_four : String?
	let trial_period : Int?
	let subscriber : Int?
	let trial_ends_at : String?
	let subscription_ends_at : String?
	let created_at : String?
	let updated_at : String?

	enum CodingKeys: String, CodingKey {

		case id = "id"
		case inviter_id = "inviter_id"
		case firstname = "firstname"
		case lastname = "lastname"
		case email = "email"
		case password = "password"
		case contactno = "contactno"
		case additional_notes = "additional_notes"
		case address = "address"
		case term_and_condition = "term_and_condition"
		case image = "image"
		case role = "role"
		case status = "status"
		case user_token = "user_token"
		case remember_token = "remember_token"
		case stripe_id = "stripe_id"
		case card_brand = "card_brand"
		case card_last_four = "card_last_four"
		case trial_period = "trial_period"
		case subscriber = "subscriber"
		case trial_ends_at = "trial_ends_at"
		case subscription_ends_at = "subscription_ends_at"
		case created_at = "created_at"
		case updated_at = "updated_at"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		id = try values.decodeIfPresent(Int.self, forKey: .id)
		inviter_id = try values.decodeIfPresent(Int.self, forKey: .inviter_id)
		firstname = try values.decodeIfPresent(String.self, forKey: .firstname)
		lastname = try values.decodeIfPresent(String.self, forKey: .lastname)
		email = try values.decodeIfPresent(String.self, forKey: .email)
		password = try values.decodeIfPresent(String.self, forKey: .password)
		contactno = try values.decodeIfPresent(String.self, forKey: .contactno)
		additional_notes = try values.decodeIfPresent(String.self, forKey: .additional_notes)
		address = try values.decodeIfPresent(String.self, forKey: .address)
		term_and_condition = try values.decodeIfPresent(Int.self, forKey: .term_and_condition)
		image = try values.decodeIfPresent(String.self, forKey: .image)
		role = try values.decodeIfPresent(Int.self, forKey: .role)
		status = try values.decodeIfPresent(Int.self, forKey: .status)
		user_token = try values.decodeIfPresent(String.self, forKey: .user_token)
		remember_token = try values.decodeIfPresent(String.self, forKey: .remember_token)
		stripe_id = try values.decodeIfPresent(String.self, forKey: .stripe_id)
		card_brand = try values.decodeIfPresent(String.self, forKey: .card_brand)
		card_last_four = try values.decodeIfPresent(String.self, forKey: .card_last_four)
		trial_period = try values.decodeIfPresent(Int.self, forKey: .trial_period)
		subscriber = try values.decodeIfPresent(Int.self, forKey: .subscriber)
		trial_ends_at = try values.decodeIfPresent(String.self, forKey: .trial_ends_at)
		subscription_ends_at = try values.decodeIfPresent(String.self, forKey: .subscription_ends_at)
		created_at = try values.decodeIfPresent(String.self, forKey: .created_at)
		updated_at = try values.decodeIfPresent(String.self, forKey: .updated_at)
	}

}
