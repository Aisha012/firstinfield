//
//  ToggleResponse.swift
//  FirstInField
//
//  Created by Renendra Saxena on 27/09/18.
//  Copyright © 2018 Namespace . All rights reserved.
//

import Foundation
struct ToggleResponse : Codable {
    let res_code : Int?
    let res_msg : String?
    let data : ToggleStatus?
    
    enum CodingKeys: String, CodingKey {
        
        case res_code = "res_code"
        case res_msg = "res_msg"
        case data = "data"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        res_code = try values.decodeIfPresent(Int.self, forKey: .res_code)
        res_msg = try values.decodeIfPresent(String.self, forKey: .res_msg)
        data = try values.decodeIfPresent(ToggleStatus.self, forKey: .data)
    }
    
}
