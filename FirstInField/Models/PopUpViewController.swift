//
//  PopUpViewController.swift
//  FirstInField
//
//  Created by Namespace  on 01/10/18.
//  Copyright © 2018 Namespace . All rights reserved.
//

import UIKit
import DropDown

class PopUpViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextViewDelegate, UITextFieldDelegate {

    var forView = ""
    var meetingId = ""
    
    var checkListResponseMeeting:Meeting?
    var meetingTypeResponse:MeetingTypeResponse?
    let dropDown = DropDown()
    
    var titleArray = [String]()
    
    let dataDict = NSMutableDictionary()
    var cell = PopUpCell()
    
    @IBOutlet weak var headerTitle: UILabel!{
        didSet{
           headerTitle.text = forView == "account" ? "Reset Account" : forView == "password" ? "Reset Password" : forView == "finish" ? "Finish Meeting" : ""
        }
    }
    @IBOutlet weak var timerView: UIView!{
        didSet{
            timerView.isHidden = (forView != "finish")
        }
    }
    
    @IBOutlet weak var lblTimer: UILabel!{
        didSet{
            DispatchQueue.main.async {
                self.setDateTimer(metinngTime: self.checkListResponseMeeting, lblTimer: self.lblTimer)
            }
        }
    }
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = forView == "account" ? "Reset Account" : forView == "password" ? "Reset Password" : forView == "finish" ? "Finish Meeting" : ""
        self.navigationController?.navigationBar.tintColor = UIColor.white
        
        titleArray = forView == "account" ? ["","Email"] : forView == "password" ? ["","Old Password", "New Password"] : forView == "finish" ? ["Meeting Type", "Note"] : [""]

        
        if forView == "finish"{
            getMeetingTypeListing()
        }
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidLayoutSubviews(){
        tableView.frame = CGRect(x: tableView.frame.origin.x, y:  tableView.frame.origin.y, width: tableView.frame.size.width, height: tableView.contentSize.height)
        tableView.reloadData()
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    //MARK:- UITableViewDelegate
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return titleArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == 0 && self.forView != "finish"{
         cell = tableView.dequeueReusableCell(withIdentifier: "imageCell", for: indexPath) as! PopUpCell
         cell.imageIcon.image = self.forView == "account" ? #imageLiteral(resourceName: "email-icon") : #imageLiteral(resourceName: "lock-password-icon")
        }else{
        let identifier = (self.forView == "finish" && indexPath.row == 1) ? "textViewCell" : "textFieldCell"
        
        cell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath) as! PopUpCell
        cell.titleLbl.text = titleArray[indexPath.row]
        
        
        if forView == "account"{
            cell.textField.text = dataDict.value(forKey: "email") as? String ?? ""
            cell.dropDownBtn.isHidden = true
            cell.textField.isSecureTextEntry = false
            cell.textField.delegate = self
            
        }else if forView == "password"{
            let key = indexPath.row == 1 ? "oldPassword" : "newPassword"
            cell.textField.text = dataDict.value(forKey: key) as? String ?? ""
             cell.dropDownBtn.isHidden = true
            cell.textField.isSecureTextEntry = true
            cell.textField.delegate = self
            
        }else{

        if (cell.dropDownBtn != nil) {
            cell.dropDownBtn.isHidden = !(self.forView == "finish" && indexPath.row == 0)
            cell.textField.text = dataDict.value(forKey: "meetingType") as? String ?? ""
        }
        
        if cell.textView != nil{
            cell.textView.layer.borderWidth = 1
            cell.textView.layer.borderColor = UIColor.lightGray.cgColor
            cell.textView.delegate = self
        }
        }
        }
        cell.selectionStyle = .none
        return cell
        
    }
    
    //MARK: IBAction
    
    @IBAction func saveButtonTapped(_ sender: UIButton) {
        self.view.endEditing(true)
        
        let resPonsedata = self.getUserDefaults(key: KeyResponseModel) as! Data
        let responseModel = try! self.jsonDecoder.decode(LoginResponse.self, from: resPonsedata)
        
        if forView == "account"{
            let completeUrl =  Urls.Reset_Email.rawValue + "\(responseModel.user?.id ?? 0)"
            
            if dataDict.value(forKey: "email") != nil {
                let request = ResetAccountRequest(api_token: API_Token, user_token: responseModel.user!.user_token!, email: "\(dataDict.value(forKey: "email") as? String ?? "")")
                
                self.postDataFromNetWithResponse(url: completeUrl, requestData: request, selector: #selector(self.handResetAccountResponse(data:)) )
            }
            
        }else if forView == "password"{
            
            let completeUrl =  Urls.Reset_Password.rawValue + "\(responseModel.user?.id ?? 0)"
         
            if dataDict.value(forKey: "newPassword") != nil && dataDict.value(forKey: "oldPassword") != nil{
            let request = ResetPasswordRequest(api_token: API_Token, user_token: responseModel.user!.user_token!, new_password: "\(dataDict.value(forKey: "newPassword") as? String ?? "")", old_password: "\(dataDict.value(forKey: "oldPassword") as? String ?? "")")
            
            self.postDataFromNetWithResponse(url: completeUrl, requestData: request, selector: #selector(self.handResetPasswordResponse(data:)) )
            }
            
        }else{
            if dataDict.value(forKey: "meetingId") == nil || "\(dataDict.value(forKey: "meetingId") as AnyObject)" == ""{
                self.showSnackBarWithMessage(message:"Meeting type is empty.", color:#colorLiteral(red: 0.7450980544, green: 0.1568627506, blue: 0.07450980693, alpha: 1),interval:intervals.long)
                
                return
            }
            
        let completeUrl =  Urls.Post_Checklist_Answer.rawValue + (self.meetingId) + "/end"
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "HH:mm:ss"
        formatter.timeZone = TimeZone(abbreviation: "UTC")
        let time = formatter.string(from: date)
        

        let request = StopMeetingRequest(api_token: API_Token, user_token: responseModel.user!.user_token!, end_time: time, meeting_type: "\(dataDict.value(forKey: "meetingId") as AnyObject)" , remark_note: dataDict.value(forKey: "note") as? String ?? "")
        
        self.postDataFromNet(url: completeUrl, requestData: request, selector: #selector(self.handStopMeetingResponse(data:)) )
        }
        
    }
    
    @IBAction func dropDownBtnTapped(_ sender: UIButton) {
        
        // The view to which the drop down will appear on
        dropDown.anchorView = sender // UIView or UIBarButtonItem
        // Will set a custom width instead of the anchor view width
        dropDown.width = sender.frame.width
        // The list of items to display. Can be changed dynamically
        var tempArray = [String]()
        for itemDict in (meetingTypeResponse?.data)!{
            tempArray.append(itemDict.meeting_type!)
        }
        dropDown.dataSource = tempArray
        
        // Action triggered on selection
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            self.dataDict.setValue(item, forKey: "meetingType")
            self.dataDict.setValue(self.meetingTypeResponse?.data![index].id, forKey: "meetingId")
            self.tableView.reloadRows(at: [IndexPath(row: 0, section: 0)], with: .none)
            self.dropDown.hide()
            
        }
    
        dropDown.show()
        
    }
    @IBAction func closeBtnTapped(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    //MARK:- Web Service
    
    func getMeetingTypeListing(){
        let resPonsedata = getUserDefaults(key: KeyResponseModel) as! Data
        let responseModel = try! jsonDecoder.decode(LoginResponse.self, from: resPonsedata)
        let token_part = "?api_token=" + API_Token + "&user_token=" + responseModel.user!.user_token!
        self.getDataFromNet(url: Urls.Get_Meeting_Type.rawValue + "\(responseModel.user?.id ?? 79)" + token_part, selector: #selector(self.handleNetResponse(data:)))
        
    }
    
    @objc func handleNetResponse(data:Data?) {
        if let dat = data{
            do{
                meetingTypeResponse = try jsonDecoder.decode(MeetingTypeResponse.self, from: dat)
                if meetingTypeResponse?.res_msg != nil && meetingTypeResponse?.res_code! != 200{
                    self.showSnackBarWithMessage(message: responseModel!.res_msg!, color: #colorLiteral(red: 0.9254902005, green: 0.2352941185, blue: 0.101907857, alpha: 1), interval: .long)
                    return
                }
                if meetingTypeResponse!.data != nil && meetingTypeResponse!.data!.count > 0 {
//                    self.sectionCounts = [Int](repeating: 0, count: checklistRespons!.checklist_data!.count)
//                    self.sectionCounts![0] = 1
//                    self.tbleView.reloadData()
                }else{
//                    self.tbleView.isHidden = true
//                    self.lblNotAvailable.isHidden = false
                }
                
            }catch let err{
                print(err)
            }
        }
    }
    
    @objc func handResetAccountResponse(data:Data?){
        if let dat = data{
            do{
                let meetingDeatail = try jsonDecoder.decode(ResetAccountResponse.self, from: dat)
                if meetingDeatail.res_msg != nil && meetingDeatail.res_code! != 200{
                    self.showSnackBarWithMessage(message: meetingDeatail.res_msg!, color: #colorLiteral(red: 0.9254902005, green: 0.2352941185, blue: 0.101907857, alpha: 1), interval: .long, minusHeight:0)
                    return
                }else {
                    self.dismiss(animated: true, completion: {
                        let messageText = "Your account is reset, Please verify you account and login again."
                        let alert = UIAlertController(title: "Alert", message: messageText, preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action) in
                            
                            self.logoutUserFromAppAPI()
                        }))
                        UIApplication.shared.keyWindow?.rootViewController?.present(alert, animated: true)
                        
                    })
                   
//                    self.showSnackBarWithMessage(message: (meetingDeatail.res_msg)!, color: #colorLiteral(red: 0.2745098174, green: 0.4862745106, blue: 0.1411764771, alpha: 1), interval: .long, minusHeight:0)
                    
                }
            }catch let err{
                print(err)
            }
        }
    }
 
    @objc func handStopMeetingResponse(data:Data?){
        if let dat = data{
            do{
                let meetingDeatail = try jsonDecoder.decode(StartMeetingResponse.self, from: dat)
                if meetingDeatail.res_msg != nil && meetingDeatail.res_code! != 200{
                    self.showSnackBarWithMessage(message: meetingDeatail.res_msg!, color: #colorLiteral(red: 0.9254902005, green: 0.2352941185, blue: 0.101907857, alpha: 1), interval: .long, minusHeight:0)
                    return
                }else if meetingDeatail.data == true{
                    self.showSnackBarWithMessage(message: "Meeting Ended Successfully", color: #colorLiteral(red: 0.2745098174, green: 0.4862745106, blue: 0.1411764771, alpha: 1), interval: .long, minusHeight:0)

                    self.navigationController?.popToRootViewController(animated:true)
                    
                }
            }catch let err{
                print(err)
            }
        }
    }
    
    @objc func handResetPasswordResponse(data:Data?){
        if let dat = data{
            do{
                let meetingDeatail = try jsonDecoder.decode(ResetPasswordResponse.self, from: dat)
                if meetingDeatail.res_msg != nil && meetingDeatail.res_code! != 200{
                    self.showSnackBarWithMessage(message: meetingDeatail.res_msg!, color: #colorLiteral(red: 0.9254902005, green: 0.2352941185, blue: 0.101907857, alpha: 1), interval: .long, minusHeight:0)
                    return
                }else {
                    self.showSnackBarWithMessage(message: (meetingDeatail.res_msg)!, color: #colorLiteral(red: 0.2745098174, green: 0.4862745106, blue: 0.1411764771, alpha: 1), interval: .long, minusHeight:0)
                    self.navigationController?.popViewController(animated: true)
//                    self.dismiss(animated: true, completion: nil)
                }
            }catch let err{
                print(err)
            }
        }
    }
   
    
//    @objc func handLogOutUserResponse(data:Data?){
//        if let dat = data{
//            do{
//
//                let logOutResponse = try jsonDecoder.decode(LogOutUserResponse.self, from: dat)
//                if logOutResponse.res_msg != nil && logOutResponse.res_code! != 200{
//                    self.showSnackBarWithMessage(message: logOutResponse.res_msg!, color: #colorLiteral(red: 0.9254902005, green: 0.2352941185, blue: 0.101907857, alpha: 1), interval: .long, minusHeight:0)
//                    return
//                }else {
//
//                    self.setUserDefaults(key: KeyHasLoggedIn, value: false)
//                    let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
//                    let viewController = mainStoryboard.instantiateViewController(withIdentifier: "LoginController")
//                    UIApplication.shared.keyWindow?.rootViewController = viewController
//
//                    //                    self.showSnackBarWithMessage(message: (meetingDeatail.res_msg)!, color: #colorLiteral(red: 0.2745098174, green: 0.4862745106, blue: 0.1411764771, alpha: 1), interval: .long, minusHeight:0)
//
//                }
//            }catch let err{
//                print(err)
//            }
//        }
//    }
    
    //MARK:- Text view delegates
    func textViewDidEndEditing(_ textView: UITextView) {
        dataDict.setValue(textView.text, forKey: "note")
    }
    

    //MARK:- Uitextfield delegate
    func textFieldDidEndEditing(_ textField: UITextField) {
        let hitpoint = textField.convert(textField.bounds.origin, to: tableView)
        let hitindex = tableView.indexPathForRow(at: hitpoint)
        
        
        if forView == "account"{
            if !textField.isTextFieldEmpty(){
                self.showSnackBarWithMessage(message:"Email is empty.", color:#colorLiteral(red: 0.7450980544, green: 0.1568627506, blue: 0.07450980693, alpha: 1),interval:intervals.long)
                
                return
            }
            if !textField.isValidEmail(){
                self.showSnackBarWithMessage(message:"Enter a valid Email.", color:#colorLiteral(red: 0.7450980544, green: 0.1568627506, blue: 0.07450980693, alpha: 1),interval:intervals.long)
                return
                
            }
            dataDict.setValue(textField.text, forKey: "email")
            
        }else if forView == "password"{
            
            if !textField.isTextFieldEmpty(){
                self.showSnackBarWithMessage(message:"Password is empty.", color:#colorLiteral(red: 0.7450980544, green: 0.1568627506, blue: 0.07450980693, alpha: 1),interval:intervals.long)
                return
            }
            if !textField.isValidTextLength(minValue: 6, maxValue: 15){
                self.showSnackBarWithMessage(message:"Password should be in between 6 and 15 characters", color:#colorLiteral(red: 0.7450980544, green: 0.1568627506, blue: 0.07450980693, alpha: 1),interval:intervals.long)
                return
            }
            let key  = hitindex?.row == 1 ? "oldPassword" : "newPassword"
            dataDict.setValue(textField.text, forKey: key)
            
        }
    }
    
}

class PopUpCell: UITableViewCell{
    
    @IBOutlet weak var imageIcon: UIImageView!
    
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var textField: UITextField!
    
    @IBOutlet weak var dropDownBtn: UIButton!
    @IBOutlet weak var textView: UITextView!
}
