/* 
Copyright (c) 2018 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar

*/

import Foundation
struct Checklists : Codable {
    let id:String?
	let title : String?
	var response : String?
	let response_by : String?
	let remark : String?
	let type : String?
	let input_type : String?
    var flag:Int?
    var from:String?
    var to:String?
    var fromDate:Date?
    var toDate:Date?
    var temResp:String?
    //var InputText:String?
	enum CodingKeys: String, CodingKey {

		case title = "title"
		case response = "response"
		case response_by = "response_by"
		case remark = "remark"
		case type = "type"
		case input_type = "input_type"
        case flag = "flag"
        case id = "id"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		title = try values.decodeIfPresent(String.self, forKey: .title)
		response = try values.decodeIfPresent(String.self, forKey: .response)
		response_by = try values.decodeIfPresent(String.self, forKey: .response_by)
		remark = try values.decodeIfPresent(String.self, forKey: .remark)
		type = try values.decodeIfPresent(String.self, forKey: .type)
		input_type = try values.decodeIfPresent(String.self, forKey: .input_type)
        flag = try values.decodeIfPresent(Int.self, forKey: .flag)
        id =   try values.decodeIfPresent(String.self, forKey: .id)
        temResp = try values.decodeIfPresent(String.self, forKey: .response)
        
        switch input_type {
        case InputType.Input_type_date.rawValue:
            from =  response ?? ""
            break
        case InputType.Input_type_time.rawValue:
            from =  response ?? ""
            break
        case InputType.Input_type_datetime.rawValue:
            from =  response ?? ""
            break
        case InputType.Input_type_date_range.rawValue:
            if response != nil{
                from = response!.components(separatedBy: "#").first!
                to = response!.components(separatedBy: "#").last!
            }
            break
        case InputType.Input_type_time_range.rawValue:
            if response != nil{
                from = response!.components(separatedBy: "#").first!
                to = response!.components(separatedBy: "#").last!
            }
            break
        default:
            break
        }
	}

}
