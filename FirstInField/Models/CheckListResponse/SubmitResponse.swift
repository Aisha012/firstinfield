//
//  SubmitResponse.swift
//  FirstInField
//
//  Created by Namespace  on 07/07/18.
//  Copyright © 2018 Namespace . All rights reserved.
//

import Foundation
struct SubmitResponse : Codable {
    var res_code : Int?
    var res_msg : String?

    enum CodingKeys: String, CodingKey {
        case res_code = "res_code"
        case res_msg = "res_msg"
     }

    init(from decoder: Decoder) throws {
        var values = try decoder.container(keyedBy: CodingKeys.self)
        res_code = try values.decodeIfPresent(Int.self, forKey: .res_code)
        res_msg = try values.decodeIfPresent(String.self, forKey: .res_msg)
        }
}
