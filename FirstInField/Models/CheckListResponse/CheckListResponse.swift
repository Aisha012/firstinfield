/* 
Copyright (c) 2018 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar

*/

import Foundation
struct CheckListResponse : Codable {
	var res_code : Int?
	var res_msg : String?
    let meeting : Meeting?
	var checklist_data : [Checklist_data]?

	enum CodingKeys: String, CodingKey {

		case res_code = "res_code"
		case res_msg = "res_msg"
        case meeting = "meeting"
		case checklist_data = "checklist_data"
	}

	init(from decoder: Decoder) throws {
		var values = try decoder.container(keyedBy: CodingKeys.self)
		res_code = try values.decodeIfPresent(Int.self, forKey: .res_code)
		res_msg = try values.decodeIfPresent(String.self, forKey: .res_msg)
        checklist_data = try values.decodeIfPresent([Checklist_data].self, forKey: .checklist_data)
        meeting = try values.decodeIfPresent(Meeting.self, forKey: .meeting)
		
	}

}


struct MeetingTypeResponse : Codable {
    var res_code : Int?
    var res_msg : String?
    var data : [MeetingType]?
   
    
    enum CodingKeys: String, CodingKey {
        
        case res_code = "res_code"
        case res_msg = "res_msg"
        case data = "data"
    }
    
    init(from decoder: Decoder) throws {
        var values = try decoder.container(keyedBy: CodingKeys.self)
        res_code = try values.decodeIfPresent(Int.self, forKey: .res_code)
        res_msg = try values.decodeIfPresent(String.self, forKey: .res_msg)
        data = try values.decodeIfPresent([MeetingType].self, forKey: .data)
    }
    
}

struct MeetingType : Codable {
    let id : Int?
    let meeting_type : String?
    
    enum CodingKeys: String, CodingKey {
        
        case id = "id"
        case meeting_type = "meeting_type"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(Int.self, forKey: .id)
        meeting_type = try values.decodeIfPresent(String.self, forKey: .meeting_type)
    }
    
}
