//
//  LoginRequest.swift
//  3C
//
//  Created by Namespace  on 07/06/18.
//  Copyright © 2018 Namespace . All rights reserved.
//

import Foundation
struct LoginRequest: Codable {
    var email: String
    var password: String
    var api_token:String
}


struct DeviceTokeRequest: Codable {
    var user_id: String?
    var notification_status: String?
    var device_id: String?
    var device_token: String?
    var device_type: String?
    var device_name: String?
    var country_code: String?

}
