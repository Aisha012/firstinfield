//
//  SignupRequest.swift
//  3C
//
//  Created by Namespace  on 09/06/18.
//  Copyright © 2018 Namespace . All rights reserved.
//

import Foundation

struct SignupRequest: Codable {
    var email: String
    var password: String?
    var first_name:String?
    var last_name:String?

    enum CodingKeys: String, CodingKey {
        case password = "user[password]"
        case email = "user[email]"
        case first_name = "user[first_name]"
        case last_name = "user[last_name]"
    }
   

}
