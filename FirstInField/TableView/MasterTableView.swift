//
//  MasterTableView.swift
//  CommonArchitecture
//
//  Created by Madhav on 7/21/16.
//  Copyright © 2016 Madhav. All rights reserved.
//

import UIKit

protocol MasterTableViewProtocol:class {
   func refresh(_ sender:UIRefreshControl)
}

extension UIViewController:MasterTableViewProtocol{
   @objc func refresh(_ sender:UIRefreshControl){
        
    }
}








@IBDesignable
class MasterTableView: UITableView, UIGestureRecognizerDelegate {
   internal var refreshControll: UIRefreshControl!
   weak var delegat:MasterTableViewProtocol!

    override func draw(_ rect: CGRect) {
       // self.addObserver(self, forKeyPath: "contentOffset", options: NSKeyValueObservingOptions.new, context: nil)
    }
    
   
    
    @IBInspectable var addRefreshControll:Bool = false{
        didSet{
            if addRefreshControll == true{
                refreshControll = UIRefreshControl()
                refreshControll.tintColor = UIColor.white
                refreshControll.attributedTitle = NSAttributedString(string: ""/*"Pull to refresh"*/)
                refreshControll.addTarget(self, action: #selector(MasterTableView.refresh(_:)), for: UIControlEvents.valueChanged)
                self.addSubview(refreshControll)
               
                
            }else{
                if refreshControll != nil{
                    refreshControll.removeFromSuperview()
                    refreshControll = nil
                }
            }
        }
    }
   
    
    @objc func refresh(_ sender:UIRefreshControl) {
        if self.delegate != nil{
            let controller:UIViewController = self.delegate! as!
            UIViewController
            self.delegat = controller
            self.delegat.refresh(sender)
        }
    }
        
    
}
