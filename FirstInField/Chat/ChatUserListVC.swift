//
//  ChatUserListVC.swift
//  FirstInField
//
//  Created by Namespace  on 05/09/18.
//  Copyright © 2018 Namespace . All rights reserved.
//

import UIKit

class ChatUserListVC: UIViewController , UITableViewDelegate , UITableViewDataSource  {
    var meetingID:String = ""
    var meetingName:String = ""
    var selectedIndex = 0
    var isRefreshing:Bool = false
    var conversationResponse:MeetingResponse?
    var chatuserdata : [ChatUserData]?
    var pageIndex = 1
    
    @IBOutlet weak var tbleView: UITableView!
    @IBOutlet weak var btnByUser: UIButton!
    @IBOutlet weak var vwFirstSegment: UIView!
    @IBOutlet weak var vwLastSegment: UIView!
    @IBOutlet weak var btnByMeeting: UIButton!
    @IBOutlet weak var centerLbl: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "Conversations"
        self.btnByMeeting.setTitle(meetingName, for: .normal)
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
            pageIndex = 1
            conversationResponse = nil
            self.tbleView.reloadData()
            getChatUsers()
    }
    
    
    //MARK:- Web Service
    func getChatUsers(){
        let resPonsedata = self.getUserDefaults(key: KeyResponseModel) as! Data
        let responseModel = try! self.jsonDecoder.decode(LoginResponse.self, from: resPonsedata)
        var completeUrl = ""
        completeUrl  =  selectedIndex == 0 ?  Urls.Get_Conversation_By_Meeting.rawValue + "\(meetingID)" + "?user_id=\(responseModel.user!.id!)" + "&apitoken=" + API_Token + "&user_token=" + responseModel.user!.user_token! : Urls.Get_Conversation_By_User.rawValue + "\(responseModel.user!.id!)" + "?apitoken=" + API_Token + "&user_token=" + responseModel.user!.user_token!
        
        self.getDataFromNet(url: completeUrl, selector:#selector(self.handleChatUserResponse(data:) ), settoken:true)
    }
    
    
    @objc func handleChatUserResponse(data:Data?) {
        if let dat = data{
            do{
                let response  = try jsonDecoder.decode(ChatUserResponse.self, from: dat)
                if response.res_msg != nil && response.res_code! != 200{
                    self.showSnackBarWithMessage(message: response.res_msg!, color: #colorLiteral(red: 0.9254902005, green: 0.2352941185, blue: 0.1019607857, alpha: 1), interval: .long)
                    return
                }
                if let usr = response.chatuserdata{
                    chatuserdata = usr
                    centerLbl.isHidden = chatuserdata?.count != 0
                    self.tbleView.reloadData()
                }
            }catch let err{
                print(err)
            }
        }
    }
    
    
    
    
    
    //MARK:- UITableViewDelegate
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return chatuserdata != nil ? chatuserdata!.count : 0
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell : ChatTableCell!
        cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! ChatTableCell
        cell.userIsActiveView.layer.cornerRadius = 5.0
        
        var nameArr = [String]()
        let userArr = chatuserdata?[indexPath.row].users
        for dict in userArr!{
            nameArr.append(dict.name!)
        }
        cell.userName.text = nameArr.count != 0 ? nameArr.joined(separator: ", ") : "N/A"

        cell.chatMessage.text = chatuserdata?[indexPath.row].last_message?.message ?? ""
       
        if (chatuserdata?[indexPath.row].last_message?.lastsendedon) != nil {
        let localDate = UTCToLocal(date: (chatuserdata?[indexPath.row].last_message?.lastsendedon)!, fromFormat: "yyyy-MM-dd HH:mm:ss", toFormat: "yyyy-MM-dd HH:mm:ss")
        let date = getDateFromString(dateString: localDate, fromFormat: "yyyy-MM-dd HH:mm:ss")
        cell.dateTime.text = configureToShowDateFormatter(for: date)
        }else{
           cell.dateTime.text = ""
        }
       
        
        cell.lblText.text = chatuserdata?[indexPath.row].last_message?.lastsendedon?.components(separatedBy: " ").last ?? "N/A"
        cell.userIsActiveView.backgroundColor = chatuserdata?[indexPath.row].last_message?.status == "Unread" ? UIColor.red : UIColor.clear
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let controller = ConversationViewController()
        controller.conversationId = "\(chatuserdata![indexPath.row].conversation_id ?? 0)"
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    //MARK:- IBAction
    @IBAction func btnOnTapSegmentButton(_ sender: UIButton) {
        switch sender.tag {
        case 0:
            vwFirstSegment.backgroundColor = #colorLiteral(red: 0.001829766668, green: 0.2451805174, blue: 0.3752157092, alpha: 1) //#colorLiter: 0.001829766668, green: 0.245180var4, blue: 0.3752157092, alpha: 1)
            vwLastSegment.backgroundColor = #colorLiteral(red: 0, green: 0.3599842191, blue: 0.4917289615, alpha: 1)
            btnByUser.setTitleColor(#colorLiteral(red: 0.5416718721, green: 0.6478435993, blue: 0.7340455651, alpha: 1), for: .normal)
            btnByMeeting.setTitleColor(#colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), for: .normal)
            selectedIndex = 0
//            if self.conversationResponse == nil {
                chatuserdata?.removeAll()
                getChatUsers()
//            }else{
//                self.tbleView.reloadData()
//                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.1) {
//                    let indexpath = IndexPath(row: 0, section: 0)
//                    self.tbleView.scrollToRow(at: indexpath, at: .top, animated: true)
//                }
//            }
            break
        case 1:
            vwFirstSegment.backgroundColor = #colorLiteral(red: 0, green: 0.3599842191, blue: 0.4917289615, alpha: 1)
            vwLastSegment.backgroundColor = #colorLiteral(red: 0.001829766668, green: 0.2451805174, blue: 0.3752157092, alpha: 1)
            btnByMeeting.setTitleColor(#colorLiteral(red: 0.5416718721, green: 0.6478435993, blue: 0.7340455651, alpha: 1), for: .normal)
            btnByUser.setTitleColor(#colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), for: .normal)
            selectedIndex = 1
//            if self.conversationResponse == nil {
                chatuserdata?.removeAll()
               getChatUsers()
//            }else{
//                self.tbleView.reloadData()
//                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.1) {
//                    let indexpath = IndexPath(row: 0, section: 0)
//                    self.tbleView.scrollToRow(at: indexpath, at: .top, animated: true)
//                }
//            }
            break
        default:
            break
        }
        
        
    }
    
    //MARK:- Helping Func
    
    override func refresh(_ sender: UIRefreshControl) {
            self.pageIndex = 1
            isRefreshing = true
            self.getChatUsers()
            sender.endRefreshing()
    }
    
}

class ChatTableCell : UITableViewCell
{
    @IBOutlet weak var userImage: UIView!
    
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var chatMessage: UILabel!
    
    @IBOutlet weak var dateTime: UILabel!
    @IBOutlet weak var cityLbl: UILabel!
    @IBOutlet weak var lblText: UILabel!
    @IBOutlet weak var userIsActiveView: UIView!
    
}
