//
//  ViewController.swift
//  FirstInField
//
//  Created by Namespace  on 12/06/18.
//  Copyright © 2018 Namespace . All rights reserved.
//

import UIKit
var responseModel:LoginResponse?
protocol DeviceTokenApi:class{
    func callDeviceTokenApi()
}



class ViewController: UIViewController {
    
    @IBOutlet weak var txtUsername: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var btnLogin: UIButton!
    @IBOutlet weak var btnForgot: UIButton!
    @IBOutlet weak var vwUserName: UIView!
    @IBOutlet weak var vwPassword: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
     
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func btnOnTapLogin(_ sender: UIButton) {
        print(sender.isSelected)
        self.view.endEditing(true)
        if validateFields(){
            let request = LoginRequest(email: txtUsername.text!, password: txtPassword.text!, api_token:API_Token)
            self.postDataFromNet(url: Urls.Login.rawValue, requestData: request, selector: #selector(ViewController.handleNetResponse(data:)))
        }
    }
    
    override func viewDidLayoutSubviews() {
        self.vwPassword.layer.cornerRadius = self.vwPassword.frame.size.height/2
        self.vwUserName.layer.cornerRadius = self.vwUserName.frame.size.height/2
        self.btnLogin.layer.cornerRadius = self.btnLogin.frame.size.height/2
        self.btnLogin.layer.borderWidth = 2
        self.btnLogin.layer.borderColor = UIColor.white.cgColor
    }
    
     @IBAction func btnonTapForgot(btn:UIButton){
    }
    
    
    func validateFields()->Bool{
        if !self.txtUsername.isTextFieldEmpty(){
            self.showSnackBarWithMessage(message:"Email is empty.", color:#colorLiteral(red: 0.7450980544, green: 0.1568627506, blue: 0.07450980693, alpha: 1),interval:intervals.long)

            return false
        }
        if !self.txtUsername.isValidEmail(){
            self.showSnackBarWithMessage(message:"Enter a valid Email.", color:#colorLiteral(red: 0.7450980544, green: 0.1568627506, blue: 0.07450980693, alpha: 1),interval:intervals.long)
            return false

        }
        if !self.txtPassword.isTextFieldEmpty(){
            self.showSnackBarWithMessage(message:"Enter Password", color:#colorLiteral(red: 0.7450980544, green: 0.1568627506, blue: 0.07450980693, alpha: 1),interval:intervals.long)
            return false
        }
        if !txtPassword.isValidTextLength(minValue: 6, maxValue: 15){
            self.showSnackBarWithMessage(message:"Invalid Password", color:#colorLiteral(red: 0.7450980544, green: 0.1568627506, blue: 0.07450980693, alpha: 1),interval:intervals.long)
            return false
        }
        return true
    }
    
    @objc func handleNetResponse(data:Data?) {
        if let dat = data{
            do{
                responseModel = try jsonDecoder.decode(LoginResponse.self, from: dat)
                if responseModel?.res_msg != nil && responseModel?.res_code! != 200 {
                    self.showSnackBarWithMessage(message: responseModel!.res_msg!, color: #colorLiteral(red: 0.9254902005, green: 0.2352941185, blue: 0.1019607857, alpha: 1), interval: .long)
                    return
                }
                setUserDefaults(key: KeyResponseModel, value: dat)
                setUserDefaults(key: KeyHasLoggedIn, value: true)
                setUserDefaults(key: Setting_Status, value: responseModel?.user?.setting_status ?? 0)
                setUserDefaults(key: Meeting_Count, value: responseModel?.user?.meeting_count ?? 0)
                setUserDefaults(key: Adhoc_Count, value: responseModel?.user?.adhoc_count ?? 0)
                
                changeRootController()
                appDelegate.connectToSocket()
            }catch let err {
                print(err)
            }
        }
    }
    
    
    @IBAction func touchDragInside(btn:UIButton){
        print("inside")
    }
  
    @IBAction func touchDragoutside(btn:UIButton){
        print("outside")
    }
    
    //Mark:- TextField Delegates
    
//    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
//        let currentText = textField.text ?? ""
//        guard let stringRange = Range(range, in: currentText) else { return false }
//        let updatedText = currentText.replacingCharacters(in: stringRange, with: string)
//        if textField == txtPassword{
//            if txtUsername.isTextFieldEmpty() && txtPassword.isTextFieldEmpty() && updatedText.count > 5 && updatedText.count < 16{
//                self.btnLogin.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
//                self.btnLogin.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
//                self.btnLogin.isUserInteractionEnabled = true
//            }else{
//                self.btnLogin.backgroundColor = UIColor.clear
//                self.btnLogin.setTitleColor(#colorLiteral(red: 1, green: 1, blue: 1, alpha: 1), for: .normal)
//                self.btnLogin.isUserInteractionEnabled = false
//            }
//        }
//        if textField == txtUsername{
//            if updatedText.count <= 0{
//                self.btnLogin.backgroundColor = UIColor.clear
//                self.btnLogin.setTitleColor(#colorLiteral(red: 1, green: 1, blue: 1, alpha: 1), for: .normal)
//                self.btnLogin.isUserInteractionEnabled = false
//            }else{
//                if txtUsername.isTextFieldEmpty() && txtPassword.isTextFieldEmpty() && (txtPassword.text?.count)! > 5{
//                    self.btnLogin.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
//                    self.btnLogin.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
//                    self.btnLogin.isUserInteractionEnabled = true
//                }else{
//                    self.btnLogin.backgroundColor = UIColor.clear
//                    self.btnLogin.setTitleColor(#colorLiteral(red: 1, green: 1, blue: 1, alpha: 1), for: .normal)
//                    self.btnLogin.isUserInteractionEnabled = false
//                }
//            }
//        }
//
//        return true
//    }
    
    func changeRootController(){
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let viewController = mainStoryboard.instantiateViewController(withIdentifier: "tabBarcontroller")
        UIApplication.shared.keyWindow?.rootViewController = viewController
    }
    
}

