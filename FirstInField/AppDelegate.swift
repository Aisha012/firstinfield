//
//  AppDelegate.swift
//  FirstInField
//
//  Created by Namespace  on 12/06/18.
//  Copyright © 2018 Namespace . All rights reserved.
//
import GoogleMaps
import GooglePlaces
import UIKit
import CoreData
import IQKeyboardManagerSwift
import Firebase
import UserNotifications
import Fabric
import Crashlytics
import GooglePlaces
import GooglePlacePicker
import GoogleMaps
import SocketIO


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate,UNUserNotificationCenterDelegate {
    var compString:String?
    var window: UIWindow?
    weak var protoDel:NotiRequestProtocol?
    weak var tokenApi:DeviceTokenApi?
    var manager : SocketManager!
    var socket : SocketIOClient!
    

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        Thread.sleep(forTimeInterval: 3.0)
        GMSServices.provideAPIKey("AIzaSyCyDO3ZbMIQe9qsweE81e0DbPdI98xyPkI")
        GMSPlacesClient.provideAPIKey("AIzaSyCyDO3ZbMIQe9qsweE81e0DbPdI98xyPkI")
        IQKeyboardManager.shared.enable = true
        FirebaseApp.configure()
        Fabric.with([Crashlytics.self])
        let cancelButtonAttributes: [NSAttributedStringKey: Any] = [NSAttributedStringKey(rawValue: NSAttributedStringKey.foregroundColor.rawValue): UIColor.white]
        UIBarButtonItem.appearance(whenContainedInInstancesOf: [UISearchBar.self]).setTitleTextAttributes(cancelButtonAttributes, for: .normal)
        
        
        UITabBar.appearance().tintColor = #colorLiteral(red: 0.1411764706, green: 0.6156862745, blue: 0.7647058824, alpha: 1)//UIColor(red: 0/255.0, green: 114/255.0, blue: 147/255.0, alpha: 1.0)
        //UITabBarItem.appearance().setTitleTextAttributes(<#T##attributes: [NSAttributedStringKey : Any]?##[NSAttributedStringKey : Any]?#>, for: <#T##UIControlState#>)
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedStringKey.foregroundColor: #colorLiteral(red: 0.1960784314, green: 0.7568627451, blue: 0.937254902, alpha: 1)], for: .selected)
        UINavigationBar.appearance().barTintColor =  #colorLiteral(red: 0.003921568627, green: 0.2156862745, blue: 0.5215686275, alpha: 1)
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedStringKey.foregroundColor:UIColor.white, NSAttributedStringKey.font:UIFont(name: "Montserrat-SemiBold", size: 20) as Any]
       
        UIApplication.shared.statusBarStyle = .lightContent
        
//      registerForPushNotifications
        if #available(iOS 10.0, *) {
            UNUserNotificationCenter.current().delegate = self
            UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .badge, .sound]) { (granted, error) in
                print("granted: (\(granted)")
            }
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
            // Fallback on earlier versions
        }
        application.registerForRemoteNotifications()
        application.applicationIconBadgeNumber = Badge_Count
        
        
        if let option = launchOptions {
            let info = option[UIApplicationLaunchOptionsKey.remoteNotification]
            if (info != nil) {
               // goAnotherVC()
                compString = "launchoptions"

            }}
        
        if (UserDefaults.standard.value(forKey: KeyHasLoggedIn) as? Bool == true){
            changeRootController()
            connectToSocket()
        }
        
        if let documentsPath = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first?.path {
            print("Documents Directory: \(documentsPath)")
        }
        
        let deviceID  =  UIDevice.current.identifierForVendor!.uuidString
        UserDefaults.standard.set(deviceID, forKey: KEY_DEVICE_ID)
        UserDefaults.standard.synchronize()
        
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
    print("going  tooooo background")
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        print("did enter foreground")
        self.protoDel?.appDidEnterForeGROUND()
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        print("applicationDidBecomeActive")

        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
        self.saveContext()
    }

    // MARK: - Core Data stack

    @available(iOS 10.0, *)
    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
        */
        let container = NSPersistentContainer(name: "FirstInField")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                 
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()

    // MARK: - Core Data Saving support

    func saveContext () {
        if #available(iOS 10.0, *) {
            let context = persistentContainer.viewContext
            if context.hasChanges {
                do {
                    try context.save()
                } catch {
                    // Replace this implementation with code to handle the error appropriately.
                    // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                    let nserror = error as NSError
                    fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
                }
            }
        } else {
            // Fallback on earlier versions
        }
        
    }
    
    func changeRootController(){
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let viewController = mainStoryboard.instantiateViewController(withIdentifier: "tabBarcontroller") 
        self.window?.rootViewController = viewController
        self.window?.makeKeyAndVisible()
    }
    
    //MARK:- Notification
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        let token = deviceToken.map { String(format: "%02.2hhx", $0) }.joined()
        UserDefaults.standard.set(token, forKey: KEY_DEVICE_TOKEN)
        UserDefaults.standard.synchronize()
        tokenApi?.callDeviceTokenApi()
        print("token: \(token)")
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("failed to register for remote notifications with with error: \(error)")
    }
    
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        print("handling notification")
        compString = "handling notification"
        
        let userInfo = response.notification.request.content.userInfo
        
        if userInfo is [String:AnyObject] {
           
            if let root = self.window?.rootViewController {
                for cont in root.childViewControllers{
                    if let con = cont as? UITabBarController{
                        let tabcontroller = con
                        let nav  = tabcontroller.childViewControllers[1] as! UINavigationController
                        let story =   UIStoryboard(name: "CheckList", bundle: nil)
                        
                        let notilist = story.instantiateViewController(withIdentifier: "NotiListVC") as! NotiListVC
                        guard tabcontroller.selectedIndex != 1 else{
                           break
                        }
                        tabcontroller.selectedIndex = 1
                        
                        nav.pushViewController(notilist, animated: true)
                        break
                    }
                }
               
            }
        }
        completionHandler()
    }
    
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                willPresent notification: UNNotification,
                                withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        let userInfo = notification.request.content.userInfo
        // Print full message.
        print(userInfo)
        print(notification.request.content)
        
//        if let topController = UIApplication.topViewController() {
//            if topController is ContainerVC {
                NotificationCenter.default.post(name: Notification.Name("refreshMeeting"), object: nil)
//            }
//            
//        }
        // Change this to your preferred presentation option
        completionHandler(UNNotificationPresentationOptions.alert)
    }
    
    private func parseRemoteNotification(notification:[String:AnyObject]) -> String? {
        if let identifier = notification["identifier"] as? String {
            return identifier
        }
        
        return nil
    }

    open func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any]) {
        
        switch application.applicationState {
        case .active:
            print("do stuff in case App is active")
            compString = " active"

        case .background:
            print("do stuff in case App is in background")
            compString = " background"

        case .inactive:
            print("App is inactive")
            compString = "  inactive"

        }
    }
    
    //MARK:- Connect Socket
    func connectToSocket(){
        manager = SocketManager(socketURL: URL(string: Urls.FIF_Socket.rawValue)!, config: [.log(true), .compress, .forceNew(true)])
        socket = manager.defaultSocket
        connectToCompanyChannel()
        
        socket.connect()
    }
    
    func connectToCompanyChannel(){
        let resPonsedata = UserDefaults.standard.object(forKey: KeyResponseModel) as! Data
        let responseModel = try! JSONDecoder().decode(LoginResponse.self, from: resPonsedata)
        let token = responseModel.user!.user_token!
        
        let headers = ["Authorization": "Bearer " + token,
                       "User": responseModel.user?.id ?? 0] as [String : Any]
        let tempHeader = ["headers":headers]
        let subscribeData = ["channel": "presence-company.\(responseModel.user?.company_id ?? 0)",
            "name": "subscribe",
            "auth": tempHeader] as [String : Any]
        
        socket.on(clientEvent: .connect) {data, ack in
            print("socket connected")
            
            self.socket.emitWithAck("subscribe", subscribeData).timingOut(after: 2, callback: {data in
                print("subscribed")
                
            })
            
        }
        
    }
    
}

