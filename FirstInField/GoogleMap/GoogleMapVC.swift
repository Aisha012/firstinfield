//
//  GoogleMapVC.swift
//  FirstInField
//
//  Created by Namespace  on 25/06/18.
//  Copyright © 2018 Namespace . All rights reserved.
//
import GoogleMaps
import UIKit

class GoogleMapVC: UIViewController {

    var srcLoc:CLLocationCoordinate2D!
    var dstLoc:CLLocationCoordinate2D!
    override func loadView() {
        // Create a GMSCameraPosition that tells the map to display the
        // coordinate -33.86,151.20 at zoom level 6.
        let camera = GMSCameraPosition.camera(withLatitude: -33.86, longitude: 151.20, zoom: 6.0)
        let mapView = GMSMapView.map(withFrame: CGRect.zero, camera: camera)
        view = mapView
        
        // Creates a marker in the center of the map.
        let marker = GMSMarker()
        marker.position = CLLocationCoordinate2D(latitude: -33.86, longitude: 151.20)
        marker.title = "Sydney"
        marker.snippet = "Australia"
        marker.map = mapView
        navigationItem.leftBarButtonItem?.tintColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)

        self.draw(src: srcLoc, dst: dstLoc)
    }
    
    
    
    func draw(src: CLLocationCoordinate2D, dst: CLLocationCoordinate2D){
        
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        
        let url = URL(string: "https://maps.googleapis.com/maps/api/directions/json?origin=\(src.latitude),\(src.longitude)&destination=\(dst.latitude),\(dst.longitude)&sensor=false&mode=driving&key=AIzaSyD-l3-JrcGUpioZYnV4aO7i1D5Q1Ubf2RM")!
        
        let task = session.dataTask(with: url, completionHandler: {
            (data, response, error) in
            if error != nil {
                print(error!.localizedDescription)
            } else {
                DispatchQueue.main.async {
                    do {
                        if let json : [String:Any] = try JSONSerialization.jsonObject(with: data!, options: .allowFragments) as? [String: Any] {
                            print(json)
                            let preRoutes = json["routes"] as! NSArray
                            if preRoutes.count == 0{
                                self.showSnackBarWithMessage(message: "No Directions Available", color: UIColor.red, txtColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), interval: .long, minusHeight: 60)
                                return
                            }
                            let routes = preRoutes[0] as! NSDictionary
                            let routeOverviewPolyline:NSDictionary = routes.value(forKey: "overview_polyline") as! NSDictionary
                            let polyString = routeOverviewPolyline.object(forKey: "points") as! String
                            
                            DispatchQueue.main.async(execute: {
                                
                                let bounds = GMSCoordinateBounds(coordinate: src, coordinate: dst)
                                let update = GMSCameraUpdate.fit(bounds, with: UIEdgeInsetsMake(170, 30, 30, 30))
                                (self.view as! GMSMapView).moveCamera(update)
                                let path = GMSPath(fromEncodedPath: polyString)
                                let polyline = GMSPolyline(path: path)
                                polyline.strokeWidth = 5.0
                                polyline.strokeColor = UIColor.red
                                polyline.map = self.view as? GMSMapView
                            })
                        }
                        
                    } catch {
                        print("parsing error")
                    }
                }
            }
        })
        task.resume()
    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
      //  self.navigationItem.title = "Google Map"

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
