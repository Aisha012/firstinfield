//
//  UrlFile.swift
//  3C
//
//  Created by Namespace  on 08/06/18.
//  Copyright © 2018 Namespace . All rights reserved.
//

import Foundation

let API_Token = "c29898a62462fc3041ffce7084363832"
//http://dashboard.firstinfield.com/api/user_running_meeting?user_id=79&api_token=c29898a62462fc3041ffce7084363832&user_token=&page=1
//http://dashboard.firstinfield.com/api/adhoc/79?api_token=c29898a62462fc3041ffce7084363832&user_token=$2y$10$bcdJzcVPS1pltpgSYdgR8.TZ1NKvczwD1Z4XJyzTqXsNT1x5hEwXO&page=1
//"http://dashboard.firstinfield.com/api/adhoc/79?api_token=c29898a62462fc3041ffce7084363832&user_token=$2y$10$bcdJzcVPS1pltpgSYdgR8.TZ1NKvczwD1Z4XJyzTqXsNT1x5hEwXO&page=1"

enum Urls:String{
    case FIF_Socket = "http://dashboard.firstinfield.com:6001"
    case Login = "http://dashboard.firstinfield.com/api/login"
    case forgot = "http://dashboard.firstinfield.com/api/forgotpassword"
    case Get_Upcoming_Meetings = "http://dashboard.firstinfield.com/api/user_meeting"
    case Get_Past_Meetings = "http://dashboard.firstinfield.com/api/user_past_meeting"
    case Get_CheckList = "http://dashboard.firstinfield.com/api/checklist?meeting_id="
    case Post_Checklist_Answer = "http://dashboard.firstinfield.com/api/meeting/"
    case Get_Meeting_Detail = "http://dashboard.firstinfield.com/api/meeting_details/"
    case Get_QusetionList = "http://dashboard.firstinfield.com/api/question?meeting_id="
   // case Post_Start_Meeting = "http://dashboard.firstinfield.com/api/meeting/"
    case Post_Device_Token = "http://dashboard.firstinfield.com/api/post/device_registration"
    case Get_Chat_Users   = "http://dashboard.firstinfield.com/api/coachbox/"
    case Upload_Doc = "http://dashboard.firstinfield.com/api/add_doc"
    case Get_Current_Meeting = "http://dashboard.firstinfield.com/api/user_running_meeting"
    case Get_Adhoc_Meeting  = "http://dashboard.firstinfield.com/api/adhoc/"
    case Get_Rep_list = "http://dashboard.firstinfield.com/api/representatives/"
    case Post_Save_Meeting = "http://dashboard.firstinfield.com/api/save_meeting"
    case Check_Adhoc_Meeting  = "http://dashboard.firstinfield.com/api/is-adhoc/"
    case toggleStatus  = "http://dashboard.firstinfield.com/api/toggle-status/"
    case Get_Adhoc_Checklist  = "http://dashboard.firstinfield.com/api/get-checklist/"
    case Get_Adhoc_QusetionList  = "http://dashboard.firstinfield.com/api/get-questions/"
    case Get_Meeting_Type  = "http://dashboard.firstinfield.com/api/meeting-type/"
    case Reset_Password  = "http://dashboard.firstinfield.com/api/reset-password/"
    case Reset_Email  = "http://dashboard.firstinfield.com/api/reset-email/"
    case Notification_listing  = "http://dashboard.firstinfield.com/api/notifications"
    case Logout_user = "http://dashboard.firstinfield.com/api/logout/"
    
    //    ConversationByUser
    case Get_Conversation_By_User = "http://dashboard.firstinfield.com/api/user-chatlist/"
    
    //    ConversationByMeeting
    case Get_Conversation_By_Meeting = "http://dashboard.firstinfield.com/api/meeting-chatlist/"
    
    //    Conversation Messages
    case Get_Conversation_Data = "http://dashboard.firstinfield.com/api/conversation-messagelist/"
    
    //    Send Messages
    case Post_Conversation_Data = "http://dashboard.firstinfield.com/api/conversation-sendMessage/"

}
