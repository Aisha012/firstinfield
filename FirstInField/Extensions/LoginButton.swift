//
//  LoginButton.swift
//  FirstInField
//
//  Created by Namespace  on 14/06/18.
//  Copyright © 2018 Namespace . All rights reserved.
//

import UIKit

@IBDesignable
class LoginButton: UIButton {

    @IBInspectable var selectedBackgroundColor:UIColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
    @IBInspectable var unselectedBackgroundColor:UIColor = UIColor.clear
    
    @IBInspectable var selectedTitleColor:UIColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
    @IBInspectable var unselectedTitleColor:UIColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
    
    @IBInspectable var unselectedBorderColor:UIColor = UIColor.clear
    var row:Int!
    var section:Int!
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    override func willChangeValue(forKey key: String) {
        print(key)
    }
    
   
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {

       self.backgroundColor = selectedBackgroundColor //#colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        self.setTitleColor(selectedTitleColor, for: .normal)
//        self.layer.borderColor = UIColor.clear.cgColor

        UIButton.animate(withDuration: 0.2,
                         animations: {
                            self.transform = CGAffineTransform(scaleX: 0.975, y: 0.96)
        })
    }
    
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        print(self.isSelected)
       
        self.backgroundColor = unselectedBackgroundColor
        self.layer.borderColor = unselectedBorderColor.cgColor
//        self.layer.borderColor = borderColor?.cgColor
        
        self.setTitleColor(unselectedTitleColor, for: .normal)
        self.sendActions(for: .touchUpInside)
        UIButton.animate(withDuration: 0.2,
                         animations: {
                            self.transform = CGAffineTransform(scaleX: 1, y: 1)
        })
    }

}
