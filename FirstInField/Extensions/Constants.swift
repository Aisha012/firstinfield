//
//  Constants.swift
//  FirstInField
//
//  Created by Namespace  on 19/06/18.
//  Copyright © 2018 Namespace . All rights reserved.
//

import Foundation


enum InputType:String{
    case Single_Input  = "1"
    case MultilineInput = "2"
    case Input_With_Phone_Validation = "3"
    case Input_With_Emai_Validation = "4"
    case Input_type_time = "5"
    case Input_type_date = "6"
    case Input_type_datetime = "7"
    case Input_type_time_range = "8"
    case Input_type_date_range = "9"
}

let KeyHasLoggedIn = "hasloggedin"
let KeyResponseModel = "ResponseModel"
let KEY_LOCATION_NOTIFICATION = "NotificationAndLocation"
let KEY_DEVICE_TOKEN = "DeviceToken"
let KEY_DEVICE_ID = "DeviceID"
let Meeting_Count = "meeting_count"
let Adhoc_Count = "adhoc_count"
let Setting_Status = "setting_status"
let Badge_Count = 0
let appDelegate = UIApplication.shared.delegate as! AppDelegate

enum PaginationType: Int {
    case new, old, reload
}



