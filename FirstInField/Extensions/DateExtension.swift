//
//  DateExtension.swift
//  3C
//
//  Created by Namespace  on 30/06/18.
//  Copyright © 2018 Namespace . All rights reserved.
//

import Foundation


extension Date {
    /// Returns the amount of years from another date
    func years(from date: Date) -> Int {
        return Calendar.current.dateComponents([.year], from: date, to: self).year ?? 0
    }
    /// Returns the amount of months from another date
    func months(from date: Date) -> Int {
        return Calendar.current.dateComponents([.month], from: date, to: self).month ?? 0
    }
    /// Returns the amount of weeks from another date
    func weeks(from date: Date) -> Int {
        return Calendar.current.dateComponents([.weekOfMonth], from: date, to: self).weekOfMonth ?? 0
    }
    /// Returns the amount of days from another date
    func days(from date: Date) -> Int {
        return Calendar.current.dateComponents([.day], from: date, to: self).day ?? 0
    }
    /// Returns the amount of hours from another date
    func hours(from date: Date) -> Int {
        return Calendar.current.dateComponents([.hour], from: date, to: self).hour ?? 0
    }
    /// Returns the amount of minutes from another date
    func minutes(from date: Date) -> Int {
        return Calendar.current.dateComponents([.minute], from: date, to: self).minute ?? 0
    }
    /// Returns the amount of seconds from another date
    func seconds(from date: Date) -> Int {
        return Calendar.current.dateComponents([.second], from: date, to: self).second ?? 0
    }
    /// Returns the amount of nanoseconds from another date
    func nanoseconds(from date: Date) -> Int {
        return Calendar.current.dateComponents([.nanosecond], from: date, to: self).nanosecond ?? 0
    }
    
    func getDaysHoursMinutesSeconds(from date: Date)->(Int, Int, Int, Int){
//        let seconds = self.seconds(from: date)
//        return (seconds / 86400, (seconds % 86400) / 3600, (seconds % 3600) / 60, (seconds % 3600) % 60)
        
        let tempComponents = Calendar.current.dateComponents([.day,.hour,.minute,.second], from: self, to: date)

            // To get the days
            print(tempComponents.day)
            // To get the hours
            print(tempComponents.hour)
            // To get the minutes
            print(tempComponents.minute)
            // To get the seconds
            print(tempComponents.second)

        return (tempComponents.day!, tempComponents.hour!, tempComponents.minute!, tempComponents.second!)
    }
    /// Returns the a custom time interval description from another date
    func offset(from date: Date) -> String {
        if years(from: date)   > 0 { return "\(years(from: date))y"   }
        if months(from: date)  > 0 { return "\(months(from: date))M"  }
        if weeks(from: date)   > 0 { return "\(weeks(from: date))w"   }
        if days(from: date)    > 0 { return "\(days(from: date))d"    }
        if hours(from: date)   > 0 { return "\(hours(from: date))h"   }
        if minutes(from: date) > 0 { return "\(minutes(from: date))m" }
        if seconds(from: date) > 0 { return "\(seconds(from: date))s" }
        if nanoseconds(from: date) > 0 { return "\(nanoseconds(from: date))ns" }
        return ""
    }
}

func getDateFromString(dateString:String, fromFormat: String) -> Date {
    let formatter = DateFormatter()
    formatter.dateFormat = fromFormat
    return formatter.date(from: dateString)!
}

func localToUTC(date:String, fromFormat: String, toFormat: String) -> String {
    
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = fromFormat
    dateFormatter.calendar = NSCalendar.current
    dateFormatter.timeZone = TimeZone.current
    dateFormatter.date
    let dt = dateFormatter.date(from: date)
    dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
    dateFormatter.dateFormat = toFormat
    
    return dateFormatter.string(from: dt!)
}

func UTCToLocal(date:String, fromFormat: String, toFormat: String) -> String {
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = fromFormat
    dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
    
    let dt = dateFormatter.date(from: date)
    dateFormatter.timeZone = TimeZone.current
    dateFormatter.dateFormat = toFormat
    
    return dt != nil ? dateFormatter.string(from: dt!) : ""
}

func configureToShowDateFormatter(for date: Date)->String {
    let formatter = DateFormatter()
    switch true {
    case Calendar.current.isDateInToday(date) || Calendar.current.isDateInYesterday(date):
        formatter.doesRelativeDateFormatting = true
        formatter.dateStyle = .short
        formatter.timeStyle = .short
    case Calendar.current.isDate(date, equalTo: Date(), toGranularity: .weekOfYear):
        formatter.dateFormat = "EEEE h:mm a"
    case Calendar.current.isDate(date, equalTo: Date(), toGranularity: .year):
        formatter.dateFormat = "E, d MMM, h:mm a"
    default:
        formatter.dateFormat = "MMM d, yyyy, h:mm a"
    }
    return formatter.string(from: date)
}

