//
//  UITextFieldExtension.swift
//  FirstInField
//
//  Created by Namespace  on 13/06/18.
//  Copyright © 2018 Namespace . All rights reserved.
//

import UIKit

extension UITextField{
    func isTextFieldEmpty() -> Bool {
      return  ( self.text != nil)  && (!(self.text?.trimmingCharacters(in: .whitespaces).isEmpty)!)
    }
    
    func isValidEmail() -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: self.text!)
    }
    
    func isValidTextLength(minValue: Int, maxValue: Int) -> Bool {
        guard  let strText = (self.text?.trimmingCharacters(in: .whitespacesAndNewlines)) else{
            return false
        }
        if ((strText.characters.count) < minValue) || ((strText.characters.count) > maxValue) {
            return false
        }
        return true
    }
    
    var isPhoneNumber: Bool {
        do {
            let detector = try NSDataDetector(types: NSTextCheckingResult.CheckingType.phoneNumber.rawValue)
            let matches = detector.matches(in: self.text!, options: [], range: NSMakeRange(0, self.text!.count))
            if let res = matches.first {
                return res.resultType == .phoneNumber && res.range.location == 0 && res.range.length == self.text!.count
            } else {
                return false
            }
        } catch {
            return false
        }
    }
    
}


extension UITextField{
    @IBInspectable var placeHolderColor: UIColor? {
        get {
            return self.placeHolderColor
        }
        set {
            self.attributedPlaceholder = NSAttributedString(string:self.placeholder != nil ? self.placeholder! : "", attributes:[NSAttributedStringKey.foregroundColor: newValue!])
        }
    }
}


extension String {
    var isNotEmptyString :Bool {
        return  ( self != nil)  && (!(self.trimmingCharacters(in: .whitespaces).isEmpty))
    }
}
