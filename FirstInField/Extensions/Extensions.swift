//
//  Extensions.swift
//  3C
//
//  Created by Namespace  on 07/06/18.
//  Copyright © 2018 Namespace . All rights reserved.
//
import Alamofire
import Foundation
import SHSnackBarView
import ANLoader
import SocketIO

class Connectivity {
    class var isConnectedToInternet:Bool {
        return NetworkReachabilityManager()!.isReachable
    }
}


extension UIViewController {
    enum intervals:Int {
        case short = 1
        case long  = 3
    }
    
    var jsonDecoder:JSONDecoder{
        get{
            return JSONDecoder()
        }
    }
    
    var jsonEncoder:JSONEncoder{
        get{
            return JSONEncoder()
        }
    }
    
    var snackbarView:snackBar{
        get{
            return snackBar()
        }
    }
    
    
    func setUserDefaults(key:String, value:Any){
        UserDefaults.standard.set(value, forKey: key)
        UserDefaults.standard.synchronize()
    }
    
    func resetDefaults() {
        let defaults = UserDefaults.standard
        let dictionary = defaults.dictionaryRepresentation()
        dictionary.keys.forEach { key in
            defaults.removeObject(forKey: key)
        }
    }
    
    func getUserDefaults(key:String)-> Any?{
        return UserDefaults.standard.value(forKey: key)
    }
    
     func showIndicator(){
        ANLoader.pulseAnimation = true //It will animate your Loading
        ANLoader.activityColor = .white
        ANLoader.activityBackgroundColor = .black
        ANLoader.activityTextColor = .white
        ANLoader.showLoading("Loading", disableUI: true)
        
    }
     func hideIndicator(){
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(1), execute: {
            ANLoader.hide()
        })
    }
    
    func showSnackBarWithMessage(message:String, color:UIColor, txtColor:UIColor? = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0),interval:intervals, minusHeight:Int = 0){
        snackbarView.showSnackBar(view: self.view, bgColor: color, text: message, textColor: txtColor!, interval: interval.rawValue, minusHeight:minusHeight)
    }
    
    
    func uploadImageWithParams(_ imageData: Data?, t:NSDictionary,url:String,  selector:Selector, type:String = "jpeg", mimetype:String = "image/jpeg") {
        if !Connectivity.isConnectedToInternet{
            if self.isKind(of: ViewController.classForCoder()){
                self.showSnackBarWithMessage(message: "Check Internet Connections", color: UIColor.red, interval: .long)
            }else{
                self.showSnackBarWithMessage(message: "Check Internet Connections", color: UIColor.red, interval: .long, minusHeight: 0)
            }
            return
        }
        showIndicator()
        
        let headers: HTTPHeaders = ["Authorization": "auth_token"]
       
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            for (key, value) in t {
                multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as! String)
            }
            
            if let data = imageData {
                multipartFormData.append(data, withName: "doc", fileName: "image." + type, mimeType:mimetype )
            }
            
        }, usingThreshold: UInt64.init(), to: url, method: .post, headers: headers) { (result) in
            switch result{
            case .success(let upload, _, _):
               
                upload.responseData {[weak self] response in
                    print("Succesfully uploaded")
                    self?.hideIndicator()
                    self?.showSnackBarWithMessage(message: "Successfully Uploaded", color: #colorLiteral(red: 0.2745098174, green: 0.4862745106, blue: 0.1411764771, alpha: 1), interval: .long, minusHeight: 0)
                    print(String(decoding: response.data!, as: UTF8.self))
                    _ = self?.perform(selector, with: response.data)
                }
                break
            case .failure(let error):
                self.hideIndicator()
                print(error)
                self.showSnackBarWithMessage(message: "Something went wrong", color: #colorLiteral(red: 0.8891240954, green: 0.2305322289, blue: 0.1835787296, alpha: 1), interval: .long, minusHeight: 0)

                break
                
            }
        }
    }
    

  
    func postDataFromNet<T:Codable>(url:String,requestData:T, selector:Selector, settoken:Bool = false){
       if !Connectivity.isConnectedToInternet{
        if self.isKind(of: ViewController.classForCoder()){
            self.showSnackBarWithMessage(message: "Check Internet Connections", color: UIColor.red, interval: .long)
        }else{
            self.showSnackBarWithMessage(message: "Check Internet Connections", color: UIColor.red, interval: .long, minusHeight: 0)
         }
           return
        }
         showIndicator()
        let urL = URL(string: url)
         let jsonData = try! jsonEncoder.encode(requestData)
         var request = URLRequest(url: urL!)
         request.httpMethod = HTTPMethod.post.rawValue
         request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        if settoken == true {
            request.setValue("c29898a62462fc3041ffce7084363832", forHTTPHeaderField: "apitoken")
        }
         request.httpBody = jsonData
         Alamofire.request(request)
            .validate(statusCode: 200..<300)
            .responseData {[weak self] (data) in
                switch data.result {
                case .success:
                    self?.hideIndicator()
                    _ = self?.perform(selector, with: data.data)
                case .failure(let error):
                    print(error)
//                    self?.showSnackBarWithMessage(message: error.localizedDescription, color: #colorLiteral(red: 0.7450980544, green: 0.1568627506, blue: 0.07450980693, alpha: 1), interval: .long)
                    self?.hideIndicator()
                    switch data.response?.statusCode{
                    case 404:
                        if self!.isKind(of: ViewController.classForCoder()){
                            self?.showSnackBarWithMessage(message: "No Records Found", color: UIColor.red, interval: .long)
                        }else{
                            self?.showSnackBarWithMessage(message: "No Records Found", color: UIColor.red, interval: .long, minusHeight: 0)
                        }
                        break
                    case 400:
                        if self!.isKind(of: ViewController.classForCoder()){
                            // self?.showSnackBarWithMessage(message: "URL not Found", color: UIColor.red, interval: .long)
                        }else{
                            // self?.showSnackBarWithMessage(message: "URL not Found", color: UIColor.red, interval: .long, minusHeight: 60)
                        }
                        break
                    default:
                        break
                    }
                    

                }
                
         }
    }
    
    
    func postDataFromNetWithResponse<T:Codable>(url:String,requestData:T, selector:Selector, settoken:Bool = false){
        if !Connectivity.isConnectedToInternet{
            if self.isKind(of: ViewController.classForCoder()){
                self.showSnackBarWithMessage(message: "Check Internet Connections", color: UIColor.red, interval: .long)
            }else{
                self.showSnackBarWithMessage(message: "Check Internet Connections", color: UIColor.red, interval: .long, minusHeight: 0)
            }
            return
        }
        showIndicator()
        let urL = URL(string: url)
        let jsonData = try! jsonEncoder.encode(requestData)
        var request = URLRequest(url: urL!)
        request.httpMethod = HTTPMethod.post.rawValue
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        if settoken == true {
            request.setValue("c29898a62462fc3041ffce7084363832", forHTTPHeaderField: "apitoken")
        }
        request.httpBody = jsonData
        Alamofire.request(request)
            .validate(statusCode: 200..<300)
            .responseData {[weak self] (data) in
                switch data.result {
                case .success:
                    self?.hideIndicator()
                    _ = self?.perform(selector, with: data.data)
                case .failure(let error):
                    print(error)
                    self?.hideIndicator()
                    switch data.response?.statusCode{
                    case 404:
                        
                        _ = self?.perform(selector, with: data.data)
                        break
                    case 400:
                        if self!.isKind(of: ViewController.classForCoder()){
                            // self?.showSnackBarWithMessage(message: "URL not Found", color: UIColor.red, interval: .long)
                        }else{
                            // self?.showSnackBarWithMessage(message: "URL not Found", color: UIColor.red, interval: .long, minusHeight: 60)
                        }
                        break
                    default:
                        break
                    }
                   
                    
                }
                
        }
    }
    
    
    func getDataFromNet(url:String,selector:Selector, settoken:Bool = false){
        if !Connectivity.isConnectedToInternet{
            if self.isKind(of: ViewController.classForCoder()){
                self.showSnackBarWithMessage(message: "Check Internet Connections", color: UIColor.red, interval: .long)
            }else{
                self.showSnackBarWithMessage(message: "Check Internet Connections", color: UIColor.red, interval: .long, minusHeight: 0)
            }
            return
        }
        showIndicator()
        let urL = URL(string: url)
        var request = URLRequest(url: urL!)
        request.httpMethod = HTTPMethod.get.rawValue
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        if settoken == true {
            request.setValue(API_Token, forHTTPHeaderField: "apitoken")
        }
        Alamofire.request(request)
            .validate(statusCode: 200..<300)
            .responseData {[weak self] (data) in
                switch data.result {
                case .success:
                    self?.hideIndicator()
                    print(String(decoding: data.data!, as: UTF8.self))
                    _ = self?.perform(selector, with: data.data)
                case .failure(let error):
                    self?.hideIndicator()
                    switch data.response?.statusCode{
                    case 404:
                        if self!.isKind(of: ViewController.classForCoder()){
                            self?.showSnackBarWithMessage(message: "No Records Found", color: UIColor.red, interval: .long)
                        }else{
                            self?.showSnackBarWithMessage(message: "No Records Found", color: UIColor.red, interval: .long, minusHeight: 0)
                        }
                        break
                    case 400:
                        if self!.isKind(of: ViewController.classForCoder()){
                           // self?.showSnackBarWithMessage(message: "URL not Found", color: UIColor.red, interval: .long)
                        }else{
                           // self?.showSnackBarWithMessage(message: "URL not Found", color: UIColor.red, interval: .long, minusHeight: 60)
                        }
                        break
                    default:
                        break
                    }
                    print(error)
                   // self?.showSnackBarWithMessage(message: error.localizedDescription, color: #colorLiteral(red: 0.7450980544, green: 0.1568627506, blue: 0.07450980693, alpha: 1), interval: .long)
//                    if self!.isKind(of: ViewController.classForCoder()){
//                        self?.showSnackBarWithMessage(message: error.localizedDescription, color: UIColor.red, interval: .long)
//                    }else{
//                        self?.showSnackBarWithMessage(message: error.localizedDescription, color: UIColor.red, interval: .long, minusHeight: 60)
//                    }
                    
                }
        }
    }
    
    
    func showInputDialog(title:String? = nil,
                         subtitle:String? = nil,
                         actionTitle:String? = "Add",
                         cancelTitle:String? = "Cancel",
                         inputPlaceholder:String? = nil,
                         inputKeyboardType:UIKeyboardType = UIKeyboardType.default,
                         cancelHandler: ((UIAlertAction) -> Swift.Void)? = nil,
                         actionHandler: ((_ text: String?) -> Void)? = nil) {
        
        let alert = UIAlertController(title: title, message: subtitle, preferredStyle: .alert)
        alert.addTextField { (textField:UITextField) in
            textField.placeholder = inputPlaceholder
            textField.keyboardType = inputKeyboardType
            let heightConstraint = NSLayoutConstraint(item: textField, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 40)
            textField.addConstraint(heightConstraint)
            
        }
        alert.addAction(UIAlertAction(title: actionTitle, style: .destructive, handler: { (action:UIAlertAction) in
            guard let textField =  alert.textFields?.first else {
                actionHandler?(nil)
                return
            }
            actionHandler?(textField.text)
        }))
        alert.addAction(UIAlertAction(title: cancelTitle, style: .cancel, handler: cancelHandler))
        
        self.present(alert, animated: true, completion: nil)
    }
    // logoutUserFromAppAPI common function
    func logoutUserFromAppAPI(){
        
        let resPonsedata = self.getUserDefaults(key: KeyResponseModel) as! Data
        let responseModel = try! self.jsonDecoder.decode(LoginResponse.self, from: resPonsedata)
        let completeUrl =  Urls.Logout_user.rawValue + "\(responseModel.user?.id ?? 0)"
        let request = LogOutAccountRequest(api_token: API_Token, user_token: responseModel.user!.user_token!, device_id: "\(UserDefaults.standard.value(forKey: KEY_DEVICE_ID) ?? "")")
    
       appDelegate.manager = SocketManager(socketURL: URL(string: Urls.FIF_Socket.rawValue)!, config: [.log(true), .compress, .forceNew(true)])
        appDelegate.socket = appDelegate.manager.defaultSocket
        appDelegate.socket.disconnect()
        
         self.postDataFromNetWithResponse(url: completeUrl, requestData: request, selector: #selector(self.handLogOutUserResponse(data:)) )
        
    }
    
    @objc func handLogOutUserResponse(data:Data?){
        if let dat = data{
            do{
                
                let logOutResponse = try jsonDecoder.decode(LogOutUserResponse.self, from: dat)
                if logOutResponse.res_msg != nil && logOutResponse.res_code! != 200{
                    self.showSnackBarWithMessage(message: logOutResponse.res_msg!, color: #colorLiteral(red: 0.9254902005, green: 0.2352941185, blue: 0.101907857, alpha: 1), interval: .long, minusHeight:0)
                    return
                }else {
                    
                    self.setUserDefaults(key: KeyHasLoggedIn, value: false)
                    let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                    let viewController = mainStoryboard.instantiateViewController(withIdentifier: "LoginController")
                    UIApplication.shared.keyWindow?.rootViewController = viewController
                
                }
            }catch let err{
                print(err)
            }
        }
    }
    
    func setDateTimer(metinngTime:Meeting?, lblTimer: UILabel){
        if metinngTime?.start_time != nil && metinngTime?.end_time != nil {
            
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            formatter.timeZone = TimeZone(abbreviation:"UTC")
            
            let stratDate = formatter.date(from: metinngTime!.start_time!)
            let endDate = formatter.date(from: metinngTime!.end_time!)
            
            
            if (stratDate != nil) && (endDate != nil){
                if #available(iOS 10.0, *) {
                    
                       Timer.scheduledTimer(withTimeInterval: 1, repeats: true) {[weak self] (timer) in
                        
                        let tempdate = formatter.string(from: Date())
                            let currentDate = formatter.date(from: tempdate)
                        
                        let totolTime = stratDate!.getDaysHoursMinutesSeconds(from: currentDate!)
                        
                        if /*(totolTime.0 < 0 || totolTime.1 < 0 || totolTime.2 < 0) &&*/ currentDate != nil && (endDate! > currentDate!){
                            
                            lblTimer.textColor = UIColor.red
                            let str = String(format:"%02i:%02i:%02i", totolTime.1, totolTime.2, totolTime.3)
                            //                            let publishSTR = str.replacingOccurrences(of: "-", with: "")
                            lblTimer.text = str
                            lblTimer.updateConstraints()
                            return
                        }else{
                            lblTimer.text = "00:00:00"
                            lblTimer.textColor = UIColor.white
                            timer.invalidate()
                            
                            let messageText = "Your meeting is finished."
                            let alert = UIAlertController(title: "Alert", message: messageText, preferredStyle: .alert)
                            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action) in
                                self?.navigationController?.popToRootViewController(animated:true)
                            }))
                            UIApplication.shared.keyWindow?.rootViewController?.present(alert, animated: true)

                           
                            return
                        }
                        
                        //                        let str = String(format:"%02i:%02i:%02i", totolTime.1, totolTime.2, totolTime.3)
                        //                        self?.lblTimer.text = str
                    }
                } else {
                    // Fallback on earlier versions
                }
            }
            
        }
    }
    
}



extension UIImage {
    class func imageWithColor(color: UIColor, sizee:CGRect) -> UIImage {
        let rect: CGRect = sizee
        UIGraphicsBeginImageContextWithOptions(sizee.size, false, 0)
        color.setFill()
        UIRectFill(rect)
        let image: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return image
    }
}


extension UIView {
    
    func roundCorners(_ corners: UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        self.layer.mask = mask
    }
    
}

extension UIApplication {
    class func topViewController(controller: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        if let navigationController = controller as? UINavigationController {
            return topViewController(controller: navigationController.visibleViewController)
        }
        if let tabController = controller as? UITabBarController {
            if let selected = tabController.selectedViewController {
                return topViewController(controller: selected)
            }
        }
        if let presented = controller?.presentedViewController {
            return topViewController(controller: presented)
        }
        return controller
    }
}


extension UIView
{
    func addShadow()
    {
        layer.masksToBounds = false
        layer.cornerRadius = 7
        layer.shadowColor = UIColor.lightGray.cgColor
        layer.shadowOffset = .zero
        layer.shadowOpacity = 0.5
        layer.shadowRadius = 5
    }


}


extension UIView {
    func addBorder(toEdges edges: UIRectEdge, color: UIColor, thickness: CGFloat) {
        
        func addBorder(toEdge edges: UIRectEdge, color: UIColor, thickness: CGFloat) {
            let border = CALayer()
            border.backgroundColor = color.cgColor
            
            switch edges {
            case .top:
                border.frame = CGRect(x: 0, y: 0, width: frame.width, height: thickness)
            case .bottom:
                border.frame = CGRect(x: 0, y: frame.height - thickness, width: frame.width, height: thickness)
            case .left:
                border.frame = CGRect(x: 0, y: 0, width: thickness, height: frame.height)
            case .right:
                border.frame = CGRect(x: frame.width - thickness, y: 0, width: thickness, height: frame.height)
            default:
                break
            }
            
            layer.addSublayer(border)
        }
        
        if edges.contains(.top) || edges.contains(.all) {
            addBorder(toEdge: .top, color: color, thickness: thickness)
        }
        
        if edges.contains(.bottom) || edges.contains(.all) {
            addBorder(toEdge: .bottom, color: color, thickness: thickness)
        }
        
        if edges.contains(.left) || edges.contains(.all) {
            addBorder(toEdge: .left, color: color, thickness: thickness)
        }
        
        if edges.contains(.right) || edges.contains(.all) {
            addBorder(toEdge: .right, color: color, thickness: thickness)
        }
    }
}

