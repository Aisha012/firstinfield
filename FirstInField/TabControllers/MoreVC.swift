//
//  MoreVC.swift
//  FirstInField
//
//  Created by Namespace  on 15/06/18.
//  Copyright © 2018 Namespace . All rights reserved.
//

import UIKit

@available(iOS 10.0, *)
class MoreVC: UIViewController , UITableViewDataSource, UITableViewDelegate{
    
 var titles = ["SETTING","PROFILE"]
    var switchResponse:ToggleResponse?
    var cell : MorevcCell!

   @IBOutlet var tableView : UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "bell"), style: .plain, target: self, action: #selector(showNotificationListing(btn:)))

        self.navigationItem.title = "More"
        self.navigationController?.navigationBar.tintColor = UIColor.white
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
   

    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if let ident = segue.identifier{
            switch ident {
            case "SeguePopOver":
                let des = segue.destination as! PopUpViewController
                des.forView = sender as! String
                break
            default:
                break
            }
        }
    }

    //MARK:- UITableViewDelegate
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section{
            
        case 0:
            switch indexPath.row{
            case 0 :
                cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! MorevcCell
                cell.lblTitle.text  = titles[indexPath.section]
                cell.titleBtns.setImage(#imageLiteral(resourceName: "setting-icon"), for: .normal)
                
            case 1:
                cell = tableView.dequeueReusableCell(withIdentifier: "ToggleCell", for: indexPath) as! MorevcCell
               cell.toggleSwitch.isOn = (self.getUserDefaults(key: Setting_Status) ?? 0) as! Bool
//                cell.toggleButton.isSelected = (self.getUserDefaults(key: Setting_Status) ?? 0) as! Bool
                

            default:
                break

            }
            
        case 1:
            
              switch indexPath.row{
              case 0 :
                cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! MorevcCell
                cell.lblTitle.text  = titles[indexPath.section]
                cell.titleBtns.setImage(#imageLiteral(resourceName: "profile-icon"), for: .normal)
              case 1:
                cell = tableView.dequeueReusableCell(withIdentifier: "ProfileCell", for: indexPath) as! MorevcCell
                
                let resPonsedata = getUserDefaults(key: KeyResponseModel) as! Data
                let responseModel = try! jsonDecoder.decode(LoginResponse.self, from: resPonsedata)
                
                cell.userName.text = "\(responseModel.user?.first_name ?? "") \(responseModel.user?.last_name ?? "")".uppercased()
                cell.userMail.text = "\(responseModel.user?.email ?? "")"
                cell.userAwarsd.text = "Awards: 0".uppercased()

              default:
                break
                
            }

        default : break
            
        }
        cell.selectionStyle = .none
        return cell
    }
 
     //    MARK:- IBAction
   
    @IBAction func toggleButtonTapped(_ sender : UISwitch){
        
        let messageText = !(sender.isSelected) ? "Disabling will set you online for messaging only during meetings." : "Enabling will set you online for messaging."
            let alert = UIAlertController(title: "Alert", message: messageText, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "No", style: .default, handler: { (action) in
                 sender.isOn = !sender.isOn
//                sender.isSelected = sender.isSelected
            }))
            alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { (ac) in
//                sender.isSelected = !(sender.isSelected)
                self.toggleStatusApi()
            }))
            
            self.present(alert, animated: true)
        
    }
    
    @IBAction func changeRootController(btn:Any){
        let messageText = "Are you sure, you want to log out?"
        let alert = UIAlertController(title: "Alert", message: messageText, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "No", style: .cancel, handler: { (action) in
            
        }))
        alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { (ac) in
            self.logoutUserFromAppAPI()
            
        }))
        
        self.present(alert, animated: true)
        
    }
    
    @IBAction func discloserButtonTapped(sender:UIButton){
    switch sender.tag{
    case 1:
    
    guard let settingsUrl = URL(string: UIApplicationOpenSettingsURLString) else {
    return
    }
    
    if UIApplication.shared.canOpenURL(settingsUrl) {
    UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
    // Checking for setting is opened or not
    print("Setting is opened: \(success)")
    })
    }
    case 2:
    self.performSegue(withIdentifier: "SeguePopOver", sender:"account")
    case 3:
    self.performSegue(withIdentifier: "SeguePopOver", sender:"password")
    
    default : break
    }
    }
    
    @IBAction func showNotificationListing(btn:Any){
        self.performSegue(withIdentifier: "SegueNotilist", sender:nil)
    }

    //    MARK:- API Methods
    func toggleStatusApi()
    {
        let resPonsedata = getUserDefaults(key: KeyResponseModel) as! Data
        let responseModel = try! jsonDecoder.decode(LoginResponse.self, from: resPonsedata)
        
        self.getDataFromNet(url: Urls.toggleStatus.rawValue  + "\(responseModel.user!.id!)?" + "api_token=" + API_Token + "&user_token=\(responseModel.user!.user_token!)", selector: #selector(self.handleStatusResponse(data:)))
        
    }
    
    
    @objc func handleStatusResponse(data:Data?) {
        if let dat = data{
            do{
                let response = try jsonDecoder.decode(ToggleResponse.self, from: dat)
                print(response)
                print(switchResponse?.data?.status ?? "Chat status not stored")
                setUserDefaults(key: Setting_Status, value: switchResponse?.data?.status ?? 0)
                
              
            }catch let err {
                print(err)
            }
        }
    }
    
}


class MorevcCell:UITableViewCell{
    @IBOutlet weak var lblTitle:UILabel!
    @IBOutlet var toggleButton : UIButton!
    @IBOutlet var titleBtns : UIButton!
    @IBOutlet var toggleSwitch : UISwitch!

    //ProfileCell
    @IBOutlet weak var userName:UILabel!
    @IBOutlet weak var userMail:UILabel!
    @IBOutlet weak var userAwarsd:UILabel!
    
    @IBOutlet weak var outerView:UIView!

}

