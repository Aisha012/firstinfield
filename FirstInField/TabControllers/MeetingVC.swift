//
//  MeetingVC.swift
//  FirstInField
//
//  Created by Namespace  on 13/06/18.
//  Copyright © 2018 Namespace . All rights reserved.
//

import UIKit

class MeetingVC: UIViewController, UITableViewDataSource,UITableViewDelegate  {
   
    @IBOutlet weak var tbleView: UITableView!
    @IBOutlet weak var btnUpcoming: UIButton!
    @IBOutlet weak var vwFirstSegment: UIView!
    @IBOutlet weak var vwLastSegment: UIView!
    @IBOutlet weak var btnPast: UIButton!
     @IBOutlet weak var centerLbl: UILabel!
    
    var meetingStatus = ""
    var selectedIndex = 0
    var isRefreshing:Bool = false
    var pastMeetingResponse:MeetingResponse?
    var meetingResponse:MeetingResponse?
    var pageIndex = 1
    var pastPageIndex = 1
    

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "Meetings"
        let img = self.navigationItem.rightBarButtonItem?.image
        self.navigationItem.rightBarButtonItem?.image = img?.withRenderingMode(.alwaysOriginal)
        self.navigationController?.tabBarController?.tabBar.isHidden = true
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.refreshMeetings), name: Notification.Name("refreshMeeting"), object: nil)
        
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "bell"), style: .plain, target: self, action: #selector(showNotificationListing(btn:)))

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if selectedIndex == 0{
            pageIndex = 1
            meetingResponse = nil
            pastMeetingResponse = nil
            self.tbleView.reloadData()
           getupcomingMeetings()
        }else{
            pageIndex = 1
            pastMeetingResponse = nil
            meetingResponse = nil
            self.tbleView.reloadData()
           getPastMeetings()
        }
    }
    
    
    //MARK:- IBAction
    
    @IBAction func startMeeting(btn:MeetingButton){
        
    }
    
    @IBAction func showActionSheet(sender: MeetingButton) {
        
        if selectedIndex == 0{
            if self.meetingResponse?.meeting_data?.user_meetings?[sender.indexPath.section].meetings?[sender.indexPath.row].scheduled_address == nil ||  self.meetingResponse?.meeting_data?.user_meetings?[sender.indexPath.section].meetings?[sender.indexPath.row].scheduled_address  == ""{
                return
            }
            
            if (self.meetingResponse?.meeting_data?.user_meetings?[sender.indexPath.section].meetings?[sender.indexPath.row].actual_longitude != nil && self.meetingResponse?.meeting_data?.user_meetings?[sender.indexPath.section].meetings?[sender.indexPath.row].actual_latitude != nil){
                let latt = self.meetingResponse!.meeting_data!.user_meetings![sender.indexPath.section].meetings![sender.indexPath.row].actual_latitude!
                let lon = self.meetingResponse!.meeting_data!.user_meetings![sender.indexPath.section].meetings![sender.indexPath.row].actual_longitude!
                
                self.performSegue(withIdentifier: "SegueAppleMap", sender: (latt, lon))
                return
            }
            
            
        }else{
            if self.pastMeetingResponse?.meeting_data?.user_meetings?[sender.indexPath.section].meetings?[sender.indexPath.row].scheduled_address == nil || self.pastMeetingResponse?.meeting_data?.user_meetings?[sender.indexPath.section].meetings?[sender.indexPath.row].scheduled_address == ""{
                return
            }
        }
        

        if (self.pastMeetingResponse?.meeting_data?.user_meetings?[sender.indexPath.section].meetings?[sender.indexPath.row].actual_longitude != nil && self.pastMeetingResponse?.meeting_data?.user_meetings?[sender.indexPath.section].meetings?[sender.indexPath.row].actual_latitude != nil){
            let latt = self.pastMeetingResponse!.meeting_data!.user_meetings![sender.indexPath.section].meetings![sender.indexPath.row].actual_latitude!
            let lon = self.pastMeetingResponse!.meeting_data!.user_meetings![sender.indexPath.section].meetings![sender.indexPath.row].actual_longitude!

            self.performSegue(withIdentifier: "SegueAppleMap", sender: (latt, lon))
            return
        }
        
        self.performSegue(withIdentifier: "SegueAppleMap", sender: nil)


    }
    
    @IBAction func showNotificationListing(btn:Any){
        self.performSegue(withIdentifier: "SegueNotilist", sender:nil)
    }
    
    
    @IBAction func btnOnTapSegmentButton(_ sender: UIButton) {
        switch sender.tag {
        case 0:
            vwFirstSegment.backgroundColor = #colorLiteral(red: 0.001829766668, green: 0.2451805174, blue: 0.3752157092, alpha: 1) //#colorLiter: 0.001829766668, green: 0.245180var4, blue: 0.3752157092, alpha: 1)
            vwLastSegment.backgroundColor = #colorLiteral(red: 0, green: 0.3599842191, blue: 0.4917289615, alpha: 1)
            btnPast.setTitleColor(#colorLiteral(red: 0.5416718721, green: 0.6478435993, blue: 0.7340455651, alpha: 1), for: .normal)
            btnUpcoming.setTitleColor(#colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), for: .normal)
            selectedIndex = 0
            if self.meetingResponse == nil || self.meetingResponse?.meeting_data?.user_meetings == nil{
//                self.tbleView.reloadData()
                getupcomingMeetings()
            }else{
                self.tbleView.reloadData()
                self.centerLbl.isHidden = (self.meetingResponse?.meeting_data?.user_meetings != nil ? self.meetingResponse!.meeting_data!.user_meetings!.count>0 : false)
                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.1) {
                    let indexpath = IndexPath(row: 0, section: 0)
                    self.tbleView.scrollToRow(at: indexpath, at: .top, animated: true)
                }
            }
            break
        case 1:
            vwFirstSegment.backgroundColor = #colorLiteral(red: 0, green: 0.3599842191, blue: 0.4917289615, alpha: 1)
            vwLastSegment.backgroundColor = #colorLiteral(red: 0.001829766668, green: 0.2451805174, blue: 0.3752157092, alpha: 1)
            btnUpcoming.setTitleColor(#colorLiteral(red: 0.5416718721, green: 0.6478435993, blue: 0.7340455651, alpha: 1), for: .normal)
            btnPast.setTitleColor(#colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), for: .normal)
            selectedIndex = 1
            if self.pastMeetingResponse == nil || self.pastMeetingResponse?.meeting_data?.user_meetings == nil{
//                self.tbleView.reloadData()
                getPastMeetings()
            }else{
                self.tbleView.reloadData()
                self.centerLbl.isHidden = (self.pastMeetingResponse?.meeting_data?.user_meetings != nil ? self.pastMeetingResponse!.meeting_data!.user_meetings!.count>0 : false)
                
                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.1) {
                    let indexpath = IndexPath(row: 0, section: 0)
                    self.tbleView.scrollToRow(at: indexpath, at: .top, animated: true)
                }
            }
            break
        default:
            break
        }
        
        
    }

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
       // dest.cordinate = sender as! tuple
        if let seg = segue.identifier{
            switch (seg){
            case "SegueCheckList":
                let dest = segue.destination as! MeetingDetailVC
                dest.meetingID = sender! as! String
                dest.titleStr = self.selectedIndex == 0 ? "Upcoming Meeting" : self.selectedIndex == 1 ? "Past Meeting" : "Meeting"
                dest.selectedIndex = self.selectedIndex
                dest.meetingStatus = self.meetingStatus
                break
                
            case "SegueAppleMap":
                let dest = segue.destination as! AppleMapVC
                if let sen = sender{
                    dest.cordinate = sender as! tuple
                }
                break
            default:
                break
            }
        }
    }
    
    

    // MARK: - Tableview Datasource Methods
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return selectedIndex == 0 ? (self.meetingResponse?.meeting_data?.user_meetings != nil ? self.meetingResponse!.meeting_data!.user_meetings!.count : 0) :(self.pastMeetingResponse?.meeting_data?.user_meetings != nil ? self.pastMeetingResponse!.meeting_data!.user_meetings!.count : 0)
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return selectedIndex == 0 ? (self.meetingResponse?.meeting_data?.user_meetings?[section].meetings != nil ? self.meetingResponse!.meeting_data!.user_meetings![section].meetings!.count : 0) :(self.pastMeetingResponse?.meeting_data?.user_meetings?[section].meetings != nil ? self.pastMeetingResponse!.meeting_data!.user_meetings![section].meetings!.count : 0)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if selectedIndex == 0{
        if (indexPath.section == self.meetingResponse!.meeting_data!.user_meetings!.count - 2 ) && (self.meetingResponse!.meeting_data!.pagination!.currentPage! < self.meetingResponse!.meeting_data!.pagination!.lastPage!){
            self.pageIndex = self.meetingResponse!.meeting_data!.pagination!.currentPage! + 1
                self.getupcomingMeetings()
        }
       // if indexPath.row % 2 == 0{
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! MeetingTableCell
        cell.lblAddress.text = self.meetingResponse!.meeting_data!.user_meetings![indexPath.section].meetings![indexPath.row].scheduled_address != nil ? self.meetingResponse!.meeting_data!.user_meetings![indexPath.section].meetings![indexPath.row].scheduled_address!:"N/A"
        
        cell.lblMeetingDesc.text = self.meetingResponse!.meeting_data!.user_meetings![indexPath.section].meetings![indexPath.row].meetingname != nil ? self.meetingResponse!.meeting_data!.user_meetings![indexPath.section].meetings![indexPath.row].meetingname!:"N/A"
            cell.btnMap.indexPath = indexPath as NSIndexPath
            
            let meetingData = self.meetingResponse!.meeting_data!.user_meetings![indexPath.section].meetings![indexPath.row]
            let startTime = UTCToLocal(date: meetingData.actual_start_time!, fromFormat: "HH:mm:ss", toFormat: "HH:mm")
            let endTime = UTCToLocal(date: meetingData.actual_end_time!, fromFormat: "HH:mm:ss", toFormat: "HH:mm")
            
            cell.lblTime.text = (startTime != "" ? startTime : meetingData.actual_start_time!) +  "  -  " + (endTime != "" ? endTime : meetingData.actual_end_time)!
            
        return cell
        }
        
        if (indexPath.section == self.pastMeetingResponse!.meeting_data!.user_meetings!.count - 2 ) && (self.pastMeetingResponse!.meeting_data!.pagination!.currentPage! < self.pastMeetingResponse!.meeting_data!.pagination!.lastPage!){
            self.pastPageIndex = self.pastMeetingResponse!.meeting_data!.pagination!.currentPage! + 1
            self.getPastMeetings()
        }
        // if indexPath.row % 2 == 0{
        let cell = tableView.dequeueReusableCell(withIdentifier: "PastCell", for: indexPath) as! MeetingTableCell
        cell.lblAddress.text = self.pastMeetingResponse!.meeting_data!.user_meetings![indexPath.section].meetings![indexPath.row].scheduled_address != nil ? self.pastMeetingResponse!.meeting_data!.user_meetings![indexPath.section].meetings![indexPath.row].scheduled_address!:"N/A"
        cell.lblMeetingDesc.text = self.pastMeetingResponse!.meeting_data!.user_meetings![indexPath.section].meetings![indexPath.row].meetingname != nil ? self.pastMeetingResponse!.meeting_data!.user_meetings![indexPath.section].meetings![indexPath.row].meetingname!:"N/A"
        cell.btnMap.indexPath = indexPath as NSIndexPath
      //  cell.btnStartMeeting.indexPath = indexPath as! NSIndexPath
        
        let meetingData = self.pastMeetingResponse!.meeting_data!.user_meetings![indexPath.section].meetings![indexPath.row]
        
        let startTime = meetingData.actual_start_time != nil ? UTCToLocal(date: meetingData.actual_start_time!, fromFormat: "HH:mm:ss", toFormat: "HH:mm") : meetingData.actual_start_time
        let endTime = meetingData.actual_end_time != nil ? UTCToLocal(date: meetingData.actual_end_time!, fromFormat: "HH:mm:ss", toFormat: "HH:mm") : meetingData.actual_end_time
        
        cell.lblTime.text = (startTime != nil ? startTime! : "") +  "  -  " + (endTime != nil ? endTime : "")!
        
        
        return cell
        
        
      //  }
//            let cell = tableView.dequeueReusableCell(withIdentifier: "Cell1", for: indexPath)
//            return cell
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if selectedIndex == 0{
        let cell = tableView.dequeueReusableCell(withIdentifier: "headerCell") as! MeetingHeaderCell
        cell.lblDate.text = self.meetingResponse!.meeting_data!.user_meetings?[section].date != nil ? localToUTC(date: self.meetingResponse!.meeting_data!.user_meetings![section].date!, fromFormat: "yyyy-MM-dd", toFormat: "dd MMMM, EE") : ""
        return cell
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "headerCell") as! MeetingHeaderCell
        cell.lblDate.text = self.pastMeetingResponse!.meeting_data!.user_meetings?[section].date != nil ? localToUTC(date: self.pastMeetingResponse!.meeting_data!.user_meetings![section].date!, fromFormat: "yyyy-MM-dd", toFormat: "dd MMMM, EE") : ""
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 30
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if self.selectedIndex == 0{
            let idd = self.meetingResponse!.meeting_data!.user_meetings?[indexPath.section].meetings?[indexPath.row].id != nil ? "\(String(describing: self.meetingResponse!.meeting_data!.user_meetings![indexPath.section].meetings![indexPath.row].id!))" : "28"
            meetingStatus = self.meetingResponse!.meeting_data!.user_meetings?[indexPath.section].meetings?[indexPath.row].status != nil ? "\(String(describing: self.meetingResponse!.meeting_data!.user_meetings![indexPath.section].meetings![indexPath.row].status!))" : "3"
            self.performSegue(withIdentifier: "SegueCheckList", sender:idd)
        }else{
            let idd = self.pastMeetingResponse!.meeting_data!.user_meetings?[indexPath.section].meetings?[indexPath.row].id != nil ? "\( self.pastMeetingResponse!.meeting_data!.user_meetings![indexPath.section].meetings![indexPath.row].id!)" : "28"
            meetingStatus = self.pastMeetingResponse!.meeting_data!.user_meetings?[indexPath.section].meetings?[indexPath.row].status != nil ? "\( self.pastMeetingResponse!.meeting_data!.user_meetings![indexPath.section].meetings![indexPath.row].status!)" : "3"
            self.performSegue(withIdentifier: "SegueCheckList", sender:idd)
        }
    }
    
    
    //MARK:- Helping Func
    
    override func refresh(_ sender: UIRefreshControl) {
        if selectedIndex == 0{
            self.pageIndex = 1
            isRefreshing = true
            self.getupcomingMeetings()
            sender.endRefreshing()
        }else{
            self.pastPageIndex = 1
            isRefreshing = true
            sender.endRefreshing()
            self.getPastMeetings()
        }
    }
    
    @objc func refreshMeetings(){
        if selectedIndex == 0{
            self.pageIndex = 1
            isRefreshing = true
            self.getupcomingMeetings()
        }else{
            self.pastPageIndex = 1
            isRefreshing = true
            self.getPastMeetings()
        }
        
    }
    
    //MARK:- WebService
    
    func getupcomingMeetings(){
        let resPonsedata = getUserDefaults(key: KeyResponseModel) as! Data
        let responseModel = try! jsonDecoder.decode(LoginResponse.self, from: resPonsedata)
        self.getDataFromNet(url: Urls.Get_Upcoming_Meetings.rawValue + "?user_id=" + "\(responseModel.user!.id!)" + "&api_token=" + API_Token + "&user_token=" + responseModel.user!.user_token!  + "&page=" + "\(pageIndex)", selector: #selector(MeetingVC.handleNetResponse(data:)))
    }
    
    @objc func handleNetResponse(data:Data?) {
        if let dat = data{
            do{
                
                
                let meetingRespons = try jsonDecoder.decode(MeetingResponse.self, from: dat)
                if meetingRespons.res_msg != nil && meetingRespons.res_code! != 200{
                    //                    self.showSnackBarWithMessage(message: responseModel!.res_msg!, color: #colorLiteral(red: 0.9254902005, green: 0.2352941185, blue: 0.1019607857, alpha: 1), interval: .long, minusHeight: 0)
                    return
                }
                
                setUserDefaults(key: Meeting_Count, value: meetingRespons.meeting_data?.meeting_count ?? 0)
              
                NotificationCenter.default.post(name: Notification.Name("resetCounter"), object: nil)
                
                if isRefreshing{
                    self.meetingResponse = nil
                    self.meetingResponse = meetingRespons
                    
                    self.centerLbl.isHidden = (self.meetingResponse?.meeting_data?.user_meetings != nil ? self.meetingResponse!.meeting_data!.user_meetings!.count>0 : false)
                    self.centerLbl.text = !(centerLbl.isHidden) ? "No Records Found." : ""
                    
                    self.tbleView.reloadData()
                    isRefreshing = false
                    return
                }
                if self.meetingResponse == nil{
                    self.meetingResponse = meetingRespons
                }else if meetingRespons.meeting_data?.user_meetings != nil{
                    let userm = meetingRespons.meeting_data!.user_meetings!
                    self.meetingResponse?.meeting_data!.user_meetings!.append(contentsOf: userm)
                    self.meetingResponse?.meeting_data!.pagination = meetingRespons.meeting_data!.pagination
                }
                
                self.centerLbl.isHidden = (self.meetingResponse?.meeting_data?.user_meetings != nil ? self.meetingResponse!.meeting_data!.user_meetings!.count>0 : false)
                self.centerLbl.text = !(centerLbl.isHidden) ? "No Records Found." : ""
                self.tbleView.reloadData()
                if meetingRespons.meeting_data!.pagination?.currentPage! == 1{
                    DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.1) {
                        let indexpath = IndexPath(row: 0, section: 0)
                        self.tbleView.scrollToRow(at: indexpath, at: .top, animated: true)
                        
                    }
                }
            }catch let err {
                centerLbl.isHidden = false
                self.centerLbl.text = !(centerLbl.isHidden) ? "No Records Found." : ""
                print(err)
            }
        }
    }
    
    func getPastMeetings(){
        let resPonsedata = getUserDefaults(key: KeyResponseModel) as! Data
        let responseModel:LoginResponse = try! jsonDecoder.decode(LoginResponse.self, from: resPonsedata)
        self.getDataFromNet(url: Urls.Get_Past_Meetings.rawValue + "?user_id=" + "\(responseModel.user!.id!)" + "&api_token=" + API_Token + "&user_token=" + responseModel.user!.user_token! + "&page=" + "\(pastPageIndex)", selector: #selector(MeetingVC.handlePastNetResponse(data:)))
    }
    
    @objc func handlePastNetResponse(data:Data?) {
        if let dat = data{
            do{
                let meetingRespons = try jsonDecoder.decode(MeetingResponse.self, from: dat)
                if meetingRespons.res_msg != nil && meetingRespons.res_code! != 200{
                    //    self.showSnackBarWithMessage(message: responseModel!.res_msg!, color: #colorLiteral(red: 0.9254902005, green: 0.2352941185, blue: 0.1019607857, alpha: 1), interval: .long)
                    return
                }
                if isRefreshing{
                    self.pastMeetingResponse = nil
                    self.pastMeetingResponse = meetingRespons
                    
                    self.centerLbl.isHidden = (self.pastMeetingResponse?.meeting_data?.user_meetings != nil ? self.pastMeetingResponse!.meeting_data!.user_meetings!.count>0 : false)
                    self.centerLbl.text = !(centerLbl.isHidden) ? "No Records Found." : ""
                    
                    self.tbleView.reloadData()
                    isRefreshing = false
                    return
                }
                if self.pastMeetingResponse == nil{
                    self.pastMeetingResponse = meetingRespons
                }else if meetingRespons.meeting_data?.user_meetings != nil{
                    let userm = meetingRespons.meeting_data!.user_meetings!
                    self.pastMeetingResponse?.meeting_data!.user_meetings!.append(contentsOf: userm)
                    self.pastMeetingResponse?.meeting_data!.pagination = meetingRespons.meeting_data!.pagination
                }
                
                self.centerLbl.isHidden = (self.pastMeetingResponse?.meeting_data?.user_meetings != nil ? self.pastMeetingResponse!.meeting_data!.user_meetings!.count>0 : false)
                self.centerLbl.text = !(centerLbl.isHidden) ? "No Records Found." : ""
                self.tbleView.reloadData()
                if meetingRespons.meeting_data!.pagination?.currentPage! == 1{
                    DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.1) {
                        let indexpath = IndexPath(row: 0, section: 0)
                        self.tbleView.scrollToRow(at: indexpath, at: .top, animated: true)
                        
                    }
                }
            }catch let err {
                centerLbl.isHidden = false
                self.centerLbl.text = !(centerLbl.isHidden) ? "No Records Found." : ""
                print(err)
            }
        }
    }
    
    
}

class MeetingHeaderCell:UITableViewCell{
    @IBOutlet weak var lblDate: UILabel!
    
}

class MeetingTableCell:UITableViewCell{
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var lblMeetingDesc: UILabel!
    @IBOutlet weak var btnMap: MeetingButton!
    
}

class MeetingButton: UIButton {
    var indexPath:NSIndexPath! = nil
}
