//
//  QuestionListVC.swift
//  FirstInField
//
//  Created by Namespace  on 19/07/18.
//  Copyright © 2018 Namespace . All rights reserved.
//

import SkyFloatingLabelTextField
import UIKit
import ActionSheetPicker_3_0


class QuestionListVC: UIViewController, UITableViewDataSource,UITableViewDelegate {

    @IBOutlet weak var tbleView: MasterTableView!
    @IBOutlet weak var lblTimer: UILabel!{
        didSet{
            DispatchQueue.main.async {
                self.setDateTimer(metinngTime: self.checkListResponseMeeting, lblTimer: self.lblTimer)
            }
        }
    }
    @IBOutlet weak var lblQuestions: UILabel!
    enum Cell_Identifiers:String{
        case Single_Input = "SingleCell"
        case TwoTextCell = "TwoTextCell"
        case MultilineCell = "MultilineTextCell"
        case TimeCell  = "timeCell"
        case SingleTimeCell  = "SingletimeCell"
    }
    
    var meetingID:String! = ""
    var selectedRow:Int!
    var selectedSection:Int!
    var checkListResponseMeeting:Meeting?
    var questionlistRespons:QuestionListResponse?
    weak var questionListCell:QuestionListCell?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "loc_finder"), style: .plain, target: self, action: nil)
        
        let btn = UIButton(frame: CGRect.init(x: 0, y: 0, width: 90, height: 25))
        btn.layer.cornerRadius = 18
        btn.layer.borderColor = UIColor.white.cgColor
        btn.layer.borderWidth = 2.0
        btn.clipsToBounds = true
        let multipleAttributes: [NSAttributedStringKey : Any] = [
            NSAttributedStringKey.foregroundColor: UIColor.white,NSAttributedStringKey.font:UIFont(name: "Montserrat-SemiBold", size: 18.0)!]
        let attributedText = NSAttributedString(string: "Finish", attributes: multipleAttributes)
        btn.setAttributedTitle(attributedText, for: .normal)
        btn.setTitleColor(#colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), for: .normal)
        btn.addTarget(self, action: #selector(btnOnTapFinish), for: .touchUpInside)
        self.navigationItem.rightBarButtonItem?.customView = btn
        self.navigationItem.title = "Questions"
        // Do any additional setup after loading the view.
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
         getQuestionList()
    }
   
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc  func btnOnTapFinish() {
        
        if self.meetingID != ""{
            self.performSegue(withIdentifier: "SeguePopOver", sender: meetingID)
        }
        
    }
    
    
    //MARK:- QuestionList DATA
    func getQuestionList(){
        let resPonsedata = getUserDefaults(key: KeyResponseModel) as! Data
        let responseModel = try! jsonDecoder.decode(LoginResponse.self, from: resPonsedata)
        let token_part = "&api_token=" + API_Token + "&user_token=" + responseModel.user!.user_token!
        
        self.getDataFromNet(url: Urls.Get_QusetionList.rawValue  + meetingID + token_part , selector: #selector(QuestionListVC.handleNetResponse(data:)))
        
    }
    
    @objc func handleNetResponse(data:Data?){
        if let dat = data{
            do{
                questionlistRespons = try jsonDecoder.decode(QuestionListResponse.self, from: dat)
                if questionlistRespons?.res_msg != nil && questionlistRespons?.res_code! != 200{
                    self.showSnackBarWithMessage(message: responseModel!.res_msg!, color: #colorLiteral(red: 0.9254902005, green: 0.2352941185, blue: 0.101907857, alpha: 1), interval: .long, minusHeight:0)
                    return
                }
                
                if questionlistRespons!.question_data != nil && questionlistRespons!.question_data!.count > 0 {
                    self.tbleView.dataSource = self
                    self.tbleView.delegate = self
                    self.sectionCounts = [Int](repeating: 0, count: questionlistRespons!.question_data!.count)
                    self.sectionCounts![0] = 1
                    self.tbleView.reloadData()
                }else{
                    //self.showSnackBarWithMessage(message: "No Records Found", color: UIColor.red, txtColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), interval: .long, minusHeight: 0)
                    self.tbleView.isHidden = true
                    self.lblQuestions.isHidden = false
                }
                
            }catch let err{
                print(err)
            }
        }
    }
    
    func postSubmitAnswer(text:String?, id:String!, response:String? = nil ){
        self.view.endEditing(true)
        let completeUrl =  Urls.Post_Checklist_Answer.rawValue + (self.meetingID)! + "/question/" + id
        let resPonsedata = self.getUserDefaults(key: KeyResponseModel) as! Data
        let responseModel = try! self.jsonDecoder.decode(LoginResponse.self, from: resPonsedata)
        let request = CheckListSubmitRequest(response: response, remark: text, response_by: "\(responseModel.user!.id!)", api_token:API_Token, user_token:responseModel.user!.user_token!)
        self.postDataFromNet(url: completeUrl, requestData: request, selector:#selector(QuestionListVC.handleSumitResponse(data:)) )
    }
    
    
    @objc func handleSumitResponse(data:Data?) {
        if let dat = data{
            do{
                let res = try jsonDecoder.decode(SubmitResponse.self, from: dat)
                if res.res_msg != nil && res.res_code! != 200{
                    self.showSnackBarWithMessage(message: responseModel!.res_msg!, color: #colorLiteral(red: 0.9254902005, green: 0.2352941185, blue: 0.101907857, alpha: 1), interval: .long)
                    return
                }else  if res.res_msg != nil && res.res_code! == 200{
                    self.showSnackBarWithMessage(message: res.res_msg!, color: #colorLiteral(red: 0.2745098174, green: 0.4862745106, blue: 0.1411764771, alpha: 1), interval: .long, minusHeight:0)
                    self.questionlistRespons!.question_data![selectedSection].question![selectedRow].flag = 1
                    self.tbleView.reloadData()
//                    self.tbleView.reloadSections([selectedSection], with: .none)
//                    self.tbleView.reloadRows(at: [IndexPath(row: selectedRow, section: selectedSection)], with: .none)
                    
                    return
                }
                
            }catch let err{
                print(err)
            }
        }
    }
    
    //MARK:- TableView Delegate
    
    var sectionCounts:[Int]?
    func numberOfSections(in tableView: UITableView) -> Int {
        return questionlistRespons?.question_data != nil ? questionlistRespons!.question_data!.count:0
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sectionCounts![section] == 0 ? 0 : (questionlistRespons?.question_data?[section].question != nil ? questionlistRespons!.question_data![section].question!.count:0)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch questionlistRespons!.question_data![indexPath.section].question![indexPath.row].input_type {
        case InputType.Single_Input.rawValue:
            let cell = tableView.dequeueReusableCell(withIdentifier: Cell_Identifiers.Single_Input.rawValue, for: indexPath) as! QuestionListCell
            cell.controller =  self
            cell.indexPath = indexPath as NSIndexPath
            cell.txtSingleLineInput.attributedPlaceholder = NSAttributedString(string: "", attributes: [NSAttributedStringKey.foregroundColor: #colorLiteral(red: 0.2823529412, green: 0.2823529412, blue: 0.2823529412, alpha: 1)])
            cell.txtSingleLineInput.keyboardType = .default
            cell.type = InputType.Single_Input.rawValue
            
            //Btn Connect
            cell.btnConnect.isSelected = !(questionlistRespons!.question_data![indexPath.section].question![indexPath.row].flag! == 0)
            cell.btnChange.isSelected = selectedRow == indexPath.row && !(cell.btnConnect.isSelected)
            cell.updateButtons()
            
            cell.txtSingleLineInput.addTarget(cell, action: #selector(cell.textFieldEditingDidChange(_:)), for: UIControlEvents.editingChanged)
            cell.txtSingleLineInput.text = questionlistRespons!.question_data![indexPath.section].question![indexPath.row].response != nil ? questionlistRespons!.question_data![indexPath.section].question![indexPath.row].response! : nil
            cell.lblTitle.text = questionlistRespons!.question_data![indexPath.section].question![indexPath.row].title != nil ? questionlistRespons!.question_data![indexPath.section].question![indexPath.row].title! : nil
            cell.txtSingleLineInput.isEnabled = questionlistRespons!.question_data![indexPath.section].question![indexPath.row].flag! == 0
            return cell
        case InputType.MultilineInput.rawValue:
            let cell = tableView.dequeueReusableCell(withIdentifier: Cell_Identifiers.MultilineCell.rawValue, for: indexPath) as! QuestionListCell
            cell.controller =  self
            cell.type = InputType.MultilineInput.rawValue
            cell.indexPath = indexPath as NSIndexPath
            //Btn Connect
            cell.btnConnect.isSelected = !(questionlistRespons!.question_data![indexPath.section].question![indexPath.row].flag! == 0)
            cell.btnChange.isSelected = selectedRow == indexPath.row && !(cell.btnConnect.isSelected)
            cell.updateButtons()
            
            cell.tvInput.isEditable = questionlistRespons!.question_data![indexPath.section].question![indexPath.row].flag! == 0

            cell.tvInput.text = questionlistRespons!.question_data![indexPath.section].question![indexPath.row].response != nil ? questionlistRespons!.question_data![indexPath.section].question![indexPath.row].response! : nil
            cell.lblTitle.text = questionlistRespons!.question_data![indexPath.section].question![indexPath.row].title != nil ? questionlistRespons!.question_data![indexPath.section].question![indexPath.row].title! : nil
            
            return cell
        case InputType.Input_With_Phone_Validation.rawValue:
            let cell = tableView.dequeueReusableCell(withIdentifier: Cell_Identifiers.Single_Input.rawValue, for: indexPath) as! QuestionListCell
            cell.controller =  self
            cell.type = InputType.Input_With_Phone_Validation.rawValue
            cell.indexPath = indexPath as NSIndexPath
            cell.txtSingleLineInput.attributedPlaceholder = NSAttributedString(string: "Phone No", attributes: [NSAttributedStringKey.foregroundColor: #colorLiteral(red: 0.2823529412, green: 0.2823529412, blue: 0.2823529412, alpha: 1)])
            cell.txtSingleLineInput.keyboardType = .numberPad
            cell.txtSingleLineInput.isEnabled = questionlistRespons!.question_data![indexPath.section].question![indexPath.row].flag! == 0
            cell.lblTitle.text = questionlistRespons!.question_data![indexPath.section].question![indexPath.row].title != nil ? questionlistRespons!.question_data![indexPath.section].question![indexPath.row].title! : nil
            
            //Btn Connect
            cell.btnConnect.isSelected = !(questionlistRespons!.question_data![indexPath.section].question![indexPath.row].flag! == 0)
            cell.btnChange.isSelected = selectedRow == indexPath.row && !(cell.btnConnect.isSelected)
            cell.updateButtons()
            
            cell.txtSingleLineInput.addTarget(cell, action: #selector(cell.textFieldEditingDidChange(_:)), for: UIControlEvents.editingChanged)
            cell.txtSingleLineInput.text = questionlistRespons!.question_data![indexPath.section].question![indexPath.row].response != nil ? questionlistRespons!.question_data![indexPath.section].question![indexPath.row].response! : nil
            
            return cell
        case InputType.Input_With_Emai_Validation.rawValue:
            let cell = tableView.dequeueReusableCell(withIdentifier: Cell_Identifiers.Single_Input.rawValue, for: indexPath) as! QuestionListCell
            cell.controller =  self
            cell.type = InputType.Input_With_Emai_Validation.rawValue
            cell.indexPath = indexPath as NSIndexPath
            cell.txtSingleLineInput.keyboardType = .emailAddress
            cell.txtSingleLineInput.isEnabled = questionlistRespons!.question_data![indexPath.section].question![indexPath.row].flag! == 0
            cell.lblTitle.text = questionlistRespons!.question_data![indexPath.section].question![indexPath.row].title != nil ? questionlistRespons!.question_data![indexPath.section].question![indexPath.row].title! : nil
            
            //Btn Connect
            cell.btnConnect.isSelected = !(questionlistRespons!.question_data![indexPath.section].question![indexPath.row].flag! == 0)
            cell.btnChange.isSelected = selectedRow == indexPath.row && !(cell.btnConnect.isSelected)
            cell.updateButtons()
            
            cell.txtSingleLineInput.attributedPlaceholder = NSAttributedString(string: "Email", attributes: [NSAttributedStringKey.foregroundColor: #colorLiteral(red: 0.2823529412, green: 0.2823529412, blue: 0.2823529412, alpha: 1)])
            cell.txtSingleLineInput.addTarget(cell, action: #selector(cell.textFieldEditingDidChange(_:)), for: UIControlEvents.editingChanged)
            cell.txtSingleLineInput.text = questionlistRespons!.question_data![indexPath.section].question![indexPath.row].response != nil ? questionlistRespons!.question_data![indexPath.section].question![indexPath.row].response! : nil
            
            return cell
        case InputType.Input_type_time_range.rawValue:
            let cell = tableView.dequeueReusableCell(withIdentifier: Cell_Identifiers.TimeCell.rawValue, for: indexPath) as! QuestionListCell
            cell.controller =  self
            cell.tittle = "Time"
            cell.mode = UIDatePickerMode.time
            cell.indexPath = indexPath as NSIndexPath
            cell.type = InputType.Input_type_time_range.rawValue
            cell.txtFrom.attributedPlaceholder = NSAttributedString(string: "Start Time", attributes: [NSAttributedStringKey.foregroundColor: #colorLiteral(red: 0.2823529412, green: 0.2823529412, blue: 0.2823529412, alpha: 1)])
            cell.txtTo.attributedPlaceholder = NSAttributedString(string: "End Time", attributes: [NSAttributedStringKey.foregroundColor: #colorLiteral(red: 0.2823529412, green: 0.2823529412, blue: 0.2823529412, alpha: 1)])

            cell.txtFrom.isEnabled = questionlistRespons!.question_data![indexPath.section].question![indexPath.row].flag! == 0
            cell.txtTo.isEnabled = questionlistRespons!.question_data![indexPath.section].question![indexPath.row].flag! == 0
            cell.txtTo.addTarget(cell, action: #selector(cell.textFieldEditingDidChange(_:)), for: UIControlEvents.editingChanged)
            cell.txtFrom.addTarget(cell, action: #selector(cell.textFieldEditingDidChange(_:)), for: UIControlEvents.editingChanged)
            cell.txtFrom.text = questionlistRespons!.question_data![indexPath.section].question![indexPath.row].from != nil ? questionlistRespons!.question_data![indexPath.section].question![indexPath.row].from! : nil
            cell.txtTo.text = questionlistRespons!.question_data![indexPath.section].question![indexPath.row].to != nil ? questionlistRespons!.question_data![indexPath.section].question![indexPath.row].to! : nil
            //Btn Connect
            cell.btnConnect.isSelected = !(questionlistRespons!.question_data![indexPath.section].question![indexPath.row].flag! == 0)
            cell.btnChange.isSelected = selectedRow == indexPath.row && !(cell.btnConnect.isSelected)
            cell.updateButtons()
            
            cell.lblTitle.text = questionlistRespons!.question_data![indexPath.section].question![indexPath.row].title != nil ? questionlistRespons!.question_data![indexPath.section].question![indexPath.row].title! : nil
            
            return cell
        case InputType.Input_type_date_range.rawValue:
            let cell = tableView.dequeueReusableCell(withIdentifier: Cell_Identifiers.TimeCell.rawValue, for: indexPath) as! QuestionListCell
            cell.controller =  self
            cell.tittle = "Date"
            cell.mode = UIDatePickerMode.date
            cell.indexPath = indexPath as NSIndexPath
            cell.type = InputType.Input_type_date_range.rawValue
            cell.txtFrom.attributedPlaceholder = NSAttributedString(string: "Start Date", attributes: [NSAttributedStringKey.foregroundColor: #colorLiteral(red: 0.2823529412, green: 0.2823529412, blue: 0.2823529412, alpha: 1)])
            cell.txtTo.attributedPlaceholder = NSAttributedString(string: "End date", attributes: [NSAttributedStringKey.foregroundColor: #colorLiteral(red: 0.2823529412, green: 0.2823529412, blue: 0.2823529412, alpha: 1) ])
            //Btn Connect
            cell.btnConnect.isSelected = !(questionlistRespons!.question_data![indexPath.section].question![indexPath.row].flag! == 0)
            cell.btnChange.isSelected = selectedRow == indexPath.row && !(cell.btnConnect.isSelected)
            cell.updateButtons()
            
            cell.txtFrom.isEnabled = questionlistRespons!.question_data![indexPath.section].question![indexPath.row].flag! == 0
            cell.txtTo.isEnabled = questionlistRespons!.question_data![indexPath.section].question![indexPath.row].flag! == 0
         
            cell.txtTo.addTarget(cell, action: #selector(cell.textFieldEditingDidChange(_:)), for: UIControlEvents.editingChanged)
            cell.txtFrom.addTarget(cell, action: #selector(cell.textFieldEditingDidChange(_:)), for: UIControlEvents.editingChanged)
            cell.txtFrom.text = questionlistRespons!.question_data![indexPath.section].question![indexPath.row].from != nil ? questionlistRespons!.question_data![indexPath.section].question![indexPath.row].from! : nil
            cell.txtTo.text = questionlistRespons!.question_data![indexPath.section].question![indexPath.row].to != nil ? questionlistRespons!.question_data![indexPath.section].question![indexPath.row].to! : nil
            
            
            cell.lblTitle.text = questionlistRespons!.question_data![indexPath.section].question![indexPath.row].title != nil ? questionlistRespons!.question_data![indexPath.section].question![indexPath.row].title! : nil
            
            return cell
        case InputType.Input_type_time.rawValue:
            let cell = tableView.dequeueReusableCell(withIdentifier: Cell_Identifiers.SingleTimeCell.rawValue, for: indexPath) as! QuestionListCell
            cell.controller =  self
            cell.tittle = "Time"
            cell.mode = UIDatePickerMode.time
            cell.indexPath = indexPath as NSIndexPath
            cell.type = InputType.Input_type_time.rawValue
            cell.txtFrom.attributedPlaceholder = NSAttributedString(string: "Time", attributes: [NSAttributedStringKey.foregroundColor: #colorLiteral(red: 0.2823529412, green: 0.2823529412, blue: 0.2823529412, alpha: 1)])
            cell.txtFrom.addTarget(cell, action: #selector(cell.textFieldEditingDidChange(_:)), for: UIControlEvents.editingChanged)
            cell.txtFrom.text = questionlistRespons!.question_data![indexPath.section].question![indexPath.row].from != nil ? questionlistRespons!.question_data![indexPath.section].question![indexPath.row].from! : nil
            //Btn Connect
            cell.btnConnect.isSelected = !(questionlistRespons!.question_data![indexPath.section].question![indexPath.row].flag! == 0)
            cell.btnChange.isSelected = selectedRow == indexPath.row && !(cell.btnConnect.isSelected)
            cell.updateButtons()
            
            cell.txtFrom.isEnabled = questionlistRespons!.question_data![indexPath.section].question![indexPath.row].flag! == 0
            cell.lblTitle.text = questionlistRespons!.question_data![indexPath.section].question![indexPath.row].title != nil ? questionlistRespons!.question_data![indexPath.section].question![indexPath.row].title! : nil
            
            return cell
        case InputType.Input_type_date.rawValue:
            let cell = tableView.dequeueReusableCell(withIdentifier: Cell_Identifiers.SingleTimeCell.rawValue, for: indexPath) as! QuestionListCell
            cell.controller =  self
            cell.tittle = "Date"
            cell.mode = UIDatePickerMode.date
            cell.indexPath = indexPath as NSIndexPath
            cell.type = InputType.Input_type_date.rawValue
            cell.txtFrom.attributedPlaceholder = NSAttributedString(string: "Date", attributes: [NSAttributedStringKey.foregroundColor: #colorLiteral(red: 0.2823529412, green: 0.2823529412, blue: 0.2823529412, alpha: 1)])
            cell.txtFrom.addTarget(cell, action: #selector(cell.textFieldEditingDidChange(_:)), for: UIControlEvents.editingChanged)
            cell.txtFrom.text = questionlistRespons!.question_data![indexPath.section].question![indexPath.row].from != nil ? questionlistRespons!.question_data![indexPath.section].question![indexPath.row].from! : nil
            //Btn Connect
            cell.btnConnect.isSelected = !(questionlistRespons!.question_data![indexPath.section].question![indexPath.row].flag! == 0)
            cell.btnChange.isSelected = selectedRow == indexPath.row && !(cell.btnConnect.isSelected)
            cell.updateButtons()
            cell.txtFrom.isEnabled = questionlistRespons!.question_data![indexPath.section].question![indexPath.row].flag! == 0
            cell.lblTitle.text = questionlistRespons!.question_data![indexPath.section].question![indexPath.row].title != nil ? questionlistRespons!.question_data![indexPath.section].question![indexPath.row].title! : nil
            
            return cell
        case InputType.Input_type_datetime.rawValue:
            let cell = tableView.dequeueReusableCell(withIdentifier: Cell_Identifiers.SingleTimeCell.rawValue, for: indexPath) as! QuestionListCell
            cell.controller =  self
            cell.tittle = "Date With Time"
            cell.mode = UIDatePickerMode.dateAndTime
            cell.indexPath = indexPath as NSIndexPath
            cell.type = InputType.Input_type_datetime.rawValue
            cell.txtFrom.attributedPlaceholder = NSAttributedString(string: "Date and Time", attributes: [NSAttributedStringKey.foregroundColor: #colorLiteral(red: 0.2823529412, green: 0.2823529412, blue: 0.2823529412, alpha: 1)])
            cell.txtFrom.addTarget(cell, action: #selector(cell.textFieldEditingDidChange(_:)), for: UIControlEvents.editingChanged)
            cell.txtFrom.text = questionlistRespons!.question_data![indexPath.section].question![indexPath.row].from != nil ? questionlistRespons!.question_data![indexPath.section].question![indexPath.row].from! : nil
            //Btn Connect
            cell.btnConnect.isSelected = !(questionlistRespons!.question_data![indexPath.section].question![indexPath.row].flag! == 0)
            cell.btnChange.isSelected = selectedRow == indexPath.row && !(cell.btnConnect.isSelected)
            cell.updateButtons()
            
            cell.txtFrom.isEnabled = questionlistRespons!.question_data![indexPath.section].question![indexPath.row].flag! == 0
            cell.lblTitle.text = questionlistRespons!.question_data![indexPath.section].question![indexPath.row].title != nil ? questionlistRespons!.question_data![indexPath.section].question![indexPath.row].title! : nil
            
            return cell
            
        default:
            break
        }
        let cell = tableView.dequeueReusableCell(withIdentifier:  Cell_Identifiers.Single_Input.rawValue, for: indexPath) as! QuestionListCell
        return cell
        
    }
    
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = tableView.dequeueReusableCell(withIdentifier: "headerCell") as! QuestionListHeader
        view.lblHeaderTitle.text = self.questionlistRespons!.question_data![section].category!
        view.ivArrow.image = sectionCounts![section] == 0 ? #imageLiteral(resourceName: "pointArrow") : #imageLiteral(resourceName: "down_arr_white")
        view.vwbtm.backgroundColor = sectionCounts![section] == 0 ? #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1): UIColor.clear
        view.section = section
        view.controler = self
        return view
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if let ident = segue.identifier{
            switch ident {
            case "SeguePopOver":
                let des = segue.destination as! PopUpViewController
                des.forView = "finish"
                des.meetingId = sender as! String
                des.checkListResponseMeeting = self.checkListResponseMeeting
                break
            default:
                break
            }
        }
    }
 

}


class QuestionListHeader:UITableViewCell{
    @IBOutlet weak var lblHeaderTitle: UILabel!
    @IBOutlet weak var ivArrow: UIImageView!
    @IBOutlet weak var vwbtm: UIView!
    
    var section:Int!
    weak var controler:QuestionListVC?
    
    @IBAction func btnOnTapExpand(_ sender: UIButton) {
        if controler?.sectionCounts![section] == 0{
            controler?.sectionCounts![section] = 1 //(controler?.questionlistRespons!.question_data![section].question!.count)!
            controler?.tbleView.reloadData()
            self.ivArrow.image = #imageLiteral(resourceName: "down_arr_white")
        }else{
            controler?.sectionCounts![section] = 0
            controler?.tbleView.reloadData()
            self.ivArrow.image = #imageLiteral(resourceName: "pointArrow")
        }
    }
    
}


class QuestionListCell:UITableViewCell, UITextFieldDelegate, UITextViewDelegate{
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var txtFrom: UITextField!
    @IBOutlet weak var txtTo: UITextField!
    @IBOutlet weak var btnChange: UIButton!
    @IBOutlet weak var btnConnect: UIButton!
    @IBOutlet weak var txtSingleLineInput: UITextField!
    @IBOutlet weak var txtAddressA: UITextField!
    @IBOutlet weak var txtAddresB: UITextField!
    @IBOutlet weak var tvInput: UITextView!
    
    
    var type:String!
    var indexPath:NSIndexPath!
    weak var controller:QuestionListVC?
    var tittle:String!
    var mode:UIDatePickerMode!
    var fromDate:Date?
    
    func updateButtons(){
        
        self.btnConnect.backgroundColor = self.btnConnect.isSelected ? #colorLiteral(red: 0, green: 0.7526736259, blue: 0.4154895842, alpha: 1) : UIColor.clear
        self.btnConnect.layer.borderColor = self.btnConnect.isSelected ? UIColor.clear.cgColor : #colorLiteral(red: 0.3536440134, green: 0.3536530137, blue: 0.3536481857, alpha: 1).cgColor
        
        self.btnChange.backgroundColor = self.btnChange.isSelected ? UIColor.red : UIColor.clear
        self.btnChange.layer.borderColor = self.btnChange.isSelected ? UIColor.clear.cgColor : #colorLiteral(red: 0.3536440134, green: 0.3536530137, blue: 0.3536481857, alpha: 1).cgColor
        
    }
    
    func showPicker(title:String, mode:UIDatePickerMode, textField:UITextField, section:Int, row:Int){
//        if textField.tag == 1  && controller?.questionlistRespons!.question_data![indexPath.section].question![indexPath.row]. == nil{
//            controller?.showSnackBarWithMessage(message: "Select From date first.", color: UIColor.red, interval: .long, minusHeight:0)
//            return
//        }
        let actionSheet = ActionSheetDatePicker(title: title, datePickerMode: mode, selectedDate: fromDate == nil ? Date():fromDate , doneBlock: {[weak self] (picker, value, vale1) in
            let formatter = DateFormatter()
            switch(mode){
            case .date:
                formatter.dateFormat = "yyyy-MM-dd"
                break
            case .time:
                formatter.dateFormat = "hh:mm a"
                formatter.amSymbol = "am"
                formatter.pmSymbol = "pm"
                break
            case .dateAndTime:
                formatter.dateFormat = "yyyy-MM-dd hh:mm a"
                break
            case .countDownTimer:
                break
            }
            
            textField.text = formatter.string(from: value as! Date)
            if textField.tag == 0{
                self?.controller?.questionlistRespons!.question_data![section].question![row].fromDate = value as? Date
                self?.controller?.questionlistRespons!.question_data![section].question![row].from = textField.text
                
            }else{
                self?.controller?.questionlistRespons!.question_data![section].question![row].toDate = value as? Date
                self?.controller?.questionlistRespons!.question_data![section].question![row].to = textField.text
            }
            
            }, cancel: { (picker) in
                
        }, origin: controller?.view)
        actionSheet?.minimumDate = self.controller?.questionlistRespons!.question_data![section].question![row].fromDate == nil ? Date():self.controller?.questionlistRespons!.question_data![section].question![row].fromDate!
        actionSheet?.show()
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        btnChange.isSelected = true
        btnConnect.isSelected = false
        updateButtons()
        
        if textField == txtFrom || textField == txtTo{
            showPicker(title: tittle, mode: mode, textField: textField, section: indexPath.section, row: indexPath.row)
            return false
        }
        return true
    }
    
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        btnChange.isSelected = true
        btnConnect.isSelected = false
        updateButtons()
        
        if textField.text == controller?.questionlistRespons!.question_data![indexPath.section].question![indexPath.row].temResp! && controller?.questionlistRespons!.question_data![indexPath.section].question![indexPath.row].flag! == 1{
            self.btnConnect.backgroundColor = #colorLiteral(red: 0, green: 0.7526736259, blue: 0.4154895842, alpha: 1)
            self.btnConnect.layer.borderColor = UIColor.clear.cgColor
            self.btnConnect.setTitleColor(#colorLiteral(red: 1, green: 1, blue: 1, alpha: 1), for: .normal)
            controller?.questionlistRespons!.question_data![indexPath.section].question![indexPath.row].flag! = 1
        }else{
            self.btnConnect.backgroundColor = UIColor.clear
            self.btnConnect.setTitleColor(#colorLiteral(red: 0.3536440134, green: 0.3536530137, blue: 0.3536481857, alpha: 1), for: .normal)
            self.btnConnect.layer.borderColor = #colorLiteral(red: 0.3536440134, green: 0.3536530137, blue: 0.3536481857, alpha: 1).cgColor
            controller?.questionlistRespons!.question_data![indexPath.section].question![indexPath.row].flag! = 0
        }
    }
    
    
    @IBAction func textFieldEditingDidChange(_ sender: UITextField){
        switch type {
        case InputType.Single_Input.rawValue, InputType.Input_With_Phone_Validation.rawValue, InputType.Input_With_Emai_Validation.rawValue, InputType.MultilineInput.rawValue:
            controller?.questionlistRespons!.question_data![indexPath.section].question![indexPath.row].response = sender.text
            break
        case InputType.Input_type_time.rawValue, InputType.Input_type_date.rawValue, InputType.Input_type_datetime.rawValue,InputType.Input_type_date_range.rawValue,InputType.Input_type_time_range.rawValue:
            if sender == txtFrom{
                controller?.questionlistRespons!.question_data![indexPath.section].question![indexPath.row].from = sender.text
            }else if sender == txtTo{
                controller?.questionlistRespons!.question_data![indexPath.section].question![indexPath.row].to = sender.text
            }
            
            break
        default:
            break
        }
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        btnChange.isSelected = true
        btnConnect.isSelected = false
        updateButtons()
    }
    
    public func textViewDidChange(_ textView: UITextView){
        controller?.questionlistRespons!.question_data![indexPath.section].question![indexPath.row].response = textView.text!
    }
    
    //MARK:- Cell Button Action
    @IBAction func btnOnTapChange(_ sender: LoginButton) {
        btnChange.isSelected = true
        btnConnect.isSelected = false
        updateButtons()
        
        switch type {
        case InputType.Single_Input.rawValue:
            txtSingleLineInput.isEnabled = true
            txtSingleLineInput.becomeFirstResponder()
            break
        case InputType.Input_With_Phone_Validation.rawValue:
            txtSingleLineInput.isEnabled = true
            txtSingleLineInput.becomeFirstResponder()
            break
        case InputType.Input_With_Emai_Validation.rawValue:
            txtSingleLineInput.isEnabled = true
            txtSingleLineInput.becomeFirstResponder()
            break
        case InputType.MultilineInput.rawValue:
            tvInput.isEditable = true
            tvInput.becomeFirstResponder()
            break
        case InputType.Input_type_time.rawValue:
            txtFrom.isEnabled = true
            txtFrom.becomeFirstResponder()
            break
        case InputType.Input_type_date.rawValue:
            txtFrom.isEnabled = true
            txtFrom.becomeFirstResponder()
            break
        case InputType.Input_type_datetime.rawValue:
            txtFrom.isEnabled = true
            txtFrom.becomeFirstResponder()
            break
        case InputType.Input_type_time_range.rawValue:
            txtTo.isEnabled = true
            txtFrom.isEnabled = true
            txtFrom.becomeFirstResponder()
            break
        case InputType.Input_type_date_range.rawValue:
            txtTo.isEnabled = true
            txtFrom.isEnabled = true
            txtFrom.becomeFirstResponder()
            
            break
        default:
            break
        }
    }
    
    @IBAction func btnOnTapConnect(_ sender: LoginButton) {
        controller?.selectedRow = indexPath.row
        controller?.selectedSection = indexPath.section
        switch type {
        case InputType.Single_Input.rawValue:
            if !txtSingleLineInput.isTextFieldEmpty(){
                let id  =  controller?.questionlistRespons!.question_data![indexPath.section].question![indexPath.row].id!
                controller?.showInputDialog(title: "Alert", subtitle: "Please submit a reason", actionTitle: "Submit", cancelTitle: "Cancel", inputPlaceholder: "Reason", inputKeyboardType: .default, cancelHandler: {[weak self] (action) in
                    self?.controller?.view.endEditing(true)
                    }, actionHandler: {[weak self] (text) in
                        if text != nil && text!.isNotEmptyString {
                            self?.controller!.postSubmitAnswer(text: text!, id: "\(id!)")
                        }else{
                            self?.controller?.view.endEditing(true)
                            self?.controller!.showSnackBarWithMessage(message: "Error: Empty Remarks can not be submitted.", color: UIColor.red, interval: .long, minusHeight:0)
                        }
                })
                return
            }else{
                let id  =  controller?.questionlistRespons!.question_data![indexPath.section].question![indexPath.row].id!
                self.controller!.postSubmitAnswer(text: nil, id: "\(id!)",response: txtSingleLineInput.text!)
                //self.btnConnect.isEnabled = false
            }
        case InputType.Input_With_Phone_Validation.rawValue:
            if !txtSingleLineInput.isTextFieldEmpty(){
                let id  =  controller?.questionlistRespons!.question_data![indexPath.section].question![indexPath.row].id!
                controller?.showInputDialog(title: "Alert", subtitle: "Please submit a reason", actionTitle: "Submit", cancelTitle: "Cancel", inputPlaceholder: "Reason", inputKeyboardType: .default, cancelHandler: {[weak self] (action) in
                    self?.controller?.view.endEditing(true)
                    }, actionHandler: {[weak self] (text) in
                        if text != nil && text!.isNotEmptyString {
                            self?.controller!.postSubmitAnswer(text: text!, id: "\(id!)")
                        }else{
                            self?.controller?.view.endEditing(true)
                            self?.controller!.showSnackBarWithMessage(message: "Error: Empty Remarks can not be submitted.", color: UIColor.red, interval: .long, minusHeight:0)
                        }
                })
                return
            }
            if !txtSingleLineInput.isPhoneNumber{
                controller?.showSnackBarWithMessage(message: "No Valid Phone no.", color: UIColor.red, interval: .long, minusHeight:0)
                return
            }
            let id  =  controller?.questionlistRespons!.question_data![indexPath.section].question![indexPath.row].id!
            self.controller!.postSubmitAnswer(text: nil, id: "\(id!)",response: txtSingleLineInput.text!)
            //self.btnConnect.isEnabled = false
            
        case InputType.Input_With_Emai_Validation.rawValue:
            if !txtSingleLineInput.isTextFieldEmpty(){
                //controller?.showSnackBarWithMessage(message: "Email is empty.", color: UIColor.red, interval: .long, minusHeight:0)
                let id  =  controller?.questionlistRespons!.question_data![indexPath.section].question![indexPath.row].id!
                controller?.showInputDialog(title: "Alert", subtitle: "Please submit a reason", actionTitle: "Submit", cancelTitle: "Cancel", inputPlaceholder: "Reason", inputKeyboardType: .default, cancelHandler: {[weak self] (action) in
                    self?.controller?.view.endEditing(true)
                    }, actionHandler: {[weak self] (text) in
                        if text != nil && text!.isNotEmptyString {
                            self?.controller!.postSubmitAnswer(text: text!, id: "\(id!)")
                        }else{
                            self?.controller?.view.endEditing(true)
                            self?.controller!.showSnackBarWithMessage(message: "Error: Empty Remarks can not be submitted.", color: UIColor.red, interval: .long, minusHeight:0)
                        }
                })
                return
            }
            if !txtSingleLineInput.isValidEmail(){
                controller?.showSnackBarWithMessage(message: "No Valid Email.", color: UIColor.red, interval: .long, minusHeight:0)
                return
            }
            let id  =  controller?.questionlistRespons!.question_data![indexPath.section].question![indexPath.row].id!
            self.controller!.postSubmitAnswer(text: nil, id: "\(id!)",response: txtSingleLineInput.text!)
            //self.btnConnect.isEnabled = false
            
            break
        case InputType.MultilineInput.rawValue:
            if !tvInput.text.isNotEmptyString{
                let id  =  controller?.questionlistRespons!.question_data![indexPath.section].question![indexPath.row].id!
                controller?.showInputDialog(title: "Alert", subtitle: "Please submit a reason", actionTitle: "Submit", cancelTitle: "Cancel", inputPlaceholder: "Reason", inputKeyboardType: .default, cancelHandler: {[weak self] (action) in
                    self?.controller?.view.endEditing(true)
                    }, actionHandler: {[weak self] (text) in
                        if text != nil && text!.isNotEmptyString {
                            self?.controller!.postSubmitAnswer(text: text!, id: "\(id!)")
                        }else{
                            self?.controller?.view.endEditing(true)
                            self?.controller!.showSnackBarWithMessage(message: "Error: Empty Remarks can not be submitted.", color: UIColor.red, interval: .long, minusHeight:0)
                        }
                })
                return
            }
            
            
            let id  =  controller?.questionlistRespons!.question_data![indexPath.section].question![indexPath.row].id!
            self.controller!.postSubmitAnswer(text: nil, id: "\(id!)",response: tvInput.text!)
            //self.btnConnect.isEnabled = false
            
            break
        case InputType.Input_type_time_range.rawValue:
            if !txtFrom.isTextFieldEmpty(){
                //controller?.showSnackBarWithMessage(message: "Provide an start time", color: UIColor.red, interval: .long, minusHeight:0)
                let id  =  controller?.questionlistRespons!.question_data![indexPath.section].question![indexPath.row].id!
                controller?.showInputDialog(title: "Alert", subtitle: "Please submit a reason", actionTitle: "Submit", cancelTitle: "Cancel", inputPlaceholder: "Reason", inputKeyboardType: .default, cancelHandler: {[weak self] (action) in
                    self?.controller?.view.endEditing(true)
                    }, actionHandler: {[weak self] (text) in
                        if text != nil && text!.isNotEmptyString {
                            self?.controller!.postSubmitAnswer(text: text!, id: "\(id!)")
                        }else{
                            self?.controller?.view.endEditing(true)
                            self?.controller!.showSnackBarWithMessage(message: "Error: Empty Remarks can not be submitted.", color: UIColor.red, interval: .long, minusHeight:0)
                        }
                })
                return
            }
            if !txtTo.isTextFieldEmpty(){
                //controller?.showSnackBarWithMessage(message: "Provide an end time", color: UIColor.red, interval: .long, minusHeight:0)
                let id  =  controller?.questionlistRespons!.question_data![indexPath.section].question![indexPath.row].id!
                controller?.showInputDialog(title: "Alert", subtitle: "Please submit a reason", actionTitle: "Submit", cancelTitle: "Cancel", inputPlaceholder: "Reason", inputKeyboardType: .default, cancelHandler: {[weak self] (action) in
                    self?.controller?.view.endEditing(true)
                    }, actionHandler: {[weak self] (text) in
                        if text != nil && text!.isNotEmptyString {
                            self?.controller!.postSubmitAnswer(text: text!, id: "\(id!)")
                        }else{
                            self?.controller?.view.endEditing(true)
                            self?.controller!.showSnackBarWithMessage(message: "Error: Empty Remarks can not be submitted.", color: UIColor.red, interval: .long, minusHeight:0)
                        }
                })
                return
            }
            let id  =  controller?.questionlistRespons!.question_data![indexPath.section].question![indexPath.row].id!
            self.controller!.postSubmitAnswer(text: nil, id: "\(id!)",response: txtFrom.text! + "#"  + txtTo.text! )
            //self.btnConnect.isEnabled = false
            
            break
        case InputType.Input_type_date_range.rawValue:
            if !txtFrom.isTextFieldEmpty(){
                //controller?.showSnackBarWithMessage(message: "Provide an start date", color: UIColor.red, interval: .long, minusHeight:0)
                let id  =  controller?.questionlistRespons!.question_data![indexPath.section].question![indexPath.row].id!
                controller?.showInputDialog(title: "Alert", subtitle: "Please submit a reason", actionTitle: "Submit", cancelTitle: "Cancel", inputPlaceholder: "Reason", inputKeyboardType: .default, cancelHandler: {[weak self] (action) in
                    self?.controller?.view.endEditing(true)
                    }, actionHandler: {[weak self] (text) in
                        if text != nil && text!.isNotEmptyString {
                            self?.controller!.postSubmitAnswer(text: text!, id: "\(id!)")
                        }else{
                            self?.controller?.view.endEditing(true)
                            self?.controller!.showSnackBarWithMessage(message: "Error: Empty Remarks can not be submitted.", color: UIColor.red, interval: .long, minusHeight:0)
                        }
                })
                return
            }
            if !txtTo.isTextFieldEmpty(){
                // controller?.showSnackBarWithMessage(message: "Provide an end date", color: UIColor.red, interval: .long, minusHeight:0)
                let id  =  controller?.questionlistRespons!.question_data![indexPath.section].question![indexPath.row].id!
                controller?.showInputDialog(title: "Alert", subtitle: "Please submit a reason", actionTitle: "Submit", cancelTitle: "Cancel", inputPlaceholder: "Reason", inputKeyboardType: .default, cancelHandler: {[weak self] (action) in
                    self?.controller?.view.endEditing(true)
                    }, actionHandler: {[weak self] (text) in
                        if text != nil && text!.isNotEmptyString {
                            self?.controller!.postSubmitAnswer(text: text!, id: "\(id!)")
                        }else{
                            self?.controller?.view.endEditing(true)
                            self?.controller!.showSnackBarWithMessage(message: "Error: Empty Remarks can not be submitted.", color: UIColor.red, interval: .long, minusHeight:0)
                        }
                })
                return
            }
            let id  =  controller?.questionlistRespons!.question_data![indexPath.section].question![indexPath.row].id!
            self.controller!.postSubmitAnswer(text: nil, id: "\(id!)",response: txtFrom.text! + "#"  + txtTo.text! )
            //self.btnConnect.isEnabled = false
            
            break
        case InputType.Input_type_datetime.rawValue:
            if !txtFrom.isTextFieldEmpty(){
                //controller?.showSnackBarWithMessage(message: "Provide an start date and time", color: UIColor.red, interval: .long, minusHeight:0)
                let id  =  controller?.questionlistRespons!.question_data![indexPath.section].question![indexPath.row].id!
                controller?.showInputDialog(title: "Alert", subtitle: "Please submit a reason", actionTitle: "Submit", cancelTitle: "Cancel", inputPlaceholder: "Reason", inputKeyboardType: .default, cancelHandler: {[weak self] (action) in
                    self?.controller?.view.endEditing(true)
                    }, actionHandler: {[weak self] (text) in
                        if text != nil && text!.isNotEmptyString {
                            self?.controller!.postSubmitAnswer(text: text!, id: "\(id!)")
                        }else{
                            self?.controller?.view.endEditing(true)
                            self?.controller!.showSnackBarWithMessage(message: "Error: Empty Remarks can not be submitted.", color: UIColor.red, interval: .long, minusHeight:0)
                        }
                })
                return
            }
            let id  =  controller?.questionlistRespons!.question_data![indexPath.section].question![indexPath.row].id!
            self.controller!.postSubmitAnswer(text: nil, id: "\(id!)",response: txtFrom.text! )
            //self.btnConnect.isEnabled = false
            
            break
        case InputType.Input_type_date.rawValue:
            if !txtFrom.isTextFieldEmpty(){
                //controller?.showSnackBarWithMessage(message: "Provide an start date and time", color: UIColor.red, interval: .long, minusHeight:0)
                let id  =  controller?.questionlistRespons!.question_data![indexPath.section].question![indexPath.row].id!
                controller?.showInputDialog(title: "Alert", subtitle: "Please submit a reason", actionTitle: "Submit", cancelTitle: "Cancel", inputPlaceholder: "Reason", inputKeyboardType: .default, cancelHandler: {[weak self] (action) in
                    self?.controller?.view.endEditing(true)
                    }, actionHandler: {[weak self] (text) in
                        if text != nil && text!.isNotEmptyString {
                            self?.controller!.postSubmitAnswer(text: text!, id: "\(id!)")
                        }else{
                            self?.controller?.view.endEditing(true)
                            self?.controller!.showSnackBarWithMessage(message: "Error: Empty Remarks can not be submitted.", color: UIColor.red, interval: .long, minusHeight:0)
                        }
                })
                return
            }
            let id  =  controller?.questionlistRespons!.question_data![indexPath.section].question![indexPath.row].id!
            self.controller!.postSubmitAnswer(text: nil, id: "\(id!)",response: txtFrom.text! )
            //self.btnConnect.isEnabled = false
            
            break
            
        case InputType.Input_type_time.rawValue:
            if !txtFrom.isTextFieldEmpty(){
                //controller?.showSnackBarWithMessage(message: "Provide an start date and time", color: UIColor.red, interval: .long, minusHeight:0)
                let id  =  controller?.questionlistRespons!.question_data![indexPath.section].question![indexPath.row].id!
                controller?.showInputDialog(title: "Alert", subtitle: "Please submit a reason", actionTitle: "Submit", cancelTitle: "Cancel", inputPlaceholder: "Reason", inputKeyboardType: .default, cancelHandler: {[weak self] (action) in
                    self?.controller?.view.endEditing(true)
                    }, actionHandler: {[weak self] (text) in
                        if text != nil && text!.isNotEmptyString {
                            self?.controller!.postSubmitAnswer(text: text!, id: "\(id!)")
                        }else{
                            self?.controller?.view.endEditing(true)
                            self?.controller!.showSnackBarWithMessage(message: "Error: Empty Remarks can not be submitted.", color: UIColor.red, interval: .long, minusHeight:0)
                        }
                })
                return
            }
            let id  =  controller?.questionlistRespons!.question_data![indexPath.section].question![indexPath.row].id!
            self.controller!.postSubmitAnswer(text: nil, id: "\(id!)",response: txtFrom.text! )
            //self.btnConnect.isEnabled = false
            
            break
        default:
            break
        }
    }
    
    
}

