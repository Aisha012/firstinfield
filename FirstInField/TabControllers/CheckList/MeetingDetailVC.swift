//
//  MeetingDetailVC.swift
//  FirstInField
//
//  Created by Namespace  on 14/07/18.
//  Copyright © 2018 Namespace . All rights reserved.
//
import SDWebImage
import UIKit
import MobileCoreServices
import CoreLocation
//import SKPhotoBrowser
import QuickLook


class MeetingDetailVC: UIViewController, UITableViewDataSource, UITableViewDelegate,CLLocationManagerDelegate, QLPreviewControllerDataSource, QLPreviewControllerDelegate {

    @IBOutlet weak var btnChat: UIButton!
    @IBOutlet weak var tbleView: UITableView!
    var fromVC = ""
    var meetingID:String = ""
    var selectedIndex = 0
    let locationManager = CLLocationManager()
    var titleStr:String = "Meeting"
    var meetingStatus = ""
    var saveAction = UIAlertAction()
    var previewController = QLPreviewController()
    var previewItem = [NSURL]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tbleView.rowHeight = UITableViewAutomaticDimension
        self.tbleView.estimatedRowHeight = 220
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        self.navigationItem.title = titleStr
       
         NotificationCenter.default.addObserver(self, selector: #selector(self.getMeetingDetails), name: Notification.Name("refreshMeeting"), object: nil)
       
//        self.btnChat.isHidden = true
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
         getMeetingDetails()
         setNavigationBar()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    var meetingDeatail:MeetingDetailResponse?
    @objc func handleSumitResponse(data:Data?) {
        if let dat = data{
            do{
                meetingDeatail = try jsonDecoder.decode(MeetingDetailResponse.self, from: dat)
                if meetingDeatail?.res_msg != nil && meetingDeatail?.res_code! != 200{
                    self.showSnackBarWithMessage(message: responseModel!.res_msg!, color: #colorLiteral(red: 0.9254902005, green: 0.2352941185, blue: 0.101907857, alpha: 1), interval: .long)
                    return
                }
                meetingStatus = meetingDeatail?.meeting_detail_data?.meeting_details?.status ?? "\(3)"
                self.setNavigationBar()
                self.tbleView.reloadData()
            }catch let err{
                print(err)
            }
        }
    }
    
    func setNavigationBar(){
//        if self.selectedIndex == 0 {
            let btn = UIButton(frame: CGRect.init(x: 0, y: 0, width: 90, height: 25))
            btn.layer.cornerRadius = 18
            btn.layer.borderColor = UIColor.white.cgColor
            btn.layer.borderWidth = 2.0
            btn.clipsToBounds = true
            let multipleAttributes: [NSAttributedStringKey : Any] = [
                NSAttributedStringKey.foregroundColor: UIColor.white,NSAttributedStringKey.font:UIFont(name: "Montserrat-SemiBold", size: 18.0)!]
            
            let statusString = meetingStatus == "1" ? "Start" : meetingStatus == "2" ? "View" : ""
            
            let attributedText = NSAttributedString(string: statusString, attributes: multipleAttributes)
            btn.setAttributedTitle(attributedText, for: .normal)
            btn.setTitleColor(#colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), for: .normal)
            btn.addTarget(self, action: #selector(btnOnTapStart), for: .touchUpInside)
            
            if statusString == ""{
                self.navigationItem.rightBarButtonItem = nil
                self.navigationItem.rightBarButtonItem?.customView?.isHidden = true
                self.btnChat.isHidden = true
            }else{
                self.navigationItem.rightBarButtonItem?.customView?.isHidden = false
                self.navigationItem.rightBarButtonItem?.customView = btn
                self.btnChat.isHidden = false
            }
//
//        }else{
//            self.navigationItem.rightBarButtonItem = nil
//            self.btnChat.isHidden = true
//
//        }
    }
    
    @objc func getMeetingDetails(){
        let resPonsedata = self.getUserDefaults(key: KeyResponseModel) as! Data
        let responseModel = try! self.jsonDecoder.decode(LoginResponse.self, from: resPonsedata)
        let completeUrl =  Urls.Get_Meeting_Detail.rawValue + (self.meetingID) + "?api_token=" + API_Token + "&user_token=" + responseModel.user!.user_token!
        self.getDataFromNet(url: completeUrl, selector:#selector(MeetingDetailVC.handleSumitResponse(data:)) )
    }
    
    var options:Options?
    @IBAction func btnOnTapUpload(_ sender: UIButton) {
        options = Bundle.main.loadNibNamed("Options", owner: self, options: nil)![0] as? Options
        options?.frame = self.view.bounds
        self.view.addSubview(options!)
        return
        
        let actionSheetController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        let firstAction: UIAlertAction = UIAlertAction(title: "Image", style: .default) { action -> Void in
            print("First Action pressed")
            DispatchQueue.main.async {
                self.btnOnTapImages(nil)
            }
        }
        
        let secondAction: UIAlertAction = UIAlertAction(title: "Documents", style: .default) { action -> Void in
            print("Second Action pressed")
            DispatchQueue.main.async {
                self.btnOnTapDocuments(nil)
            }
        }
        let cancelAction: UIAlertAction = UIAlertAction(title: "Cancel", style: .cancel) { action -> Void in }
        
        // add actions
        actionSheetController.addAction(firstAction)
        actionSheetController.addAction(secondAction)
        actionSheetController.addAction(cancelAction)
        self.present(actionSheetController, animated: true, completion: nil)

    }
    @IBAction func btnOnTapLocation(_ sender: UIButton) {
        if meetingDeatail?.meeting_detail_data?.meeting_details?.address != nil{
            let latt = meetingDeatail!.meeting_detail_data!.meeting_details!.latitude!
            let lon = meetingDeatail!.meeting_detail_data!.meeting_details!.longitude!
            self.performSegue(withIdentifier: "SegueAppleMap", sender: (latt, lon))
        }
        
    }
    
    
    
    @IBAction func btnOnTapImages(_ sender: UIButton?) {
        options?.removeFromSuperview()
        options = nil
        let controler = JSImagePickerViewController()
        controler.delegate = self
        controler.showImagePicker(in: self, animated: true)
    }
    
    func checkPermissions(){
        switch CLLocationManager.authorizationStatus() {
        case .notDetermined:
            // Request when-in-use authorization initially
            self.showSnackBarWithMessage(message: "Please Turn on the Locations", color: #colorLiteral(red: 0.8891240954, green: 0.2305322289, blue: 0.1835787296, alpha: 1), txtColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), interval: .long, minusHeight: 0)
            return
            
        case .restricted, .denied:
            self.showSnackBarWithMessage(message: "Please Turn on the Locations", color: #colorLiteral(red: 0.8891240954, green: 0.2305322289, blue: 0.1835787296, alpha: 1), txtColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), interval: .long, minusHeight: 0)
            return
            // txtUsername.delegate =  self
            // txtPassword.delegate =  self
        // Do any additional setup after loading the view, typically from a nib.
        case .authorizedAlways:
            locationManager.delegate = self
            locationManager.startUpdatingLocation()
            break
        case .authorizedWhenInUse:
            locationManager.delegate = self
            locationManager.startUpdatingLocation()
            break
        }
    }
    
    var hasCalledOnce:Bool = false
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        print("Did location updates is called")
        let locValue : CLLocationCoordinate2D = locationManager.location!.coordinate;
        let long = locValue.longitude;
        let lat = locValue.latitude;
        locationManager.stopUpdatingLocation();
        if !hasCalledOnce{
            startMeetingApi(longitude: "\(long)", lattitude: "\(lat)")
            hasCalledOnce = true
        }
        //store the user location here to firebase or somewhere
    }
    
    @objc func handStartMeetingResponse(data:Data?){
        if let dat = data{
            do{
               let meetingDeatail = try jsonDecoder.decode(StartMeetingResponse.self, from: dat)
                hasCalledOnce = false
                if meetingDeatail.res_msg != nil && meetingDeatail.res_code! != 200{
                    self.showSnackBarWithMessage(message: responseModel!.res_msg!, color: #colorLiteral(red: 0.9254902005, green: 0.2352941185, blue: 0.101907857, alpha: 1), interval: .long)
                    return
                }else if meetingDeatail.data == true{
                    hasCalledOnce = false
                   self.performSegue(withIdentifier: "SegugeCheckList", sender: nil)
                }
            }catch let err{
                print(err)
            }
        }
    }
    
    func startMeetingApi(longitude:String!, lattitude:String!){
        let resPonsedata = self.getUserDefaults(key: KeyResponseModel) as! Data
        let responseModel = try! self.jsonDecoder.decode(LoginResponse.self, from: resPonsedata)
        let completeUrl =  Urls.Post_Checklist_Answer.rawValue + (self.meetingID) + "/start" //"?api_token=" + API_Token + "&user_token=" + responseModel.user!.user_token!
        //
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "HH:mm:ss"
        formatter.timeZone = TimeZone(abbreviation: "UTC")
        let time = formatter.string(from: date)
        let request = StartMeetingRequest(api_token: API_Token, user_token: responseModel.user!.user_token!, start_time: time, latitude: lattitude!, longitude: longitude!)
        self.postDataFromNet(url: completeUrl, requestData: request, selector: #selector(MeetingDetailVC.handStartMeetingResponse(data:)) )
    }
    
    //SegugeCheckList
    
   @objc  func btnOnTapStart() {
    
  
        let status = self.meetingDeatail?.meeting_detail_data?.meeting_details?.status
        
        if self.meetingID != "" && status == "1"{
            let messageText = "This is a future meeting, Do you really want to start now?"
            let alert = UIAlertController(title: "Alert", message: messageText, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "No", style: .cancel, handler: { (action) in
                
            }))
            alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { (ac) in
                  self.checkPermissions()
            }))
            self.present(alert, animated: true)
            
        }else{
            self.hasCalledOnce = false
            self.performSegue(withIdentifier: "SegugeCheckList", sender: nil)
        }
   
  }
    
    @IBAction func btnOnTapDocuments(_ sender: UIButton?) {
        options?.removeFromSuperview()
        options = nil
      
        let types: NSArray = NSArray(object: kUTTypePDF as NSString)

        let documentPicker = UIDocumentPickerViewController(documentTypes: types as! [String], in: .import)
        documentPicker.delegate = self
        documentPicker.modalPresentationStyle = .formSheet
        self.present(documentPicker, animated: true, completion: nil)
    }
    

    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if let ident  = segue.identifier{
            if ident == "SegugeCheckList"{
                let dest = segue.destination as! CheckListVC
                dest.meetingID = self.meetingID
            }
            if ident == "SegueAppleMap"{
                let dest = segue.destination as! AppleMapVC
                dest.cordinate = sender! as! tuple
            }
            if ident == "SegueChatUsers"{
                let dest = segue.destination as! ChatUserListVC
                dest.meetingID = self.meetingID
                dest.meetingName = meetingDeatail?.meeting_detail_data?.meeting_details?.meetingname != nil ? meetingDeatail!.meeting_detail_data!.meeting_details!.meetingname! : "N/A"
            }
        }
    }
    
    //MARK:- Preview document via quick look
    @IBAction func previewDocuments(_ sender: UIButton) {
        let hitpoint = sender.convert(sender.bounds.origin, to: tbleView)
        let hitindex = tbleView.indexPathForRow(at: hitpoint)
        self.previewItem.removeAll()
        let docData = meetingDeatail!.meeting_detail_data!.documents!
        for tempData in docData {
            // Download file
            self.downloadfile(filePath: tempData.path!,fileUniqueName:tempData.uniquename!, completion: {(success, fileLocationURL) in
                if success {
                    // Set the preview item to display======
                    self.previewItem.append(fileLocationURL! as NSURL)
                    if docData.count == self.previewItem.count{
                    // Display file
//                    self.previewController.navigationController?.navigationBar.tintColor = #colorLiteral(red: 0.003921568627, green: 0.2156862745, blue: 0.5215686275, alpha: 1)
//                    self.previewController.navigationController?.navigationBar.barTintColor = #colorLiteral(red: 0.003921568627, green: 0.2156862745, blue: 0.5215686275, alpha: 1)
                    self.previewController.dataSource = self
                    self.previewController.currentPreviewItemIndex = hitindex?.row ?? 0
                    self.present(self.previewController, animated: true, completion: nil)
                    }
                    
                }else{
                    debugPrint("File can't be downloaded")
                }
            })
        }
    
    }
    
    func downloadfile(filePath: String, fileUniqueName: String,completion: @escaping (_ success: Bool,_ fileLocation: URL?) -> Void){
        
        let itemUrl = URL(string: filePath + fileUniqueName)
        
        // then lets create your document folder url
        let documentsDirectoryURL =  FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
        
        // lets create your destination file url
        let destinationUrl = documentsDirectoryURL.appendingPathComponent( fileUniqueName)
        
        // to check if it exists before downloading it
        if FileManager.default.fileExists(atPath: destinationUrl.path) {
            debugPrint("The file already exists at path")
            completion(true, destinationUrl)
            
            // if the file doesn't exist
        } else {
            
            // you can use NSURLSession.sharedSession to download the data asynchronously
            URLSession.shared.downloadTask(with: itemUrl!, completionHandler: { (location, response, error) -> Void in
                guard let tempLocation = location, error == nil else { return }
                do {
                    // after downloading your file you need to move it to your destination url
                    try FileManager.default.moveItem(at: tempLocation, to: destinationUrl)
                    print("File moved to documents folder")
                    completion(true, destinationUrl)
                } catch let error as NSError {
                    print(error.localizedDescription)
                    completion(false, nil)
                }
            }).resume()
        }
    }
    
    func numberOfPreviewItems(in controller: QLPreviewController) -> Int {
        return previewItem.count
    }
    func previewController(_ controller: QLPreviewController, previewItemAt index: Int) -> QLPreviewItem {
        return previewItem[index] as QLPreviewItem
    }
  
    
    //MARK:- UITAbleview Delegate and datasource
    func numberOfSections(in tableView: UITableView) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return meetingDeatail?.meeting_detail_data?.meeting_details != nil ? 1  : 0
        case 1:
            return meetingDeatail?.meeting_detail_data?.representatives != nil ? meetingDeatail!.meeting_detail_data!.representatives!.count  : 0
        case 2:
            return  meetingDeatail != nil ? 1 : 0
        case 3:
            return meetingDeatail?.meeting_detail_data?.documents != nil ?  meetingDeatail!.meeting_detail_data!.documents!.count  : 0
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell:UITableViewCell! = nil
        switch indexPath.section {
        case 0:
            cell = tableView.dequeueReusableCell(withIdentifier: "MeetingDetailCell", for: indexPath) as! MeetingDetailCell
            (cell as! MeetingDetailCell).lblMeetingName.text = meetingDeatail?.meeting_detail_data?.meeting_details?.meetingname != nil ? meetingDeatail!.meeting_detail_data!.meeting_details!.meetingname! : "N/A"
            (cell as! MeetingDetailCell).lblMeetingLocation.text = meetingDeatail?.meeting_detail_data?.meeting_details?.address != nil ? meetingDeatail!.meeting_detail_data!.meeting_details!.address! : "N/A"
            
            let meetingData = meetingDeatail?.meeting_detail_data?.meeting_details
            (cell as! MeetingDetailCell).lblMeetingDate.text = UTCToLocal(date: (meetingData?.date)!, fromFormat: "yyyy-MM-dd", toFormat: "dd MMM, yyyy") 
            
            let startTime = UTCToLocal(date: (meetingData?.start_time ?? ""), fromFormat: "HH:mm:ss", toFormat: "HH:mm")
            let endTime = UTCToLocal(date: (meetingData?.end_time ?? ""), fromFormat: "HH:mm:ss", toFormat: "HH:mm")
            
            (cell as! MeetingDetailCell).lblMeetingTime.text = (startTime != "" ? startTime : (meetingData?.start_time ?? ""))! +  "  -  " + (endTime != "" ? endTime : (meetingData?.end_time ?? ""))!
            
            
            (cell as! MeetingDetailCell).lblMeetingDescription.text = meetingDeatail?.meeting_detail_data?.meeting_details?.description != nil ? meetingDeatail!.meeting_detail_data!.meeting_details!.description! : ""

        case 1:
            cell = tableView.dequeueReusableCell(withIdentifier: "MeetingRepsCell", for: indexPath) as! MeetingRepsCell
            (cell as! MeetingRepsCell).lblMeetingRepsName.text = (meetingDeatail?.meeting_detail_data?.representatives?[indexPath.row].firstname != nil ? meetingDeatail!.meeting_detail_data!.representatives![indexPath.row].firstname! : "N/A") + "  " + (meetingDeatail?.meeting_detail_data?.representatives?[indexPath.row].lastname != nil ? meetingDeatail!.meeting_detail_data!.representatives![indexPath.row].lastname! : "N/A")
            if let urlSTR = meetingDeatail?.meeting_detail_data?.representatives?[indexPath.row].image{
                (cell as! MeetingRepsCell).ivMeetingReps.sd_setImage(with: URL(string: urlSTR), completed: nil)
            }
        case 2:
            cell = tableView.dequeueReusableCell(withIdentifier: "MeetingAnalysisCell", for: indexPath) as! MeetingAnalysisCell
            (cell as! MeetingAnalysisCell).lblTotalChecklist.text = meetingDeatail?.meeting_detail_data?.no_of_checklists != nil ? "\(meetingDeatail!.meeting_detail_data!.no_of_checklists!)":"0"
            (cell as! MeetingAnalysisCell).lblTotalQuestions.text = meetingDeatail?.meeting_detail_data?.no_of_questions != nil ? "\(meetingDeatail!.meeting_detail_data!.no_of_questions!)":"0"
            (cell as! MeetingAnalysisCell).btnPlus.isHidden  = self.selectedIndex == 1
            if meetingDeatail!.meeting_detail_data!.documents != nil  {
                (cell as! MeetingAnalysisCell).bgView.isHidden =  false
               // (cell as! MeetingAnalysisCell).bgView.layoutSubviews()
            }else{
                (cell as! MeetingAnalysisCell).bgView.isHidden = true
            }
        case 3:
            cell = tableView.dequeueReusableCell(withIdentifier: "MeetingDocumentCell", for: indexPath) as! MeetingDocumentCell
            (cell as! MeetingDocumentCell).lblTypeName.text = " ." + meetingDeatail!.meeting_detail_data!.documents![indexPath.row].type!
            (cell as! MeetingDocumentCell).lblDocName.text = meetingDeatail!.meeting_detail_data!.documents![indexPath.row].uniquename!
            if indexPath.row  ==  meetingDeatail!.meeting_detail_data!.documents!.count - 1{
                (cell as! MeetingDocumentCell).vwBg.backgroundColor = UIColor.clear
            }
            switch meetingDeatail!.meeting_detail_data!.documents![indexPath.row].type!.lowercased(){
            case "png", "jpg","jpeg":
                (cell as! MeetingDocumentCell).ivDoc.image = #imageLiteral(resourceName: "doc_image")
                break
            default:
                (cell as! MeetingDocumentCell).ivDoc.image = #imageLiteral(resourceName: "doc_pdf")
                break
            }
            
        default:
            return cell
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0
    }

    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0
    }

}


extension MeetingDetailVC:JSImagePickerViewControllerDelegate, UITextFieldDelegate{
    
   @objc func addDocumentNameAlert(data: Data, type: String) {
        let alertController = UIAlertController(title: "First In Field", message: "Please enter name for the selected document.", preferredStyle: .alert)
        alertController.addTextField { (textField : UITextField!) -> Void in
            textField.placeholder = "Document description"
            textField.delegate = self
        }
        saveAction = UIAlertAction(title: "Save", style: .default, handler: { alert -> Void in
            let firstTextField = alertController.textFields![0] as UITextField
            
            if type == "pdf"{
                DispatchQueue.main.async {
                    
                        let resPonsedata = self.getUserDefaults(key: KeyResponseModel) as! Data
                        let responseModel = try! self.jsonDecoder.decode(LoginResponse.self, from: resPonsedata)
                    let userDict = ["api_token": API_Token, "user_token": responseModel.user!.user_token!, "meeting_id":self.meetingID, "document_name": firstTextField.text!]
                        self.uploadImageWithParams(data, t: userDict as NSDictionary, url: Urls.Upload_Doc.rawValue, selector: #selector(MeetingDetailVC.imageUploadedSuccessfully(data:)), type: "pdf", mimetype: "image/pdf")
                   
                }
            }else{
           
            let resPonsedata = self.getUserDefaults(key: KeyResponseModel) as! Data
            let responseModel = try! self.jsonDecoder.decode(LoginResponse.self, from: resPonsedata)
                let userDict = ["api_token": API_Token, "user_token": responseModel.user!.user_token!, "meeting_id":self.meetingID, "document_name": firstTextField.text!]
            
                self.uploadImageWithParams(data, t: userDict as NSDictionary, url: Urls.Upload_Doc.rawValue, selector:#selector(MeetingDetailVC.imageUploadedSuccessfully(data:)))
            }
            
        })
        let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: { (action : UIAlertAction!) -> Void in })
    
        saveAction.isEnabled = false
    
        alertController.addAction(cancelAction)
        alertController.addAction(saveAction)
    
        UIApplication.topViewController()?.present(alertController, animated: true, completion: nil)
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        saveAction.isEnabled = ((textField.text?.count)! > 0)
        return true
    }
    
    @objc func imageUploadedSuccessfully(data:Data?){
        if let dat = data{
            do{
               let uplodDoc = try jsonDecoder.decode(UploadDocResponse.self, from: dat)
                if uplodDoc.res_msg != nil && uplodDoc.res_code! != 200{
                    self.showSnackBarWithMessage(message: responseModel!.res_msg!, color: #colorLiteral(red: 0.9254902005, green: 0.2352941185, blue: 0.101907857, alpha: 1), interval: .long)
                    return
                }
                if uplodDoc.documents != nil{
                if meetingDeatail!.meeting_detail_data!.documents != nil && meetingDeatail!.meeting_detail_data!.documents!.count > 0 {
                    self.meetingDeatail!.meeting_detail_data!.documents!.append(uplodDoc.documents!)
                }else{
                  self.meetingDeatail!.meeting_detail_data!.documents = [uplodDoc.documents!]
                }
                self.tbleView.reloadData()
                self.tbleView.scrollToNearestSelectedRow(at: .bottom, animated: true)
                }else{
                    self.showSnackBarWithMessage(message: responseModel!.res_msg != nil ? responseModel!.res_msg! : "Something went wrong.", color: #colorLiteral(red: 0.9254902005, green: 0.2352941185, blue: 0.101907857, alpha: 1), interval: .long)
                }
            }catch let err{
                print(err)
            }
        }
    }
    
    func imagePicker(_ imagePicker: JSImagePickerViewController!, didSelectImages images: [Any]!) {
         print(images.count)
        let imageData = UIImageJPEGRepresentation(images[0] as! UIImage, 0.5)
        if #available(iOS 10.0, *) {
            Timer.scheduledTimer(withTimeInterval: 1, repeats: false) {[weak self] (timer) in
                (self?.addDocumentNameAlert(data: imageData!, type: "image"))
            }
        } else {
            // Fallback on earlier versions
        }
        
    }
    
}


extension MeetingDetailVC:UIDocumentPickerDelegate, UIDocumentMenuDelegate, UINavigationControllerDelegate {
    func documentMenu(_ documentMenu: UIDocumentMenuViewController, didPickDocumentPicker documentPicker: UIDocumentPickerViewController) {
        self.present(documentPicker, animated: true) {
            
        }
    }
    
    func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentsAt urls: [URL]) {
        // you get from the urls parameter the urls from the files selected
        do{
            
        self.showIndicator()
        let data  = try Data(contentsOf: urls[0])
        self.hideIndicator()
            if #available(iOS 10.0, *) {
                Timer.scheduledTimer(withTimeInterval: 1, repeats: false) {[weak self] (timer) in
                    self?.addDocumentNameAlert(data: data, type: "pdf")
                }
            } else {
                // Fallback on earlier versions
            }
        
             }catch{}
        
    }
    
}


class MeetingDetailCell: UITableViewCell {
    @IBOutlet weak var lblMeetingName: UILabel!
    @IBOutlet weak var lblMeetingTime: UILabel!
    @IBOutlet weak var lblMeetingLocation: UILabel!
    @IBOutlet weak var lblMeetingDescription: UILabel!
    
    @IBOutlet weak var lblMeetingDate: UILabel!
}

class MeetingRepsCell: UITableViewCell {
    @IBOutlet weak var ivMeetingReps: UIImageView!
    @IBOutlet weak var lblMeetingRepsName: UILabel!
    
}

class MeetingAnalysisCell: UITableViewCell {
    @IBOutlet weak var lblTotalChecklist: UILabel!
    @IBOutlet weak var lblTotalQuestions: UILabel!
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var btnPlus: UIButton!
    
}

class MeetingDocumentCell: UITableViewCell {
    @IBOutlet weak var ivDoc: UIImageView!
    @IBOutlet weak var lblTypeName: UILabel!
    @IBOutlet weak var lblDocName: UILabel!
    @IBOutlet weak var vwBg: UIView!
    
}


class RoundView: UIView {
    override func layoutSubviews() {
        super.layoutSubviews()
        
        self.roundCorners([.topLeft, .topRight], radius: 10)
    }
}


class BottomRoundView: UIView {
    override func layoutSubviews() {
        super.layoutSubviews()
        
        self.roundCorners([.bottomLeft, .bottomRight], radius: 10)
    }
}
