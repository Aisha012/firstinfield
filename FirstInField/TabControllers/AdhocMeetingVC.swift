//
//  AdhocMeetingVC.swift
//  FirstInField
//
//  Created by Namespace  on 15/06/18.
//  Copyright © 2018 Namespace . All rights reserved.
//

import UIKit

class AdhocMeetingVC: UIViewController, UITableViewDataSource,UITableViewDelegate {
  
    var pageIndex = 1
    var isRefreshing:Bool = false
    var meetingStatus = ""
    var canCreateAdhoc = true
    
    @IBOutlet weak var tbleView: UITableView!
    @IBOutlet weak var centerLbl: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "Ad Hoc Meetings"
        let btn = UIButton(frame: CGRect.init(x: 0, y: 0, width: 50, height: 25))
        btn.clipsToBounds = true
        btn.setImage(#imageLiteral(resourceName: "addMeetingIcon"), for: .normal)
        btn.contentHorizontalAlignment = UIControlContentHorizontalAlignment.right
        btn.addTarget(self, action: #selector(btnOnTapStart), for: .touchUpInside)
        self.navigationItem.rightBarButtonItem?.customView = btn
        self.navigationController?.navigationBar.tintColor = UIColor.white
        
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "bell"), style: .plain, target: self, action: #selector(showNotificationListing(btn:)))
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.refreshMeetings), name: Notification.Name("refreshMeeting"), object: nil)
        
        // Do any additional setup after loading the view.

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        isRefreshing = true
         checkAdhocMeetingEnabled()
    }
    
    
    @objc func btnOnTapStart(){
        self.performSegue(withIdentifier: "SegueCreateMeeting", sender: "adhocChecklist")
        
    }
    
    
    
    override func refresh(_ sender: UIRefreshControl) {
            self.pageIndex = 1
            isRefreshing = true
            checkAdhocMeetingEnabled()
            sender.endRefreshing()
    }
    
    var meetingResponse:MeetingResponse?
    
    func checkAdhocMeetingEnabled(){
        let resPonsedata = getUserDefaults(key: KeyResponseModel) as! Data
        let responseModel = try! jsonDecoder.decode(LoginResponse.self, from: resPonsedata)
        
        self.getDataFromNet(url: Urls.Check_Adhoc_Meeting.rawValue  + "\(responseModel.user!.id!)?" + "api_token=" + API_Token + "&user_token=" + responseModel.user!.user_token!, selector: #selector(AdhocMeetingVC.handleCheckResponse(data:)))
    }
    
    @objc func handleCheckResponse(data:Data?) {
        if let dat = data{
            do{
                let meetingRespons = try jsonDecoder.decode(CheckAdhocMeetingResponse.self, from: dat)
                if meetingRespons.res_msg != nil && meetingRespons.res_code! != 200{
                   // self.showSnackBarWithMessage(message: responseModel!.res_msg!, color: #colorLiteral(red: 0.9254902005, green: 0.2352941185, blue: 0.1019607857, alpha: 1), interval: .long, minusHeight: 0)
                    return
                }else if meetingRespons.meeting_data != nil{
                     getAdhocMeetings()
                    canCreateAdhoc = meetingRespons.meeting_data!
                    if meetingRespons.meeting_data!{
                        self.navigationItem.rightBarButtonItem?.isEnabled = true
                        self.navigationItem.rightBarButtonItem?.customView?.isHidden = false
                    }else{
                         self.centerLbl.text = "You can not create ad hoc meetings now, contact your manager for more information."
                        setUserDefaults(key: Adhoc_Count, value: 0)
                        NotificationCenter.default.post(name: Notification.Name("resetCounter"), object: nil)
                        self.navigationItem.rightBarButtonItem?.isEnabled = false
                        self.navigationItem.rightBarButtonItem?.customView?.isHidden = true
                    }
                    
                }
            }catch let err {
                print(err)
            }
        }
    }
    
    func getAdhocMeetings(){
        let resPonsedata = getUserDefaults(key: KeyResponseModel) as! Data
        let responseModel = try! jsonDecoder.decode(LoginResponse.self, from: resPonsedata)
        self.getDataFromNet(url: Urls.Get_Adhoc_Meeting.rawValue  + "\(responseModel.user!.id!)?" + "api_token=" + API_Token + "&user_token=" + responseModel.user!.user_token!  + "&page=" + "\(pageIndex)", selector: #selector(AdhocMeetingVC.handleNetResponse(data:)))
    }
    
    @objc func handleNetResponse(data:Data?) {
        if let dat = data{
            do{
                let meetingRespons = try jsonDecoder.decode(MeetingResponse.self, from: dat)
                if meetingRespons.res_msg != nil && meetingRespons.res_code! != 200{
//                    self.showSnackBarWithMessage(message: responseModel!.res_msg!, color: #colorLiteral(red: 0.9254902005, green: 0.2352941185, blue: 0.1019607857, alpha: 1), interval: .long, minusHeight: 0)
                    return
                }
                setUserDefaults(key: Adhoc_Count, value: meetingRespons.meeting_data?.adhoc_count ?? 0)
                 NotificationCenter.default.post(name: Notification.Name("resetCounter"), object: nil)
                
                if meetingRespons.meeting_data?.user_meetings != nil && !canCreateAdhoc{
                    self.showSnackBarWithMessage(message: "You can not create ad-hoc meetings now, contact your Manager for more information.", color: #colorLiteral(red: 0.9254902005, green: 0.2352941185, blue: 0.1019607857, alpha: 1), interval: .long, minusHeight: 0)
                }
                
                if isRefreshing{
                    self.meetingResponse = nil
                    self.meetingResponse = meetingRespons
                    
                    self.centerLbl.isHidden = (self.meetingResponse?.meeting_data?.user_meetings != nil ? self.meetingResponse!.meeting_data!.user_meetings!.count>0 : false)
                    let msgStr = canCreateAdhoc ? "No Records Found." : "You can not create ad-hoc meetings now, contact your Manager for more information."
                    self.centerLbl.text = !(centerLbl.isHidden) ? msgStr : ""
                    
                    self.tbleView.reloadData()
                    isRefreshing = false
                    return
                }
                if self.meetingResponse == nil{
                    self.meetingResponse = meetingRespons
                }else if meetingRespons.meeting_data?.user_meetings != nil{
                    let userm = meetingRespons.meeting_data!.user_meetings!
                    self.meetingResponse?.meeting_data!.user_meetings!.append(contentsOf: userm)
                    self.meetingResponse?.meeting_data!.pagination = meetingRespons.meeting_data!.pagination
                }
                
                
                let msgStr = canCreateAdhoc ? "No Records Found." : "You can not create ad-hoc meetings now, contact your Manager for more information."
                self.centerLbl.text = !(centerLbl.isHidden) ? msgStr : ""
                self.tbleView.reloadData()
                if meetingRespons.meeting_data!.pagination?.currentPage! == 1{
                    DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.1) {
                        let indexpath = IndexPath(row: 0, section: 0)
                        self.tbleView.scrollToRow(at: indexpath, at: .top, animated: true)
                        
                    }
                }
            }catch let err {
                let msgStr = canCreateAdhoc ? "No Records Found." : "You can not create ad-hoc meetings now, contact your Manager for more information."
                self.centerLbl.text = !(centerLbl.isHidden) ? msgStr : ""

                print(err)
            }
        }
    }
    
    @objc func refreshMeetings(){
            self.pageIndex = 1
            isRefreshing = true
            self.getAdhocMeetings()
    
    }

    
    // MARK: - Tableview Datasource Methods
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return  self.meetingResponse?.meeting_data?.user_meetings != nil ? self.meetingResponse!.meeting_data!.user_meetings!.count : 0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (self.meetingResponse?.meeting_data?.user_meetings?[section].meetings != nil ? self.meetingResponse!.meeting_data!.user_meetings![section].meetings!.count : 0) 
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if (indexPath.section == self.meetingResponse!.meeting_data!.user_meetings!.count-2) && (self.meetingResponse!.meeting_data!.pagination!.currentPage! < self.meetingResponse!.meeting_data!.pagination!.lastPage!){
            self.pageIndex = self.meetingResponse!.meeting_data!.pagination!.currentPage! + 1
            self.getAdhocMeetings()
        }
        
            // if indexPath.row % 2 == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! MeetingTableCell
            cell.lblAddress.text = self.meetingResponse!.meeting_data!.user_meetings![indexPath.section].meetings![indexPath.row].scheduled_address != nil ? self.meetingResponse!.meeting_data!.user_meetings![indexPath.section].meetings![indexPath.row].scheduled_address!:"N/A"
            
//            cell.lblMeetingStatus.text = self.meetingResponse!.meeting_data!.user_meetings![indexPath.section].meetings![indexPath.row].status! == "1" ? "Upcoming":"Running"
//            cell.lblMeetingStatus.textColor = self.meetingResponse!.meeting_data!.user_meetings![indexPath.section].meetings![indexPath.row].status! == "1" ? #colorLiteral(red: 0, green: 0.5294117647, blue: 0, alpha: 1):#colorLiteral(red: 0.8891240954, green: 0.2305322289, blue: 0.1835787296, alpha: 1)
            cell.lblMeetingDesc.text = self.meetingResponse!.meeting_data!.user_meetings![indexPath.section].meetings![indexPath.row].meetingname != nil ? self.meetingResponse!.meeting_data!.user_meetings![indexPath.section].meetings![indexPath.row].meetingname!:"N/A"
            cell.btnMap.indexPath = indexPath as NSIndexPath
//            cell.btnStartMeeting.indexPath = indexPath as NSIndexPath
        
            let meetingData = self.meetingResponse!.meeting_data!.user_meetings![indexPath.section].meetings![indexPath.row]
            let startTime = UTCToLocal(date: meetingData.actual_start_time!, fromFormat: "HH:mm:ss", toFormat: "HH:mm")
            let endTime = UTCToLocal(date: meetingData.actual_end_time!, fromFormat: "HH:mm:ss", toFormat: "HH:mm")
        
            cell.lblTime.text = (startTime != "" ? startTime : meetingData.actual_start_time!) +  "  -  " + (endTime != "" ? endTime : meetingData.actual_end_time)!
        
            return cell
    }
    
    
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
            let cell = tableView.dequeueReusableCell(withIdentifier: "headerCell") as! MeetingHeaderCell
            cell.lblDate.text = self.meetingResponse!.meeting_data!.user_meetings?[section].date != nil ? localToUTC(date: self.meetingResponse!.meeting_data!.user_meetings![section].date!, fromFormat: "yyyy-MM-dd", toFormat: "dd MMMM, EE") : ""
            return cell
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 30
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let idd = self.meetingResponse!.meeting_data!.user_meetings?[indexPath.section].meetings?[indexPath.row].id != nil ? "\(String(describing: self.meetingResponse!.meeting_data!.user_meetings![indexPath.section].meetings![indexPath.row].id!))" : "28"
        meetingStatus = self.meetingResponse!.meeting_data!.user_meetings?[indexPath.section].meetings?[indexPath.row].status != nil ? "\(String(describing: self.meetingResponse!.meeting_data!.user_meetings![indexPath.section].meetings![indexPath.row].status!))" : "3"
        self.performSegue(withIdentifier: "SegueCheckList", sender:idd)
    }

    @objc func refreshAdhocMeetings() {
        self.pageIndex = 1
        self.isRefreshing = true
        checkAdhocMeetingEnabled()
    }
    
    
    @IBAction func showActionSheet(sender: MeetingButton) {
        if self.meetingResponse?.meeting_data?.user_meetings?[sender.indexPath.section].meetings?[sender.indexPath.row].scheduled_address == nil ||  self.meetingResponse?.meeting_data?.user_meetings?[sender.indexPath.section].meetings?[sender.indexPath.row].scheduled_address  == ""{
            return
        }
        
        if (self.meetingResponse?.meeting_data?.user_meetings?[sender.indexPath.section].meetings?[sender.indexPath.row].actual_longitude != nil && self.meetingResponse?.meeting_data?.user_meetings?[sender.indexPath.section].meetings?[sender.indexPath.row].actual_latitude != nil){
            let latt = self.meetingResponse!.meeting_data!.user_meetings![sender.indexPath.section].meetings![sender.indexPath.row].actual_latitude!
            let lon = self.meetingResponse!.meeting_data!.user_meetings![sender.indexPath.section].meetings![sender.indexPath.row].actual_longitude!
            
            self.performSegue(withIdentifier: "SegueAppleMap", sender: (latt, lon))
            return
        }
        
    }
    
    @IBAction func showNotificationListing(btn:Any){
        self.performSegue(withIdentifier: "SegueNotilist", sender:nil)
    }
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        // dest.cordinate = sender as! tuple
        if let seg = segue.identifier{
            switch (seg){
            case "SegueCheckList":
                let dest = segue.destination as! MeetingDetailVC
                dest.meetingID = sender! as! String
                dest.titleStr = "Adhoc Meeting"
                dest.fromVC = "adhoc"
                dest.meetingStatus = self.meetingStatus
                break
                
            case "SegueAppleMap":
                let dest = segue.destination as! AppleMapVC
                if let sen = sender{
                    dest.cordinate = sender as! tuple
                }
                break
                
            case "SegueCreateMeeting":
                let dest = segue.destination as! CreateAddhocMeeting
                
//                if let sen = sender{
//                    dest.cordinate = sender as! tuple
//                }
                break
                
                
            default:
                break
            }
        }
    }
    
}




