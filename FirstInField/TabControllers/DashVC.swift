//
//  DashVC.swift
//  FirstInField
//
//  Created by Namespace  on 15/06/18.
//  Copyright © 2018 Namespace . All rights reserved.
//
import CoreLocation
import UIKit


class DashVC: UIViewController, DeviceTokenApi, UITableViewDataSource,UITableViewDelegate {
    lazy var locationManager = CLLocationManager()
    let del = UIApplication.shared.delegate as! AppDelegate
    var meetingStatus = ""
    @IBOutlet weak var tbleView: UITableView!
    @IBOutlet weak var centerLbl: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "Active Meetings"//del.compString != nil ? del.compString! : "dash"
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "bell"), style: .plain, target: self, action: #selector(showNotificationListing(btn:)))
        
        self.navigationController?.navigationBar.tintColor = UIColor.white
        (self.navigationController?.tabBarController?.parent as! ContainerVC).checkPermissions()
        del.tokenApi = self
        

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        isRefreshing = true
        getupcomingMeetings()
    }
    
    
    
    func callDeviceTokenApi() {
            let resPonsedata = self.getUserDefaults(key: KeyResponseModel) as! Data
            let responseModel = try! self.jsonDecoder.decode(LoginResponse.self, from: resPonsedata)
        if responseModel.user!.id != nil && responseModel.user!.id != 0 {
        makeCallDeviceTokenApi(userID: "\(responseModel.user!.id!)")
        }
    }
    
    func makeCallDeviceTokenApi(userID:String){
        let token = UserDefaults.standard.value(forKey: KEY_DEVICE_TOKEN)
        let deviceID  =  UIDevice.current.identifierForVendor!.uuidString
        let deviceName = UIDevice.current.name
        let device_token_request  = DeviceTokeRequest(user_id: userID, notification_status: "1"/*"true"*/, device_id:deviceID , device_token: token as? String, device_type: "iPhone", device_name: deviceName, country_code: nil)
        print("TOKEN >> \(String(describing: token))")
        self.postDataFromNet(url: Urls.Post_Device_Token.rawValue, requestData: device_token_request, selector: #selector(DashVC.handleNetDeviceResponse(data:)))
        
    }
   
    
    @objc func handleNetDeviceResponse(data:Data?) {
        if let dat = data{
                print(dat)
        }
    }

    
    @IBAction func showActionSheet(sender: MeetingButton) {
        
            if self.meetingResponse?.meeting_data?.user_meetings?[sender.indexPath.section].meetings?[sender.indexPath.row].scheduled_address == nil ||  self.meetingResponse?.meeting_data?.user_meetings?[sender.indexPath.section].meetings?[sender.indexPath.row].scheduled_address  == ""{
                return
            }
            
            if (self.meetingResponse?.meeting_data?.user_meetings?[sender.indexPath.section].meetings?[sender.indexPath.row].actual_longitude != nil && self.meetingResponse?.meeting_data?.user_meetings?[sender.indexPath.section].meetings?[sender.indexPath.row].actual_latitude != nil){
                let latt = self.meetingResponse!.meeting_data!.user_meetings![sender.indexPath.section].meetings![sender.indexPath.row].actual_latitude!
                let lon = self.meetingResponse!.meeting_data!.user_meetings![sender.indexPath.section].meetings![sender.indexPath.row].actual_longitude!
                
                self.performSegue(withIdentifier: "SegueAppleMap", sender: (latt, lon))
                return
            }
            
            
        }
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        // dest.cordinate = sender as! tuple
        if let seg = segue.identifier{
            switch (seg){
            case "SegueCheckList":
                let dest = segue.destination as! MeetingDetailVC
                dest.meetingID = sender! as! String
                dest.selectedIndex = 0
                dest.titleStr = "Active Meetings"
                dest.meetingStatus = self.meetingStatus
                break
                
            case "SegueAppleMap":
                let dest = segue.destination as! AppleMapVC
                if let sen = sender{
                    dest.cordinate = sender as! tuple
                }
                break
            default:
                break
            }
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    var isRefreshing:Bool = false
    override func refresh(_ sender: UIRefreshControl) {
        self.pageIndex = 1
        isRefreshing = true
        self.getupcomingMeetings()
        sender.endRefreshing()
    }
    
    var meetingResponse:MeetingResponse?
    
    @objc func handleNetResponse(data:Data?) {
        if let dat = data{
            do{
                let meetingRespons = try jsonDecoder.decode(MeetingResponse.self, from: dat)
                if meetingRespons.res_msg != nil && meetingRespons.res_code! != 200{
//                    self.showSnackBarWithMessage(message: responseModel!.res_msg!, color: #colorLiteral(red: 0.9254902005, green: 0.2352941185, blue: 0.1019607857, alpha: 1), interval: .long, minusHeight: 0)
                    return
                }
                if isRefreshing{
                    self.meetingResponse = nil
                    self.meetingResponse = meetingRespons
                    
                    self.centerLbl.isHidden = (self.meetingResponse?.meeting_data?.user_meetings != nil ? self.meetingResponse!.meeting_data!.user_meetings!.count>0 : false)
                    self.centerLbl.text = !(centerLbl.isHidden) ? "No Records Found." : ""
                    
                    self.tbleView.reloadData()
                    isRefreshing = false
                    return
                }
                if self.meetingResponse == nil{
                    self.meetingResponse = meetingRespons
                }else if meetingRespons.meeting_data?.user_meetings != nil{
                    let userm = meetingRespons.meeting_data!.user_meetings!
                    self.meetingResponse?.meeting_data!.user_meetings!.append(contentsOf: userm)
                    self.meetingResponse?.meeting_data!.pagination = meetingRespons.meeting_data!.pagination
                }
                
                self.centerLbl.isHidden = (self.meetingResponse?.meeting_data?.user_meetings != nil ? self.meetingResponse!.meeting_data!.user_meetings!.count>0 : false)
                self.centerLbl.text = !(centerLbl.isHidden) ? "No Records Found." : ""
                self.tbleView.reloadData()
                if meetingRespons.meeting_data!.pagination?.currentPage! == 1{
                    DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.1) {
                        let indexpath = IndexPath(row: 0, section: 0)
                        self.tbleView.scrollToRow(at: indexpath, at: .top, animated: true)
                        
                    }
                }
            }catch let err {
                self.centerLbl.text = !(centerLbl.isHidden) ? "No Records Found." : ""
                print(err)
            }
        }
    }
    
    
    
    
    
    var pageIndex = 1
    var pastPageIndex = 1
    func getupcomingMeetings(){
        let resPonsedata = getUserDefaults(key: KeyResponseModel) as! Data
        let responseModel = try! jsonDecoder.decode(LoginResponse.self, from: resPonsedata)
        self.getDataFromNet(url: Urls.Get_Current_Meeting.rawValue + "?user_id=" + "\(responseModel.user!.id!)" + "&api_token=" + API_Token + "&user_token=" + responseModel.user!.user_token!  + "&page=" + "\(pageIndex)", selector: #selector(DashVC.handleNetResponse(data:)))
        
    }
    
    
    // MARK: - Tableview Datasource Methods
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return  self.meetingResponse?.meeting_data?.user_meetings != nil ? self.meetingResponse!.meeting_data!.user_meetings!.count : 0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (self.meetingResponse?.meeting_data?.user_meetings?[section].meetings != nil ? self.meetingResponse!.meeting_data!.user_meetings![section].meetings!.count : 0)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if (indexPath.section == self.meetingResponse!.meeting_data!.user_meetings!.count - 2 ) && (self.meetingResponse!.meeting_data!.pagination!.currentPage! < self.meetingResponse!.meeting_data!.pagination!.lastPage!){
            self.pageIndex = self.meetingResponse!.meeting_data!.pagination!.currentPage! + 1
            self.getupcomingMeetings()
        }
        // if indexPath.row % 2 == 0{
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! MeetingTableCell
        cell.lblAddress.text = self.meetingResponse!.meeting_data!.user_meetings![indexPath.section].meetings![indexPath.row].scheduled_address != nil ? self.meetingResponse!.meeting_data!.user_meetings![indexPath.section].meetings![indexPath.row].scheduled_address!:"N/A"
        
        cell.lblMeetingDesc.text = self.meetingResponse!.meeting_data!.user_meetings![indexPath.section].meetings![indexPath.row].meetingname != nil ? self.meetingResponse!.meeting_data!.user_meetings![indexPath.section].meetings![indexPath.row].meetingname!:"N/A"
        cell.btnMap.indexPath = indexPath as NSIndexPath
        
        let meetingData = self.meetingResponse!.meeting_data!.user_meetings![indexPath.section].meetings![indexPath.row]
        let startTime = UTCToLocal(date: meetingData.actual_start_time!, fromFormat: "HH:mm:ss", toFormat: "HH:mm")
        let endTime = UTCToLocal(date: meetingData.actual_end_time!, fromFormat: "HH:mm:ss", toFormat: "HH:mm")
       
        cell.lblTime.text = (startTime != "" ? startTime : meetingData.actual_start_time!) +  "  -  " + (endTime != "" ? endTime : meetingData.actual_end_time)!
       
        return cell
    }
    
    
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cell = tableView.dequeueReusableCell(withIdentifier: "headerCell") as! MeetingHeaderCell
        cell.lblDate.text = self.meetingResponse!.meeting_data!.user_meetings?[section].date != nil ? localToUTC(date: self.meetingResponse!.meeting_data!.user_meetings![section].date!, fromFormat: "yyyy-MM-dd", toFormat: "dd MMMM, EE") : ""
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 30
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
                    let idd = self.meetingResponse!.meeting_data!.user_meetings?[indexPath.section].meetings?[indexPath.row].id != nil ? "\(String(describing: self.meetingResponse!.meeting_data!.user_meetings![indexPath.section].meetings![indexPath.row].id!))" : "28"
        meetingStatus = self.meetingResponse!.meeting_data!.user_meetings?[indexPath.section].meetings?[indexPath.row].status != nil ? "\(String(describing: self.meetingResponse!.meeting_data!.user_meetings![indexPath.section].meetings![indexPath.row].status!))" : "3"
        
                    self.performSegue(withIdentifier: "SegueCheckList", sender:idd)
    }

    @IBAction func showNotificationListing(btn:Any){
        self.performSegue(withIdentifier: "SegueNotilist", sender:nil)
    }
}
