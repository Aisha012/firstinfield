//
//  ContainerVC.swift
//  FirstInField
//
//  Created by Namespace  on 16/06/18.
//  Copyright © 2018 Namespace . All rights reserved.
//

import UIKit
import UserNotifications
import CoreLocation

class ContainerVC: UIViewController, UINavigationControllerDelegate {
    @IBOutlet weak var vwContaoner: UIContentContainer!
    @IBOutlet weak var lblMenu: UILabel!
    @IBOutlet weak var ivMenu: UIImageView!
    @IBOutlet weak var ivMeeting: UIImageView!
    @IBOutlet weak var lblMeeting: UILabel!
    @IBOutlet weak var ivAdhohc: UIImageView!
    @IBOutlet weak var lblAdhoc: UILabel!
    @IBOutlet weak var ivMore: UIImageView!
    @IBOutlet weak var lblMore: UILabel!
    @IBOutlet weak var lblMeetingCounter: UILabel!
    @IBOutlet weak var lblAdhocCounter: UILabel!

    
    
    var tabController:UITabBarController?
    @IBOutlet weak var vwTabs: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tabController?.selectedIndex = 0
       NotificationCenter.default.addObserver(self, selector: #selector(setCounterValue), name: Notification.Name("resetCounter"), object: nil)
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setCounterValue()
        setLayoutForSelectedTab(selectedTag: (tabController?.selectedIndex)!)
       
    }
    
    
    @IBAction func setSelectectedIndex(_ sender: UIButton) {
        tabController?.selectedIndex = sender.tag
        setLayoutForSelectedTab(selectedTag: sender.tag)
        
    }
    
    func setLayoutForSelectedTab(selectedTag: Int){
        switch selectedTag {
        case 0:
            var img = ivMenu.image
            img = img?.withRenderingMode(.alwaysTemplate)
            ivMenu.image = img
            ivMenu.tintColor = #colorLiteral(red: 0.1411764706, green: 0.6156862745, blue: 0.7647058824, alpha: 1)
            lblMenu.textColor = #colorLiteral(red: 0.1960784314, green: 0.7568627451, blue: 0.937254902, alpha: 1)
            
            var img1 = ivMeeting.image
            img1 = img1?.withRenderingMode(.alwaysTemplate)
            ivMeeting.image = img1
            ivMeeting.tintColor = #colorLiteral(red: 0.4745098039, green: 0.4745098039, blue: 0.4745098039, alpha: 1)
            lblMeeting.textColor = #colorLiteral(red: 0.4745098039, green: 0.4745098039, blue: 0.4745098039, alpha: 1)
            
            var img2 = ivAdhohc.image
            img2 = img2?.withRenderingMode(.alwaysTemplate)
            ivAdhohc.image = img2
            ivAdhohc.tintColor = #colorLiteral(red: 0.4745098039, green: 0.4745098039, blue: 0.4745098039, alpha: 1)
            lblAdhoc.textColor = #colorLiteral(red: 0.4745098039, green: 0.4745098039, blue: 0.4745098039, alpha: 1)
            
            var img3 = ivMore.image
            img3 = img3?.withRenderingMode(.alwaysTemplate)
            ivMore.image = img3
            ivMore.tintColor = #colorLiteral(red: 0.4745098039, green: 0.4745098039, blue: 0.4745098039, alpha: 1)
            lblMore.textColor = #colorLiteral(red: 0.4745098039, green: 0.4745098039, blue: 0.4745098039, alpha: 1)
            break
        case 1:
            var img = ivMeeting.image
            img = img?.withRenderingMode(.alwaysTemplate)
            ivMeeting.image = img
            ivMeeting.tintColor = #colorLiteral(red: 0.1411764706, green: 0.6156862745, blue: 0.7647058824, alpha: 1)
            lblMeeting.textColor = #colorLiteral(red: 0.1960784314, green: 0.7568627451, blue: 0.937254902, alpha: 1)
            
            var img1 = ivMenu.image
            img1 = img1?.withRenderingMode(.alwaysTemplate)
            ivMenu.image = img1
            ivMenu.tintColor = #colorLiteral(red: 0.4745098039, green: 0.4745098039, blue: 0.4745098039, alpha: 1)
            lblMenu.textColor = #colorLiteral(red: 0.4745098039, green: 0.4745098039, blue: 0.4745098039, alpha: 1)
            
            var img2 = ivAdhohc.image
            img2 = img2?.withRenderingMode(.alwaysTemplate)
            ivAdhohc.image = img2
            ivAdhohc.tintColor = #colorLiteral(red: 0.4745098039, green: 0.4745098039, blue: 0.4745098039, alpha: 1)
            lblAdhoc.textColor = #colorLiteral(red: 0.4745098039, green: 0.4745098039, blue: 0.4745098039, alpha: 1)
            
            var img3 = ivMore.image
            img3 = img3?.withRenderingMode(.alwaysTemplate)
            ivMore.image = img3
            ivMore.tintColor = #colorLiteral(red: 0.4745098039, green: 0.4745098039, blue: 0.4745098039, alpha: 1)
            lblMore.textColor = #colorLiteral(red: 0.4745098039, green: 0.4745098039, blue: 0.4745098039, alpha: 1)
            
            break
        case 2:
            
            var img = ivAdhohc.image
            img = img?.withRenderingMode(.alwaysTemplate)
            ivAdhohc.image = img
            ivAdhohc.tintColor = #colorLiteral(red: 0.1411764706, green: 0.6156862745, blue: 0.7647058824, alpha: 1)
            lblAdhoc.textColor = #colorLiteral(red: 0.1960784314, green: 0.7568627451, blue: 0.937254902, alpha: 1)
            
            var img1 = ivMenu.image
            img1 = img1?.withRenderingMode(.alwaysTemplate)
            ivMenu.image = img1
            ivMenu.tintColor = #colorLiteral(red: 0.4745098039, green: 0.4745098039, blue: 0.4745098039, alpha: 1)
            lblMenu.textColor = #colorLiteral(red: 0.4745098039, green: 0.4745098039, blue: 0.4745098039, alpha: 1)
            
            var img2 = ivMeeting.image
            img2 = img2?.withRenderingMode(.alwaysTemplate)
            ivMeeting.image = img2
            ivMeeting.tintColor = #colorLiteral(red: 0.4745098039, green: 0.4745098039, blue: 0.4745098039, alpha: 1)
            lblMeeting.textColor = #colorLiteral(red: 0.4745098039, green: 0.4745098039, blue: 0.4745098039, alpha: 1)
            
            var img3 = ivMore.image
            img3 = img3?.withRenderingMode(.alwaysTemplate)
            ivMore.image = img3
            ivMore.tintColor = #colorLiteral(red: 0.4745098039, green: 0.4745098039, blue: 0.4745098039, alpha: 1)
            lblMore.textColor = #colorLiteral(red: 0.4745098039, green: 0.4745098039, blue: 0.4745098039, alpha: 1)
            
            
            break
        case 3:
            var img = ivMore.image
            img = img?.withRenderingMode(.alwaysTemplate)
            ivMore.image = img
            ivMore.tintColor = #colorLiteral(red: 0.1411764706, green: 0.6156862745, blue: 0.7647058824, alpha: 1)
            lblMore.textColor = #colorLiteral(red: 0.1960784314, green: 0.7568627451, blue: 0.937254902, alpha: 1)
            
            var img1 = ivMeeting.image
            img1 = img1?.withRenderingMode(.alwaysTemplate)
            ivMeeting.image = img1
            ivMeeting.tintColor = #colorLiteral(red: 0.4745098039, green: 0.4745098039, blue: 0.4745098039, alpha: 1)
            lblMeeting.textColor = #colorLiteral(red: 0.4745098039, green: 0.4745098039, blue: 0.4745098039, alpha: 1)
            
            var img2 = ivAdhohc.image
            img2 = img2?.withRenderingMode(.alwaysTemplate)
            ivAdhohc.image = img2
            ivAdhohc.tintColor = #colorLiteral(red: 0.4745098039, green: 0.4745098039, blue: 0.4745098039, alpha: 1)
            lblAdhoc.textColor = #colorLiteral(red: 0.4745098039, green: 0.4745098039, blue: 0.4745098039, alpha: 1)
            
            var img3 = ivMenu.image
            img3 = img3?.withRenderingMode(.alwaysTemplate)
            ivMenu.image = img3
            ivMenu.tintColor = #colorLiteral(red: 0.4745098039, green: 0.4745098039, blue: 0.4745098039, alpha: 1)
            lblMenu.textColor = #colorLiteral(red: 0.4745098039, green: 0.4745098039, blue: 0.4745098039, alpha: 1)
            
            break
        default:
            break
        }
        
    }
    let containerSegueIdentifier = "ContainerSegue"
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == containerSegueIdentifier {
            tabController = segue.destination as? UITabBarController
            for cont  in tabController!.viewControllers!{
                (cont as! UINavigationController).delegate = self
            }
        }
    }

    func navigationController(_ navigationController: UINavigationController, willShow viewController: UIViewController, animated: Bool) {
            self.vwTabs.isHidden = navigationController.viewControllers.count > 1
            navigationController.view.frame.size.height = navigationController.viewControllers.count > 1 ? UIScreen.main.bounds.size.height : UIScreen.main.bounds.size.height - 60
            self.tabController!.tabBar.isHidden = true
    }
    
    func navigationController(_ navigationController: UINavigationController, didShow viewController: UIViewController, animated: Bool) {
        
    }
    
    
    
    
    
    
    func checkPermissions(){
        switch CLLocationManager.authorizationStatus() {
        case .notDetermined:
            // Request when-in-use authorization initially
            self.performSegue(withIdentifier: "ShowNoti", sender: nil)
            return
            
        case .restricted, .denied:
            self.performSegue(withIdentifier: "ShowNoti", sender: nil)
            return
            // txtUsername.delegate =  self
            // txtPassword.delegate =  self
        // Do any additional setup after loading the view, typically from a nib.
        case .authorizedAlways:
            break
        case .authorizedWhenInUse:
            break
        }
        
        
        
        if #available(iOS 10.0, *) {
            UNUserNotificationCenter.current().getNotificationSettings {[weak self] (settings) in
                guard settings.authorizationStatus == .authorized else {
                    self?.performSegue(withIdentifier: "ShowNoti", sender: nil)
                    return
                }
                let resPonsedata = self?.getUserDefaults(key: KeyResponseModel) as! Data
                let responseModel = try! self?.jsonDecoder.decode(LoginResponse.self, from: resPonsedata)
                
                if responseModel!.user!.id != nil && responseModel!.user!.id != 0{
                self?.makeCallDeviceTokenApi(userID: "\(responseModel!.user!.id!)")
                }
                
            }
        } else {
            if UIApplication.shared.isRegisteredForRemoteNotifications == false {
                self.performSegue(withIdentifier: "ShowNoti", sender: nil)
                return
            }
        }
    }
    
    func makeCallDeviceTokenApi(userID:String){
        let token = UserDefaults.standard.value(forKey: KEY_DEVICE_TOKEN)
        let deviceID  =  UIDevice.current.identifierForVendor!.uuidString
        let deviceName = UIDevice.current.name
        if token != nil {
            let device_token_request  = DeviceTokeRequest(user_id: userID, notification_status: "1"/*"true"*/, device_id:deviceID , device_token: token as? String, device_type: "iPhone", device_name: deviceName, country_code: nil)
            print("TOKEN >> \(String(describing: token))")
            self.postDataFromNet(url: Urls.Post_Device_Token.rawValue, requestData: device_token_request, selector: #selector(ContainerVC.handleNetDeviceResponse(data:)))
        }
       
    }
    
    @objc func handleNetDeviceResponse(data:Data?) {
        
        if let dat = data{
            do{
                print(dat)
            }catch let err {
                print(err)
            }
        }
    }
    
    
    @objc func setCounterValue(){
        
        print(getUserDefaults(key: Meeting_Count))
        print(getUserDefaults(key: Adhoc_Count))
     
        lblMeetingCounter.text = "\(self.getUserDefaults(key: Meeting_Count) ?? 0)"
        lblMeetingCounter.isHidden = (lblMeetingCounter.text == "0")

        lblAdhocCounter.text = "\(self.getUserDefaults(key: Adhoc_Count) ?? 0)"
        lblAdhocCounter.isHidden = (lblAdhocCounter.text == "0")
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}




