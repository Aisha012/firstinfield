//
//  CreateAddhocMeeting.swift
//  FirstInField
//
//  Created by Namespace  on 20/09/18.
//  Copyright © 2018 Namespace . All rights reserved.
//

import UIKit
import GooglePlaces
import ActionSheetPicker_3_0
import RSSelectionMenu

class CreateAddhocMeeting: UIViewController, UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate, AddressPlacePickerTableViewCellDelegate {
   
    var repDict = NSMutableDictionary()
    var userRegistration = UserRegistration()
    var selectedReps = [String]()
    var reps = [String]()
    
    @IBOutlet weak var tbleView: UITableView!
    

    var titles = ["Meeting Name", "Date", "Start Time", "End Time", "Representative", "Location"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        self.navigationItem.title = "Add Meeting"
        self.navigationItem.backBarButtonItem?.title = "Back"
        
         getRepsList()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- Helping Func
    func validateFields()->Bool{
        
        for i in 0..<6{
            let cell = tbleView.cellForRow(at: IndexPath.init(row: i, section: 0)) as! AdhocCell
            if !cell.txtFld.isTextFieldEmpty(){
                self.showSnackBarWithMessage(message: titles[i] + " is empty.", color: #colorLiteral(red: 0.7450980544, green: 0.1568627506, blue: 0.07450980693, alpha: 1), interval: UIViewController.intervals.long)
                return false
            }
            switch i {
            case 0:
                userRegistration.meetingname = cell.txtFld.text!
                break
            case 1:
                userRegistration.date = cell.txtFld.text!
                break
            case 2:
                let startTime = getDateFromString(dateString: "\(userRegistration.date! + ", " + cell.txtFld.text!)", fromFormat: "dd MMM yyyy, HH:mm")
                if  startTime<Date(){
                    self.showSnackBarWithMessage(message: titles[i] + " must be greater that Current Time.", color: #colorLiteral(red: 0.7450980544, green: 0.1568627506, blue: 0.07450980693, alpha: 1), interval: UIViewController.intervals.long)
                    return false
                }
                userRegistration.scheduled_start_time = cell.txtFld.text!
                break
            case 3:
                let startTime = getDateFromString(dateString: "\(userRegistration.date! + ", " + userRegistration.scheduled_start_time!)", fromFormat: "dd MMM yyyy, HH:mm")
                let endTime = getDateFromString(dateString: "\(userRegistration.date! + ", " + cell.txtFld.text!)", fromFormat: "dd MMM yyyy, HH:mm")
                if  startTime>endTime{
                    self.showSnackBarWithMessage(message: titles[i] + " must be greater that Start Time.", color: #colorLiteral(red: 0.7450980544, green: 0.1568627506, blue: 0.07450980693, alpha: 1), interval: UIViewController.intervals.long)
                    return false
                }
                userRegistration.scheduled_end_time = cell.txtFld.text!
                break
            case 4:
                
                break
            case 5:
                userRegistration.scheduled_address = cell.txtFld.text!
                break
            default:
                break
            }
        }
        return true
    }
    
    
    //MARK:- IBAction
   @IBAction func onPressSubmit(btn:UIButton){
    if validateFields(){
        userRegistration.api_token = API_Token
        var repsIds = [Int]()
        for item in selectedReps{
            repsIds.append(repDict.value(forKey: item) as! Int)
        }
        userRegistration.representatives = repsIds
        let resPonsedata = self.getUserDefaults(key: KeyResponseModel) as! Data
        let responseModel = try! self.jsonDecoder.decode(LoginResponse.self, from: resPonsedata)
        userRegistration.id = "\(responseModel.user!.id!)"
        userRegistration.user_token = responseModel.user!.user_token!
        
        userRegistration.scheduled_start_time = localToUTC(date: userRegistration.scheduled_start_time!, fromFormat: "HH:mm", toFormat: "HH:mm:ss")
        userRegistration.scheduled_end_time = localToUTC(date: userRegistration.scheduled_end_time!, fromFormat: "HH:mm", toFormat: "HH:mm:ss")
        userRegistration.date = userRegistration.date != nil ? localToUTC(date: userRegistration.date!, fromFormat: "dd MMM yyyy", toFormat: "yyyy-MM-dd") : ""
        
        self.postDataFromNet(url: Urls.Post_Save_Meeting.rawValue, requestData: userRegistration, selector: #selector(CreateAddhocMeeting.handleSavetResponse(data:)))
    }
 }

    //MARK:- Webservice
    @objc func handleSumitResponse(data:Data?) {
        if let dat = data{
            do{
                let repsResp = try jsonDecoder.decode(UserRepsListResponse.self, from: dat)
                if repsResp.res_msg != nil && repsResp.res_code! != 200{
                    self.showSnackBarWithMessage(message: responseModel!.res_msg!, color: #colorLiteral(red: 0.9254902005, green: 0.2352941185, blue: 0.101907857, alpha: 1), interval: .long)
                    return
                }
                if let repsData = repsResp.data{
                    reps.removeAll()
                    repDict.removeAllObjects()
                    for name in repsData{
                        repDict.setValue(name.id!, forKey: (name.firstname != nil ? name.firstname!: "") + (name.lastname != nil ?  " " + name.lastname!:""))
                    }
                }
                
            }catch let err{
                print(err)
            }
        }
    }
    
    
    @objc func handleSavetResponse(data:Data?) {
        if let dat = data{
            do{
                let repsResp = try jsonDecoder.decode(SaveMeetingResponse.self, from: dat)
                if repsResp.res_msg != nil && repsResp.res_code! != 200{
                    self.showSnackBarWithMessage(message: repsResp.res_msg!, color: #colorLiteral(red: 0.9254902005, green: 0.2352941185, blue: 0.101907857, alpha: 1), interval: .long)
                    return
                }
                if repsResp.res_msg != nil && repsResp.res_code! == 200{
                    self.showSnackBarWithMessage(message: repsResp.res_msg!, color: #colorLiteral(red: 0.2745098174, green: 0.4862745106, blue: 0.1411764771, alpha: 1), interval: .long)
                self.navigationController?.popViewController(animated: true)
//                    self.dismiss(animated: true, completion: {
//                        NotificationCenter.default.post(name: Notification.Name("refreshAdhoc"), object: nil)
//
//                    })
                    return
                }
                
                
            }catch let err{
                print(err)
            }
        }
    }
    
    
    func getRepsList(){
        let resPonsedata = self.getUserDefaults(key: KeyResponseModel) as! Data
        let responseModel = try! self.jsonDecoder.decode(LoginResponse.self, from: resPonsedata)
        let completeUrl =  Urls.Get_Rep_list.rawValue + "\(String(describing: responseModel.user!.id!))" + "?api_token=" + API_Token + "&user_token=" + responseModel.user!.user_token!
        self.getDataFromNet(url: completeUrl, selector:#selector(CreateAddhocMeeting.handleSumitResponse(data:)) )
    }

    //MARK:- Cell Delegates
    func setPickerForRepresentativeFields(txtFld:UITextField){
        self.view.endEditing(true)
        let selectionMenu = RSSelectionMenu(selectionType: .Multiple, dataSource: repDict.allKeys as! [String], cellType: .Basic) { (cell, object, indexPath) in
            cell.textLabel?.text = object
        }
        if selectedReps.count > 0 {
            selectionMenu.setSelectedItems(items: selectedReps) { (strVal, boolVal, strArr) in
                print(strArr)
            }
        }
        selectionMenu.show(style: .Actionsheet(title: "Representatives List", action: "Done", height: Double(UIScreen.main.bounds.height-100)), from: self)
        selectionMenu.onDismiss = { selectedItems in
            self.view.endEditing(true)
            self.selectedReps = selectedItems
            txtFld.text =  self.selectedReps.joined(separator: ",")
            // perform any operation once you get selected items
        }
        //selectionMenu.show(style: .Alert(title: "Select", action: nil, height: nil), from: self)
    }
    
    func showPlacePicker() {
        let autocompleteController = GMSAutocompleteViewController()
        autocompleteController.delegate = self
        autocompleteController.primaryTextColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        autocompleteController.tintColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        self.present(autocompleteController, animated: true, completion: nil)
    }
   
//    @IBAction func closeBtn(_ sender: UIButton) {
//        self.dismiss(animated: true, completion: {
//            NotificationCenter.default.post(name: Notification.Name("refreshAdhoc"), object: nil)
//
//        })
//    }

    
    //MARK:- Tableview delegates
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 6
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: titles[indexPath.row], for: indexPath) as! AdhocCell
        switch indexPath.row {
        case 1:
            cell.setPicker(mode: UIDatePickerMode.date)
            cell.picker.minimumDate = Date()
            
            break
        case 2,3:
           cell.setPicker(mode: UIDatePickerMode.time)
          break
        case 4, 5:
            cell.txtFld.delegate = cell
            cell.delegate = self
  
        default:
          break
        }
        cell.selectionStyle = .none
        return cell

    }

    
}


extension CreateAddhocMeeting: GMSAutocompleteViewControllerDelegate {
    
    // Handle the user's selection.
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        
        userRegistration.latitude = String(place.coordinate.latitude)
        userRegistration.longitude = String(place.coordinate.longitude)

//        // Get the address components.
//        if let addressLines = place.addressComponents {
//            // Populate all of the address fields we can find.
//            for field in addressLines {
//                switch field.type {
//                case kGMSPlaceTypeCountry:
//                    let index = countries.index { (country) -> Bool in
//                        country.name == field.name
//                    }
//                    if let _index = index {
//                        userRegistration.country = countries[_index].name
//                        userRegistration.countryID = countries[_index].id
//                    }
//                case kGMSPlaceTypePostalCode:
//                    userRegistration.zipCode = field.name
//                case kGMSPlaceTypeLocality:
//                    userRegistration.city = field.name
//                default:
//                    print("ignore")
//                }
//            }
//        }
//
//        tableView.reloadData()
        let cell = tbleView.cellForRow(at: IndexPath.init(row: 5, section: 0)) as! AdhocCell
        cell.txtFld.text = place.formattedAddress ?? place.name
       viewController.dismiss(animated: true, completion: nil)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        // TODO: handle the error.
        print("Error: ", error.localizedDescription)
    }
    
    // User canceled the operation.
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        viewController.dismiss(animated: true, completion: nil)
    }
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
}

//MARK:- AdhocCell

class AdhocCell: UITableViewCell, UITextFieldDelegate {
    
    @IBOutlet weak var txtFld:UITextField!
    @IBOutlet weak var fldLbl:UILabel!
   weak var delegate:AddressPlacePickerTableViewCellDelegate?
    
    @objc func updateDateField(sender: UIDatePicker) {
        switch sender.datePickerMode {
        case .date:
            self.txtFld.text = formatDateForDisplay(date: picker.date)
            break
        default:
            self.txtFld.text = formatDateForDisplayTime(date: picker.date)
            break
        }
        
//        self.txtFld.text = formatDateForDisplay(date: sender.date)
    }
    
    let picker = UIDatePicker()
    func setPicker(mode:UIDatePickerMode){
        picker.date = Date()
        picker.datePickerMode = mode
        self.txtFld.inputView = picker
        picker.addTarget(self, action: #selector(AdhocCell.updateDateField(sender:)), for: .valueChanged)
        self.txtFld.delegate = self
    }
  
    fileprivate func formatDateForDisplay(date: Date) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd MMM yyyy"
        return formatter.string(from: date)
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
       
        switch textField.tag {
        case 2:
            self.txtFld.text = formatDateForDisplay(date: picker.date)
            return
        case 3,4:
            self.txtFld.text = formatDateForDisplayTime(date: picker.date)
            return

        case 5:
            self.delegate?.setPickerForRepresentativeFields(txtFld: textField)
            return
        case 6:
            self.endEditing(true)
             self.delegate?.showPlacePicker()
            return
           
        default:
            break
        }
       
    }
    
    fileprivate func formatDateForDisplayTime(date: Date) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = "HH:mm"
        return formatter.string(from: date)
    }
    

}



protocol AddressPlacePickerTableViewCellDelegate:class {
    func showPlacePicker()
    func setPickerForRepresentativeFields(txtFld:UITextField)

}

